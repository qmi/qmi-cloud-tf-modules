resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!)"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "azurerm_postgresql_flexible_server" "postgresql-server" {
  name = "qmi-postgresql-${var.provision_id}"
  location = var.location
  resource_group_name = var.resource_group_name

  version                = var.postgresql-version
 
  administrator_login    = var.admin_login
  administrator_password = random_password.password.result

  storage_mb = var.postgresql-storage
  sku_name   = var.postgresql-sku-name

  geo_redundant_backup_enabled = false

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
    ProvId = var.provision_id
  }

}

resource "azurerm_postgresql_flexible_server_configuration" "conf1" {
  name      = "wal_level"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  value     = "logical"
}

resource "azurerm_postgresql_flexible_server_configuration" "conf2" {
  name      = "log_statement"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  value     = "all"
}

resource "azurerm_postgresql_flexible_server_database" "postgresql-db" {
  name      = "QlikPostgresqlDB"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  collation = "en_US.utf8"
  charset   = "utf8"
}
