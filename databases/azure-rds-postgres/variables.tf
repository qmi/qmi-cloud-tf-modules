variable "resource_group_name" {

}

variable "provision_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "location" {
  type = string
  description = "(optional) describe your variable"
  default = "EAST US"
}

variable "subnet_id" {
  default = null
}


variable "admin_login" {
  type = string
  description = "Login to authenticate to PostgreSQL Server"
  default = "qmirdsuser"
}

variable "postgresql-version" {
  type        = string
  description = "PostgreSQL Server version to deploy"
  default     = "11"
}
variable "postgresql-sku-name" {
  type        = string
  description = "PostgreSQL SKU Name"
  default     = "GP_Gen5_8"
}
variable "postgresql-storage" {
  type        = string
  description = "PostgreSQL Storage in MB"
  default     = "5120"
}

variable "user_id" {

}

variable "dummydata" {
  default = null
}

variable "envbranch" {
  default = "master"
}