Param(
    [string] $Username,
    [string] $Pass
)

Import-Module "Carbon"

$secpasswd = ConvertTo-SecureString $Pass -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ($Username, $secpasswd)

Install-CUser -Credential $cred 
Write-Log "Done! Setting new password to user $Username"