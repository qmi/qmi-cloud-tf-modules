<#
Module:             di-db-setup
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - First release 04/05/2020
last updated:       04/05/2020
Intent: Setting the DBs
#>

Param(
    [string] $version = 'gen2'
)

Write-Host "--- Setting Compose License"

# TODO set license
$bin = "C:\Program Files\Qlik\Compose\bin\"
if ( $version -eq "c4dw" ) {
    $bin = "C:\Program Files\Attunity\Compose for Data Warehouses\bin\"
}
$bin = $bin -replace ' ','` '

$cmd = "$($bin)ComposeCli.exe connect"
Write-Host "--- Connect to Compose: $($cmd)"
$cmd = "$($bin)ComposeCli.exe connect"
Invoke-Expression $cmd

Start-Sleep 5

if ( $version -eq "2022.2.0" -or $version -eq "c4dw") {
    # Old way
    $cmd = "$($bin)ComposeCli.exe register_license --req @$PSScriptRoot\compose-license.txt"
} else {
    # New way
    $cmd = "$($bin)ComposeCli.exe register_license --infile $PSScriptRoot\compose-license.txt"
}
Write-Host "Apply Compose License: $($cmd)"
Invoke-Expression $cmd

Start-Sleep 5

