
variable "prefix" {
  default = "QMI"
}

variable "subnet_id" {
}

variable "location" {
}

variable "snapshot_id" {
  default = null
}

variable "snapshot_uri" {
  default = null
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_D4s_v3"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "scdemoadmin"
}

variable "initial_password" {
  default = null
}

variable "virtual_machine_name" {
  default = null
}

variable "user_id" {
  default = null
}

variable "provId" {
  default = null
}

variable "is_24x7"{
  type = bool
  default = null
}

variable "shutdownTime"{
  default = null
}

variable "startupTime"{
  default = null
}

variable "notrename" {
  default = null
}

variable "restartAfterRename" {
  default = true
}

variable "waitAfterRestartSecs" {
  default = 30
}

variable "carbonblack" {
  default = true
}

variable "tenable" {
  default = true
}

variable "wincommon" {
  default = true
}


