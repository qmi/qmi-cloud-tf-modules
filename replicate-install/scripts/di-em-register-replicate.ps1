
Param(
    [string] $user,
    [string] $pass,
    [string] $domain = "qem.qmi"
)

Import-Module "Carbon"
Set-HostsEntry -IPAddress 127.0.0.1 -HostName 'localhost'
Set-HostsEntry -IPAddress 127.0.0.1 -HostName 'di.qmi.qlik-poc.com'


$domainuser = "$($env:COMPUTERNAME).$($domain)\$user"

$pair = "$($domainuser):$($pass)"
$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes($pair))
$basicAuthValue = "Basic $encodedCreds"
$Headers = @{
    Authorization = $basicAuthValue
}

$response=(Invoke-WebRequest -UseBasicParsing -Uri "https://di.qmi.qlik-poc.com/attunityenterprisemanager/api/v1/login" -Headers $Headers | Select-Object -ExpandProperty Headers)

$Headers2 = @{
    "EnterpriseManager.APISessionID" = $response."EnterpriseManager.APISessionID"
}

$body_json = Get-Content -Raw -Path "$PSScriptRoot\addrepl2em.json" | ConvertFrom-Json
$replsrvName = "replicateServer1"
$body_json.name = $replsrvName
$body_json.username = $user
$body_json.password = $pass
$body_json.host = "$($env:COMPUTERNAME).$($domain)"
$body_json = $body_json | ConvertTo-Json


Write-Host "--- Registering Replicate server in Enterprise Manager with name '$replsrvName' ..."
Invoke-RestMethod -Uri "https://di.qmi.qlik-poc.com/attunityenterprisemanager/api/v1/servers/$replsrvName/def" -Method "PUT" -Headers $Headers2 -Body $body_json

Write-Host "--- Setting Replicate server license through EM API"
Invoke-WebRequest -UseBasicParsing -Uri "https://di.qmi.qlik-poc.com/attunityenterprisemanager/api/v1/servers/$replsrvName/license/def" -Method "PUT" -Infile "$PSScriptRoot\replicate_license.txt" -Headers $Headers2
