resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "random_password" "qlikpassword" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = random_password.password.result
}

data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  network_interface_ids = [ module.qmi-nic.id ]

  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_id =  var.image_reference

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "3m"
    }
    source      = "${path.module}/scripts"
    destination = "C:/provision"
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = "qservice"
      password = "Qlik1234"
      port     = 5985
      https    = false
      timeout  = "3m"
    }

    inline = [
      "powershell.exe -File C:/provision/bootstrap-qs.ps1 -ModuleName vm-qs-qib",
      //"powershell.exe -File C:/provision/gen-jwt.ps1",
    ]
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = "qservice"
      password = "Qlik1234"
      port     = 5985
      https    = false
      timeout  = "3m"
    }

    inline = [
      "powershell.exe -File C:/provision/qs-post-cfg.ps1 -Hostname ${var.resource_group_name}.qmi.qlik-poc.com -CertPwd \"${data.azurerm_key_vault_secret.cert-password.value}\" -QlikUserPwd ${random_password.qlikpassword.result}",
      "powershell.exe -File C:/provision/bootstrap-qib/qib.ps1 -ExternalDomain ${var.resource_group_name}.qmi.qlik-poc.com  -IsExternalAccess ${var.is_external_access} -ImageReference ${var.image_reference}",
    ]
  }
  
}