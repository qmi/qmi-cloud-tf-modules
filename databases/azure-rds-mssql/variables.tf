variable "resource_group_name" {
}

variable "location" {

}

variable "provision_id" {
    default = null
}

variable "subnet_id" {
    default = null
}

variable "user_id" {
    
}

variable "sql_administrator_login" {
    default = "scdemoadmin"
}

variable "sql_administrator_login_password" {
    default = null
}

variable "dummydata" {
  default = null
}

variable "envbranch" {
  default = "master"
}
