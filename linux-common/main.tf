data "azurerm_key_vault_secret" "cid" {
  name         = "falcon-cid"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "tenable-key" {
  name         = "tenable-key"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "cert_password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

#data "azurerm_key_vault_certificate_data" "cert-data" {
#  name         = "star-qmi-qlikpoc-com-cert"
#  key_vault_id = var.key_vault_id
#}


locals {
  falcon_id     = nonsensitive(data.azurerm_key_vault_secret.cid.value)
  tenable_key   = nonsensitive(data.azurerm_key_vault_secret.tenable-key.value)
  cert_password = nonsensitive(data.azurerm_key_vault_secret.cert_password.value)
  #cert_pem =  nonsensitive(data.azurerm_key_vault_certificate_data.cert-data.pem)
  #cert_key =  nonsensitive(data.azurerm_key_vault_certificate_data.cert-data.key)
} 

resource "null_resource" "files" {
  provisioner "file" {
        connection {
            type     = "ssh"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            timeout  = "10m"
            #private_key = "${file("~/.ssh/id_rsa")}"   
        }
        source      = "${path.module}/${var.os_type}/common"
        destination = "/home/${var.admin_username}"
    }
}

resource "null_resource" "post-linux-vm" {
    
    depends_on = [
      null_resource.files
    ]

    provisioner "remote-exec" {
        connection {
            type     = "ssh"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            timeout  = "3m"
            #private_key = "${file("~/.ssh/id_rsa")}"
        }

        inline = [
            "echo ${var.admin_password} | sudo -S chmod u+x /home/${var.admin_username}/common/*.sh",
            "sudo /home/${var.admin_username}/common/falcon.sh '${local.falcon_id}'",
            "sudo /home/${var.admin_username}/common/tenable.sh '${local.tenable_key}'",
            "sudo /home/${var.admin_username}/common/extract-certs.sh '${local.cert_password}'",
            #"sudo /home/${var.admin_username}/common/save-cert.sh '${local.cert_pem}' '${local.cert_key}'",
        ]
    }
}

resource "null_resource" "update" {

  count = var.update? 1 : 0

  depends_on = [
      null_resource.files,
      null_resource.post-linux-vm
  ]

  provisioner "remote-exec" {
        connection {
            type     = "ssh"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            timeout  = "3m"
            #private_key = "${file("~/.ssh/id_rsa")}"
        }

        inline = [
            "echo ${var.admin_password} | sudo -S chmod u+x /home/${var.admin_username}/common/*.sh",
            "sudo /home/${var.admin_username}/common/update.sh",
        ]
  }
}

resource "null_resource" "resize" {

  count = var.resize? 1 : 0

  depends_on = [
      null_resource.files,
      null_resource.update
  ]

  provisioner "remote-exec" {
        connection {
            type     = "ssh"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            timeout  = "3m"
            #private_key = "${file("~/.ssh/id_rsa")}"
        }

        inline = [
            "echo ${var.admin_password} | sudo -S chmod u+x /home/${var.admin_username}/common/*.sh",
            "sudo /home/${var.admin_username}/common/resizedisk.sh",
        ]
  }
}

