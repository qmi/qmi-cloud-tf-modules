output "finished" {
    value = true

    depends_on = [
      null_resource.install
    ]
}