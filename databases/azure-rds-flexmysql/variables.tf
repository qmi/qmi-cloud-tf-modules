variable "resource_group_name" {

}

variable "provision_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "location" {
  type = string
  description = "(optional) describe your variable"
  default = "EAST US"
}

variable "user_id" {
}

variable "admin_login" {
  type = string
  description = "Login to authenticate to MySQL Server"
  default = "qmi"
}

variable "mysql-version" {
  type = string
  description = "MySQL Server version to deploy"
  default = "5.7"
}
variable "mysql-sku-name" {
  type = string
  description = "MySQL SKU Name"
  default = "GP_Standard_D2ds_v4"
}
variable "mysql-storage" {
  type = string
  description = "MySQL Storage in MB"
  default = "20"
}

variable "dummydata" {
  default = null
}
