#!/bin/bash

echo "--- Executing: $0 $@"

BASEDIR=$(dirname "$0")

CERTSFOLDER=$BASEDIR/qmicerts
mkdir -p $CERTSFOLDER

wget --quiet https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pfx -O $CERTSFOLDER/wildcard_qmi_qlik-poc_com.pfx
wget --quiet https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pem -O $CERTSFOLDER/wildcard_qmi_qlik-poc_com.pem

PASSWORD=$1

echo "CERT_PASSWORD: $PASSWORD"

# Extract the private key
openssl pkcs12 -in $CERTSFOLDER/wildcard_qmi_qlik-poc_com.pfx -nocerts -nodes -out $CERTSFOLDER/wildcard_qmi_qlik-poc_com.key -passin pass:$PASSWORD
# Extract the public key
openssl pkcs12 -in $CERTSFOLDER/wildcard_qmi_qlik-poc_com.pfx -clcerts -nokeys -out $CERTSFOLDER/wildcard_qmi_qlik-poc_com.crt -passin pass:$PASSWORD
# Extract the CA cert chain
openssl pkcs12 -in $CERTSFOLDER/wildcard_qmi_qlik-poc_com.pfx -cacerts -nokeys -out $CERTSFOLDER/wildcard_qmi_qlik-poc_com-ca.crt -passin pass:$PASSWORD


cat $CERTSFOLDER/wildcard_qmi_qlik-poc_com-ca.crt $CERTSFOLDER/wildcard_qmi_qlik-poc_com.crt > $CERTSFOLDER/wildcard_qmi_qlik-poc_com-fullchain.crt

rm -fr $CERTSFOLDER/wildcard_qmi_qlik-poc_com.crt $CERTSFOLDER/wildcard_qmi_qlik-poc_com-ca.crt


echo $PASSWORD > $CERTSFOLDER/pfx-cert-password.txt