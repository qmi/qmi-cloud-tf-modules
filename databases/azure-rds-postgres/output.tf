/*output "postgresql_server" {
  value = azurerm_postgresql_server.postgresql-server
}*/

output "db_server_fqdn" {
  value = azurerm_postgresql_server.postgresql-server.fqdn
}

output "root_username" {
  value = "${var.admin_login}@qmi-postgresql-${var.provision_id}"
}

output "root_username_password" {
  value = nonsensitive(random_password.password.result)
}

output "dummy_data_databases_available" {
  value = var.dummydata != null? module.dummy-data[0].dbs : null
}
