Param(
    [string] $SensorSettings
)

New-Item -ItemType Directory -Force -Path C:\Temp | Out-Null

Write-Host "Installing and configuring CarbonBlack sensor agent..."
$ProgressPreference = 'SilentlyContinue'
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$cbBinaryUrl = "https://d7ipctdjxxii4.cloudfront.net/others/CarbonBlackClientSetup.exe"
Invoke-WebRequest -Uri $cbBinaryUrl -OutFile "C:\Temp\CarbonBlackClientSetup.exe"

$iniFile="C:\Temp\sensorsettings.ini"
if (!(Test-Path $iniFile)) {
    New-Item $iniFile | Out-Null
}
Set-Content C:\Temp\sensorsettings.ini $SensorSettings | Out-Null
(Get-Content C:\Temp\sensorsettings.ini) -replace ' ',"`r`n" | Set-Content C:\Temp\sensorsettings.ini -Force

C:\Temp\CarbonBlackClientSetup.exe /S

Write-Host "CarbonBlack is configure!"



