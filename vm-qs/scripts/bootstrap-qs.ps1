Param(
    [string] $ModuleName = "vm-qs"
)

# Helper Functions
# ----------------
function New-Credential($u,$p) {
    $secpasswd = ConvertTo-SecureString $p -AsPlainText -Force
    return New-Object System.Management.Automation.PSCredential ($u, $secpasswd)
}

Write-Log "--- Current logged-on user: $env:USERNAME"

New-NetFirewallRule -DisplayName "Qlik Sense More" -Direction Inbound -LocalPort 4244, 4747, 4242, 4432, 4444, 5355, 5353, 80, 4248, 3090, 4000, 5555, 5556, 4993, 4994 -Protocol TCP -Action Allow -ea Stop | Out-Null

REG ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 0 /f

#Set-Service -Name "QlikLoggingService" -StartupType Automatic
Set-Service -Name "QlikSenseServiceDispatcher" -StartupType Automatic
Set-Service -Name "QlikSenseProxyService" -StartupType Automatic
Set-Service -Name "QlikSenseEngineService"  -StartupType Automatic
Set-Service -Name "QlikSensePrintingService"  -StartupType Automatic
Set-Service -Name "QlikSenseSchedulerService"  -StartupType Automatic
Set-Service -Name "QlikSenseRepositoryService"  -StartupType Automatic
Set-Service -Name "QlikSenseRepositoryDatabase"  -StartupType Automatic

Write-Log "===== Bootstrap Qlik Sense ====="

#Delete certificates
#Write-Log "Deleting old certificates files..."
#Get-ChildItem "$($env:ProgramData)\Qlik\Sense\Repository\Exported Certificates\" | Remove-Item -Recurse
Write-Log "--- Deleting old certificates from keyStore..."
$Certs = Get-ChildItem cert:"CurrentUser\My"
$Certs | ForEach-Object{Remove-Item -path $_.PSPath -recurse -Force}
$Certs = Get-ChildItem cert:"LocalMachine\My"
$Certs | ForEach-Object{Remove-Item -path $_.PSPath -recurse -Force}
$Certs = Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'GEN-QS' -or $_.Subject -match 'QMI' }
$Certs | ForEach-Object{Remove-Item -path $_.PSPath -recurse -Force}

Write-Log "--- Starting only QlikSenseRepositoryDatabase and QlikSenseServiceDispatcher services ..."
Stop-Service QlikSenseRepositoryService
Start-Service QlikSenseRepositoryDatabase
Start-Service QlikSenseServiceDispatcher

Write-Log "--- Setting new hostname ($($env:computername)) in to Host.cfg file..."
$enchostname = [Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes("$($env:computername)"))
Set-Content -Path C:\ProgramData\Qlik\Sense\Host.cfg -Value $enchostname


Write-Log "--- Executing ... Repository.exe -bootstrap -standalone -restorehostname"
$waiting=20
if ( $env:USERNAME -eq "qservice" ) {
    $waiting=50
    Write-Log "--- The user executing this script is already 'qservice'"
    Start-Process powershell.exe -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/k C:\PROGRA~1\Qlik\Sense\Repository\Repository.exe -bootstrap -standalone -restorehostname'"

} else {
    Write-Log "--- The user executing this script is NOT 'qservice'"
    $cred = New-Credential "qservice" "Qlik1234"
    Start-Process powershell.exe -Credential $cred -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/k C:\PROGRA~1\Qlik\Sense\Repository\Repository.exe -bootstrap -standalone -restorehostname'"
}

Start-Sleep -s $waiting

Write-Log "--- Restarting Service Dispatcher"
Restart-Service QlikSenseServiceDispatcher -Force

Start-Sleep -s 30

Write-Log "Restarting all Qlik Sense services"
#Restart-Service QlikSenseServiceDispatcher -Force
#Restart-Service QlikLoggingService -Force
Restart-Service QlikSenseRepositoryService -Force
Start-Sleep -s 10
Restart-Service QlikSenseProxyService -Force
Restart-Service QlikSenseEngineService -Force
Restart-Service QlikSenseSchedulerService -Force 
Restart-Service QlikSensePrintingService -Force 


REG ADD HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System /v ConsentPromptBehaviorAdmin /t REG_DWORD /d 5 /f

#### Recreate QS desktop shortcuts
Write-Log "--- Recreating Qlik Sense desktop shortcuts..."
$sourcepath="C:\Users\Public\Desktop\Qlik Management Console.lnk"
$destination="C:\Users\Public\Desktop\Qlik Management Console2.lnk"
Copy-Item $sourcepath $destination  ## Get the lnk we want to use as a template
Remove-Item -Path $sourcepath -Force
$shell = New-Object -COM WScript.Shell
$shortcut = $shell.CreateShortcut($destination)  ## Open the lnk
$shortcut.TargetPath = $shell.ExpandEnvironmentStrings("C:\Program Files\Google\Chrome\Application\chrome.exe")
$shortcut.Arguments = "https://$env:computername/qmc"
$shortcut.Save()  ## Save
Rename-Item -Path $destination -NewName "Qlik Management Console.lnk"

$sourcepath="C:\Users\Public\Desktop\Qlik Sense Hub.lnk"
$destination="C:\Users\Public\Desktop\Qlik Sense Hub2.lnk"
Copy-Item $sourcepath $destination  ## Get the lnk we want to use as a template
Remove-Item -Path $sourcepath -Force
$shell = New-Object -COM WScript.Shell
$shortcut = $shell.CreateShortcut($destination)  ## Open the lnk
$shortcut.TargetPath = $shell.ExpandEnvironmentStrings("C:\Program Files\Google\Chrome\Application\chrome.exe")
$shortcut.Arguments = "https://$env:computername/hub"
$shortcut.Save()  ## Save
Rename-Item -Path $destination -NewName "Qlik Sense Hub.lnk"
####


Write-Log "--- New Certs: CurrentUser\My"
Get-ChildItem cert:"CurrentUser\My"
Write-Log "--- New Certs: LocalMachine\My"
Get-ChildItem cert:"LocalMachine\My"
Write-Log "--- New Certs: LocalMachine\Root"
Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'QMI' }
$NewCerts = Get-ChildItem cert:"LocalMachine\Root" | Where-Object { $_.Subject -match 'QMI' }

if ($NewCerts) {

    if ( $ModuleName -ne "vm-qs" ) {
        Start-Sleep -s 20
        Write-Log "--- Recovering Qlik Sense users"
        Start-Process powershell.exe -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/c $PSScriptRoot\qs-reset-users.bat'"
        Restart-Service QlikSenseRepositoryService -Force
    }

    Write-Log "===== Boostrap Qlik Sense completed!! ======"

    # May 2021 needs a new restart of QlikSenseServiceDispatcher 
    Restart-Service QlikSenseServiceDispatcher -Force

} else {
    Write-Error "==== Error Bootstraping Qlik Sense ====="
    throw "Error Bootstraping Qlik Sense"
}

