#!/bin/bash

echo "Setting new password ($2) for user '$1'"
echo $2 | passwd --stdin $1