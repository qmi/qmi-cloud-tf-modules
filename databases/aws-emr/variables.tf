variable "region" {
  default = "us-east-1"
}

variable "provision_id" {
  
}

variable "user_id" {
}

variable "vpc_id_ap" {
  default = "vpc-22ee2844"
}

variable "vpc_id_eu" {
  default = "vpc-73f0500a"
}

variable "vpc_id_us" {
  default = "vpc-c079f5bd"
}

variable "subnet_ids_ap" {
  default = ["subnet-658aec3c", "subnet-e030eba8", "subnet-94309bf2"]
}

variable "subnet_ids_eu" {
  default = ["subnet-4d441b17", "subnet-95c22fde", "subnet-70938116"]
}

variable "subnet_ids_us" {
  default = ["subnet-4d26552b", "subnet-0414685b", "subnet-c97f7c84", "subnet-7f695c71", "subnet-96acd2b7", "subnet-88ab2cb9"]
}

variable "forced_destroy" {
  default = null
}
