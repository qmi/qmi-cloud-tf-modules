output "virtual_machine_id" {
  value = azurerm_virtual_machine.vm.id
}

output "virtual_machine_name" {
    value = azurerm_virtual_machine.vm.name
}

output "admin_username" {
  value = local.admin_username
}

output "admin_password" {
  value = local.admin_password
}

output "nic_id" {
  value = module.qmi-nic.id
}

output "nic_private_ip_address" {
  value = module.qmi-nic.private_ip_address
}

output "nic_ip_configuration_name" {
  value = module.qmi-nic.ip_configuration_name
}

output "principal_id" {
  value = azurerm_virtual_machine.vm.identity.0.principal_id
}