#!/bin/bash

echo "--- Executing: $0 $@"

if  grep -Pq '/dev/(mapper/|disk/by-id/dm)' /etc/fstab  ||  mount | grep -q /dev/mapper/
then
    echo "LVM is in use (Oracle Linux)"
    #work out what disk we need.  About 1 in 5 times we get sdb instead on sda
    rootdisk=$(df --type=xfs|grep \/dev\/sd | sed -e's/[0-9].*//')
    sudo gdisk -l $rootdisk
    sudo growpart $rootdisk 2
    sudo pvresize $rootdisk"2"
    sudo lvextend -l +49%FREE /dev/rootvg/rootlv
    sudo lvextend -l +100%FREE /dev/mapper/rootvg-crashlv
    sudo xfs_growfs /dev/rootvg/rootlv
    sudo xfs_growfs /dev/mapper/rootvg-crashlv
else
    echo "LVM not in use.  Resizing main partition to max available disk"
    (
    echo u # Change the units to sectors
    echo p # List the partitions details
    echo d # Delete  partition
    echo 2 # Partition number
    echo n # Add a new partition
    echo p # Primary partition
    echo 2 # Partition number
    echo   # First sector (Accept default)
    echo   # Last sector (Accept default: varies)
    echo w # Write changes
    ) | sudo fdisk /dev/sda

    sudo partprobe
    sudo xfs_growfs -d /
fi
echo "Done resize!"
echo ""
df -h