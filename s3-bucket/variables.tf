variable "region" {
    default = "us-east-1"
}

variable "provision_id" { 
}

variable "user_id" {
    default = null
}

variable "forced_destroy" {
  default = null
}

variable "tenant_id" {
  default = null
}

