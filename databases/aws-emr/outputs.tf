output "s3_bucket" {
  value = module.qmi-s3-bucket.bucket.s3_bucket_id
}

output "s3_iam_name" {
  value = module.qmi-s3-bucket.iam_name
}

output "s3_iam_access_key" {
  value = module.qmi-s3-bucket.iam_access_key
}

output "s3_iam_access_secret" {
  value =  module.qmi-s3-bucket.iam_access_secret
}