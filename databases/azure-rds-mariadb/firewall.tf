resource "azurerm_mariadb_virtual_network_rule" "vnetrule" {

  count = var.subnet_id != null? 1 : 0

  name                = "vnet-rule-${var.provision_id}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mariadb_server.mariadb-server.name
  subnet_id           = var.subnet_id

}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}


resource "azurerm_mariadb_firewall_rule" "fw_rule" {
  for_each = module.fw-ips.ips

  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mariadb_server.mariadb-server.name
  start_ip_address    = each.value.0
  end_ip_address      = each.value.1

}