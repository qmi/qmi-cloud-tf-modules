terraform {

  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

locals {
  tags = {
    Deployment = "QMI"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner    = var.user_id
    ProvID   = var.provision_id
    Name     = "qmi-${var.provision_id}"
    forced_destroy = var.forced_destroy
  }
}

module "iam_user" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"

  version = "~> 3.0"

  name          = "qmi-user-${var.provision_id}"
  force_destroy = true

  create_iam_user_login_profile = false
  #pgp_key = "keybase:test"

  password_reset_required = false

  tags = local.tags

}

module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  bucket = "qmi-bucket-${var.provision_id}"

  versioning = {
    enabled = false
  }

  force_destroy = true

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

  tags = local.tags

}


resource "aws_iam_user_policy" "lb_ro" {
    
  name = "s3only_policy_${module.iam_user.this_iam_user_name}"
  user = module.iam_user.this_iam_user_name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
      "Version": "2012-10-17",
      "Statement": [
          {
              "Effect": "Allow",
              "Action": [
                          "s3:GetBucketLocation",
                          "s3:ListAllMyBuckets"
                        ],
              "Resource": "arn:aws:s3:::*"
          },
          {
              "Effect": "Allow",
              "Action": "s3:*",
              "Resource": [
                  module.s3_bucket.s3_bucket_arn,
                  "${module.s3_bucket.s3_bucket_arn}/*"
              ]
          }
      ]
  })
}

resource "aws_iam_role" "qlik_s3" {
  
  count = var.tenant_id != null? 1 : 0
  
  name = "qlik_s3_${var.tenant_id}"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        #Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::072398622971:root"
        }
        Condition = {
           StringEquals = {
                  "sts:ExternalId" = "qlik_connection_${var.tenant_id}"
            }
        }
      },
    ]
  })

  tags = local.tags
}

resource "aws_iam_role_policy" "test_policy" {

  count = var.tenant_id != null? 1 : 0

  name = "qmi-bucket-${var.provision_id}_policy"
  role = aws_iam_role.qlik_s3[0].id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Sid = ""
        Effect   = "Allow"
        Action = [
          "s3:GetObject",
          "s3:ListBucket"
        ]
        Resource = [
          "arn:aws:s3:::${module.s3_bucket.s3_bucket_id}",
          "arn:aws:s3:::${module.s3_bucket.s3_bucket_id}/*"
        ]
      },
    ]
  })
}