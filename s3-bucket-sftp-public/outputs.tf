
output "id" {
  value       = aws_transfer_server.public.id
  description = "ID of transfer server"
}

output "endpoint" {
  value       = aws_transfer_server.public.endpoint
  description = "Endpoint of transfer server"
}

output "username" {
  value = "sftpusr"
}

output "sftp-private" {
  value = nonsensitive(aws_secretsmanager_secret_version.private_key.secret_string)
}

output "sftp-public" {
  value = aws_key_pair.generated_key.public_key
}

output "host-key-fingerprint" {
  value = aws_transfer_server.public.host_key_fingerprint
}

output "s3-bucket-name" {
  value = "sftp-${var.provision_id}"
}


