#!/bin/bash

echo "Exec --- $0 $1"

POSTGRES_PASSWD=$1

echo "Setting new Postgres password: $POSTGRES_PASSWD"

#Create passwd file
FILE=/root/.pgpass
if test -f "$FILE"; then
    echo "$FILE exists."
else
    echo *:*:*:postgres:postgres > $FILE
    chmod 0600 $FILE
fi

FILE=/home/qmi/.pgpass
if test -f "$FILE"; then
    echo "$FILE exists."
else
    echo *:*:*:postgres:postgres > $FILE
    chmod 0600 $FILE
fi

sudo psql -h localhost -U postgres -w -c "ALTER USER postgres PASSWORD '$POSTGRES_PASSWD';"

#Create passwd file
echo *:*:*:postgres:$POSTGRES_PASSWD > /home/qmi/.pgpass
chmod 0600 /home/qmi/.pgpass

echo *:*:*:postgres:$POSTGRES_PASSWD > /root/.pgpass
chmod 0600 /root/.pgpass


