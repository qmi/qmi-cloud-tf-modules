 
Param(
    [string] $CertPwd,
    [string] $url
)

try {

    Write-Host "Install qmi_qlik-poc_com certificate on Windows"
    
    $ProgressPreference = 'SilentlyContinue'
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    (New-Object System.Net.WebClient).DownloadFile("https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pfx", "$PSScriptRoot\wildcard_qmi_qlik-poc_com.pfx") 

    $secpasswd = ConvertTo-SecureString $CertPwd -AsPlainText -Force
    $sslCert = Import-PfxCertificate -FilePath $PSScriptRoot/wildcard_qmi_qlik-poc_com.pfx -CertStoreLocation Cert:\LocalMachine\My -Password $secpasswd
    $thumb=$($sslCert.Thumbprint)

    Start-Sleep 20
    
    Write-Host "Set SSL qmi_qlik-poc_com for Replicate"

    Stop-Service AttunityReplicateConsole
    if ( $url -Match "2022.11" -or $url -Match "2023.5" ) {  
        Stop-Service QlikReplicateServer
    } else {
        Stop-Service AttunityReplicateServer
    }

    Start-Process -FilePath "C:\Program Files\Attunity\Replicate\bin\RepUiCtl.exe" -ArgumentList "certificate clean" -Wait -NoNewWindow

    netsh http add sslcert ipport=0.0.0.0:443 certhash=$thumb appid='{4dc3e181-e14b-4a21-b022-59fc669b0914}'

    Start-Service AttunityReplicateConsole 
    if ( $url -Match "2022.11" -or $url -Match "2023.5" ) {  
        Start-Service QlikReplicateServer
    } else {
        Start-Service AttunityReplicateServer
    }
    

    Start-Sleep 10
    Write-Host "Replicate services restarted!"


} catch {
    Write-Host $_.Exception.Message
}