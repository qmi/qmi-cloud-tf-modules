output "virtual_machine_id" {
  value = azurerm_linux_virtual_machine.vm.id
}

output "virtual_machine_name" {
    value = azurerm_linux_virtual_machine.vm.name
}

output "admin_username" {
  value = var.admin_username
}

output "admin_password" {
  value = nonsensitive(random_password.password.result)
}

output "ssh" {
  value = nonsensitive("ssh ${var.admin_username}@${module.qmi-nic.private_ip_address}   --> password: ${random_password.password.result}")
}

output "nic_id" {
  value = module.qmi-nic.id
}

output "nic_private_ip_address" {
  value = module.qmi-nic.private_ip_address
}

output "nic_ip_configuration_name" {
  value = module.qmi-nic.ip_configuration_name
}