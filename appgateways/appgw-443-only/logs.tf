resource "azurerm_monitor_diagnostic_setting" "example" {

    name               = "${local.appgw_name}-${var.provision_id}-diagsettings"
    target_resource_id = azurerm_application_gateway.qmi-app-gw.id

    log_analytics_workspace_id = var.log_analytics_workspace_id

    enabled_log {
        category = "ApplicationGatewayAccessLog"

        retention_policy {
            enabled = true
        }
    }

    enabled_log {
        category = "ApplicationGatewayFirewallLog"

        retention_policy {
            enabled = true
        }
    }
}