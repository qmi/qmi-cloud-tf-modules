terraform {

  required_version = ">= 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.49.0"
    }
  }
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!"
  min_numeric = 1
  upper = true
  lower = true
  numeric = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  s3_prefix = "redshift/qmi-${var.provision_id}"
  provid5              = substr(var.provision_id, 0, 5)
  vpc_id               = (var.region == "eu-west-1") ? var.vpc_id_eu : (var.region == "us-east-1") ? var.vpc_id_us : var.vpc_id_ap
  subnet_ids           = (var.region == "eu-west-1") ? var.subnet_ids_eu : (var.region == "us-east-1") ? var.subnet_ids_us : var.subnet_ids_ap
  port                 = "5439"
  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    ProvID   = var.provision_id
    Name     = "qmi-${var.provision_id}"
    Owner    = var.user_id
    forced_destroy = var.forced_destroy
  }
}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}

module "security_group" {
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG"
  description = "${var.provision_id}-SG"
  vpc_id      = local.vpc_id


  # ingress

  ingress_cidr_blocks = module.fw-ips.cidr_blocks

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Redshift"

    },
  ]

  # egress

  egress_cidr_blocks = module.fw-ips.cidr_blocks

  
  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Redshift"

    },
  ]

  tags = local.tags
}

module "security_group_2" {
  
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG2"
  description = "${var.provision_id}-SG2"
  vpc_id      = local.vpc_id


  # ingress

  ingress_cidr_blocks = module.fw-ips.cidr_blocks_others
  

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Redshift"

    },
  ]

  # egress

  egress_cidr_blocks = module.fw-ips.cidr_blocks_others

  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Redshift"

    },
  ]

  tags = local.tags
}

module "qmi-s3-bucket" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//s3-bucket"
  
  provision_id                  = var.provision_id
  region                        = var.region
  user_id                       = var.user_id

  forced_destroy                = var.forced_destroy
}

module "redshift" {

  source  = "terraform-aws-modules/redshift/aws"
  version = "~> 5.0.0"

  cluster_identifier      = "qmi-${var.provision_id}"
  node_type       = "dc2.large" #"dc1.large"
  number_of_nodes = 1

  database_name   = var.cluster_database_name
  master_username = var.cluster_master_username
  create_random_password = false
  master_password = random_password.password.result

  # Group parameters
  #wlm_json_configuration = "[{\"query_concurrency\": 5}]"

  # DB Subnet Group Inputs
  subnet_ids = local.subnet_ids
  vpc_security_group_ids = [
      module.security_group.security_group_id,
      module.security_group_2.security_group_id
  ]
  publicly_accessible = true

  /*logging = {
    enable        = true
    bucket_name   = module.qmi-s3-bucket.bucket.s3_bucket_id
    s3_key_prefix = local.s3_prefix
  }*/

  # IAM Roles
  #cluster_iam_roles = ["arn:aws:iam::225367859851:role/developer"]

  tags = local.tags

}

