
Param(
    [string] $ExternalDomain = "qibtemp.qmi.qlik-poc.com",
    [int] $IsExternalAccess = 1,
    [string] $ImageReference = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/images/qliksense-qib-base-feb20-1"
)

Import-Module Qlik-Cli

### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {write-log -Message "Connecting to Qlik Sense Repository"; start-sleep 15} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )

$computerName=$env:COMPUTERNAME
$DeploymentEngineServiceName = "Qlik Insight Bot Deployment Engine Service"

Write-Log "QIB image used: $ImageReference"

if ( $ImageReference -Match "1.4.0" -or $ImageReference -Match "qliksense-qib-qa-base-june2020b" -or $ImageReference -Match "0.6.2020" ) {
    $DeploymentEngineServiceName = "Qlik Insight Bot Deployement Engine"
}

Function restartQIBServices {
    Write-Log "Starting QIB Services on $computerName"

    Set-Service -Name $DeploymentEngineServiceName -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot Duckling Service" -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot Narrative Service" -StartupType Automatic
    Set-Service -Name "Qlik Insight Bot NLU Service"  -StartupType Automatic

    Start-Service -InputObject "Qlik Insight Bot Duckling Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject "Qlik Insight Bot Narrative Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject "Qlik Insight Bot NLU Service"  -ErrorAction SilentlyContinue
    Start-Service -InputObject $DeploymentEngineServiceName  -ErrorAction SilentlyContinue

    Write-Log "Restart QIB ISS services"
    Import-Module WebAdministration
    Stop-WebSite 'portal'
    Stop-WebSite 'engineservice'
    Stop-WebSite 'nlpservice'
    Stop-WebSite 'managementservice'
    Start-WebSite 'engineservice'
    Start-WebSite 'nlpservice'
    Start-WebSite 'managementservice'
    Start-WebSite 'portal'
}

#Remove-Item –path "c:\ProgramData\Qlik Insight Bot\Logs\*" -Force

Write-Log "Exporting new certificates"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Windows"
Export-QlikCertificate -machineNames "localhost" -includeSecretsKey -exportFormat "Pem"

    

Write-Log "Copy new certs to QIB location"
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\root.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\server.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\server_key.pem" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate\qmi-qs-1927.pfx" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate\qmi-qs-qib-2ef3.pfx" -Force
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\localhost\client.pfx" -Destination "C:\ProgramData\Qlik Insight Bot\Certificate\$computerName.pfx" -Force

if ( $IsExternalAccess -eq 1 ) {
    ((Get-Content -path $PSScriptRoot\NodeConfiguration.json -Raw) -replace '{{EXTERNAL_DOMAIN}}',$ExternalDomain) | Set-Content -Path $PSScriptRoot\NodeConfiguration.json
    
    Write-Log "Whitelist extenal url in the 'qib' VP in Qlik Sense"
    Update-QlikVirtualProxy -id (Get-QlikVirtualProxy -filter "description eq 'qib'").id -websocketCrossOriginWhiteList $ExternalDomain | Out-Null
} else {
    ((Get-Content -path $PSScriptRoot\NodeConfiguration.json -Raw) -replace '{{EXTERNAL_DOMAIN}}',$computerName) | Set-Content -Path $PSScriptRoot\NodeConfiguration.json
}

((Get-Content -path $PSScriptRoot\NodeConfiguration.json -Raw) -replace '{{HOSTNAME}}',$computerName) | Set-Content -Path $PSScriptRoot\NodeConfiguration.json
Copy-Item "$PSScriptRoot\NodeConfiguration.json" -Destination "C:\ProgramData\Qlik Insight Bot\Qlik\NodeConfiguration.json" -Force

restartQIBServices

Stop-Service -InputObject $DeploymentEngineServiceName  -ErrorAction SilentlyContinue
Start-Service -InputObject $DeploymentEngineServiceName  -ErrorAction SilentlyContinue

Write-Log "Restart Qlik Sense Proxy and ServiceDispatcher"
Restart-Service QlikSenseProxyService -Force
Restart-Service QlikSenseServiceDispatcher -Force


