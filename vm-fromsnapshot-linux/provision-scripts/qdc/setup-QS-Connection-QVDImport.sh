#!/bin/bash

echo "Start setup-QVD-Connector_QDC.sh"
echo "This will configure Qlik Sense Connection in QDC for QVD Import"

# Install JQ for JSON parsing
echo "Install JQ for JSON parsing"
sudo yum install epel-release -y -q
sudo yum install jq -y -q

QS_HOST=$1
QDC_IP=$2

#export JWT=$(cat /root/jwt.save)
USER_AGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"

echo "QS_HOST=$QS_HOST"
echo ""

URL="https://$QDC_IP:8443/qdc/"
echo "--> Login to QDC: $URL"
 
## Login
curl $URL \
  -H "Connection: keep-alive" \
  -H "Cache-Control: max-age=0" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "User-Agent: $USER_AGENT" \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" \
  -H "Referer: https://$QDC_IP:8443/qdc/login" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  --compressed \
  --insecure \
  -c qdcinit.cookie.txt

csrf=`awk 'NR == 5 {print $7}' qdcinit.cookie.txt`

echo "CSRF = $csrf"

URL="https://$QDC_IP:8443/qdc/j_spring_security_check"
echo "--> Login to QDC: $URL"
 
## Login
curl $URL \
  -H "Connection: keep-alive" \
  -H "Cache-Control: max-age=0" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "User-Agent: $USER_AGENT" \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" \
  -H "Referer: https://$QDC_IP:8443/qdc/login" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  --data "j_password=nvs2014%21&j_username=podium&_csrf=$csrf" \
  --compressed \
  --insecure \
  -b qdcinit.cookie.txt \
  -c qdc.cookie.txt


echo "Cookie: "
cat qdc.cookie.txt

csrf=`awk 'NR == 6 {print $7}' qdc.cookie.txt`
echo "CSRF = $csrf"

echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/test"
echo "--> Test Connector: $URL"
## Connector TEST
INSTALL_ID=$(curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Host: $QDC_IP:8443" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "X-XSRF-TOKEN: $csrf" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  --data-binary '{"name":"QlikSenseConnection1","username":"qlik-data-catalyst","hostname":"'$QS_HOST'","userDirectory":"QLIK-EXTERNAL-SERVICE","baseDirectoryPath":"/usr/local/qdc/data","installId":"","certificatesPath":"/usr/local/qdc/qlikpublish/certs","defaultLinuxMountPoint":"/usr/local/qdc/source"}'\
  --compressed \
  --insecure \
  -b qdc.cookie.txt)

INSTALL_ID=$(echo $INSTALL_ID | cut -d '"' -f 2)
echo "INSTALL_ID=$INSTALL_ID"


## Connector Save SAVE
echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/save"
echo "--> Save Connector: $URL"
SAVED=$(curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Host: $QDC_IP:8443" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  -H "X-XSRF-TOKEN: $csrf" \
  --data-binary '{"name":"QlikSenseConnection1","baseDirectoryPath":"/usr/local/qdc/data","certificatesPath":"/usr/local/qdc/qlikpublish/certs","hostname":"'$QS_HOST'","installId":"'$INSTALL_ID'","defaultLinuxMountPoint":"/usr/local/qdc/source","userDirectory": "QLIK-EXTERNAL-SERVICE","username":"qlik-data-catalyst"}'\
  --compressed \
  --insecure \
  -b qdc.cookie.txt)


#<<'COMMENTS'

ID=$(echo $SAVED | jq '.id')
echo "Next ID=$ID"

## Connector SYNC PATH
echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/refreshPaths/$ID"
echo "--> Sync Paths: $URL"
PATHS=$(curl $URL \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Host: $QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  -H "X-XSRF-TOKEN: $csrf" \
  --compressed \
  --insecure \
  -b qdc.cookie.txt)

echo $PATHS
dataBinary=$(echo $PATHS | jq '.[0]')
dataBin=$(echo $dataBinary | jq '.sourceName = "QVDsQMI"' -c) 

## Connector SET SOURCE NAME
echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/linuxPathAvailable/"$ID
echo "--> Set source name: $URL"
echo "Data --> $dataBin"
sleep 5
curl $URL \
  -X "POST" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Host: $QDC_IP:8443" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  -H "X-XSRF-TOKEN: $csrf" \
  --data-binary $dataBin \
  --compressed \
  --insecure \
  -b qdc.cookie.txt

sleep 5
echo "Wait 5 Sec."

## Connector Accept Path
echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/acceptSyncUpdate/"$ID
echo "--> Accept Path: $URL"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Host: $QDC_IP:8443" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  -H "X-XSRF-TOKEN: $csrf" \
  --data-binary $dataBin \
  --compressed \
  --insecure \
  -b qdc.cookie.txt  


#echo ""
#URL="https://$QDC_IP:8443/qdc/qsConnector/refreshPaths/"$ID
#echo "--> Refresh Path: $URL"
#curl $URL \
#  -H 'Connection: keep-alive' \
#  -H 'Accept: application/json, text/plain, */*' \
#  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36' \
#  -H 'Origin: https://$QDC_IP:8443' \
#  -H 'Referer: https://$QDC_IP:8443/qdc/' \
#  -H 'Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7' \
#  --compressed \
#  --insecure \
#  -b qdc.cookie.txt  


#echo "Get Job status"
#curl 'https://$QDC_IP:8443/qdc/qsConnector/getJobStatus/'$ID \
#  -H 'Connection: keep-alive' \
#  -H 'Accept: application/json, text/plain, */*' \
#  -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36' \
#  -H 'Referer: https://$QDC_IP:8443/qdc/' \
#  -H 'Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7' \
#  --compressed \
#  --insecure \
#  -b qdc.cookie.txt    


## QVD READ RUN
echo ""
URL="https://$QDC_IP:8443/qdc/qsConnector/runFullReloadAndSynch/$ID?isLoadData=true"
echo "--> Schedule Full Reload and load Data: $URL"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Content-Length: 0" \
  -H "Accept: application/json, text/plain, */*" \
  -H "User-Agent: $USER_AGENT" \
  -H "Host: $QDC_IP:8443" \
  -H "Origin: https://$QDC_IP:8443" \
  -H "Referer: https://$QDC_IP:8443/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  -H "X-XSRF-TOKEN: $csrf" \
  --compressed \
  --insecure \
  -b qdc.cookie.txt 
#COMMENTS

