echo "--- Installting Mysql...."
MYSQL_PASSWORD=$1
#Update the System
#yum -y update

#Get MySql install package
wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
rpm -Uvh mysql80-community-release-el7-3.noarch.rpm

#Import new Key
rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022

#Install MySql
yum -y -q install mysql-server

#Enable Service and start MySql
systemctl enable mysqld
systemctl start mysqld

#Get the default password
PASSWD=$(cat /var/log/mysqld.log | grep password | awk -F " " '{print $13}')

#Replace the tmp password with the default one
mysqladmin -u root -p$PASSWD password $MYSQL_PASSWORD

echo "--- Creating demo sample databases in Mysql...."

#Create a Demo DB and demo user. Grent user to DB
mysqladmin -u'root' -p$MYSQL_PASSWORD create 'sales'
mysqladmin -u'root' -p$MYSQL_PASSWORD create 'classic-models'
mysqladmin -u'root' -p$MYSQL_PASSWORD create 'sales-errors'
mysqladmin -u'root' -p$MYSQL_PASSWORD create 'classic-models-errors'

cp /home/qmi/scripts/mysql_sampledb_sql/user.sql /home/qmi/scripts/mysql_sampledb_sql/user_mod.sql 
sed -i "s/%MYSQL_DEMO_PASSWORD%/$MYSQL_PASSWORD/g" /home/qmi/scripts/mysql_sampledb_sql/user_mod.sql 
mysql -u root -p$MYSQL_PASSWORD < /home/qmi/scripts/mysql_sampledb_sql/user_mod.sql > /home/qmi/scripts/outsql.log

echo "--- Importing demo sample databases in Mysql...."

#Import databases
mysql -u root -p$MYSQL_PASSWORD sales < /home/qmi/scripts/mysql_sampledb_sql/sales.sql >> /home/qmi/scripts/outsql.log
mysql -u root -p$MYSQL_PASSWORD classic-models < /home/qmi/scripts/mysql_sampledb_sql/classic-models.sql >> /home/qmi/scripts/outsql.log
mysql -u root -p$MYSQL_PASSWORD sales-errors < /home/qmi/scripts/mysql_sampledb_sql/sales-errors.sql >> /home/qmi/scripts/outsql.log
mysql -u root -p$MYSQL_PASSWORD classic-models-errors < /home/qmi/scripts/mysql_sampledb_sql/classicmodels-errors.sql >> /home/qmi/scripts/outsql.log

#Enable external connection
echo "bind-address=0.0.0.0" >> /etc/my.cnf