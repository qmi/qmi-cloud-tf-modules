resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}


resource "azurerm_mariadb_server" "mariadb-server" {
  name = "qmi-mariadb-${var.provision_id}"
  location = var.location
  resource_group_name = var.resource_group_name

  administrator_login = var.admin_login
  administrator_login_password = random_password.password.result

  sku_name = var.sku-name
  version = var.db-version
 
  storage_mb = var.storage
  auto_grow_enabled = true

  
  backup_retention_days         = 7
  geo_redundant_backup_enabled  = false
  public_network_access_enabled = true
  ssl_enforcement_enabled       = false

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
    ProvId = var.provision_id
  }
}

resource "azurerm_mariadb_database" "mariadb-db" {
  name                = "QlikDB"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mariadb_server.mariadb-server.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

/*
module "dummy-data" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/dummy"

  type      = "mysql"
  host      = "${azurerm_mariadb_server.mariadb-server.fqdn}:3306"
  username  = var.admin_login
  password  = nonsensitive(random_password.password.result)
  database  = ""
}
*/