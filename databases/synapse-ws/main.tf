locals {
  myRegex = "/[^[:alpha:]]/"
  container_name1 = split(" ", lower(var.user_id))
  np0 = replace(element(local.container_name1,0), local.myRegex, "")
  np1 = replace(element(local.container_name1,1), local.myRegex, "")
  container_n1 = substr(local.np0, 0, 3)
  container_n2 = substr(local.np1, 0, 1)
  container_n3 = substr(strrev(local.np1), 0, 1)
  container_name = "${local.container_n1}${local.container_n2}${local.container_n3}"
  container_name_upper = upper(local.container_name)

}

resource "random_id" "randomServerId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "azurerm_storage_data_lake_gen2_filesystem" "synapsefs" {
  name               = "${local.container_name}synapsefilesystem"
  storage_account_id = var.storage_account_id
}

resource "azurerm_synapse_workspace" "synapsews" {
  name                                 = "qliksyn${random_id.randomServerId.hex}"
  resource_group_name                  = var.resource_group_name
  location                             = var.location
  storage_data_lake_gen2_filesystem_id = azurerm_storage_data_lake_gen2_filesystem.synapsefs.id
  sql_administrator_login              = var.sql_administrator_login
  sql_administrator_login_password     = var.sql_administrator_login_password


  managed_virtual_network_enabled      = true

  identity {
    type = "SystemAssigned"
  }

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id != null? var.user_id : null
    Owner    = var.user_id != null? var.user_id : null
    ADAAutomation = "SQLDWSuspend"
    ProvId = var.provision_id
  }

}

resource "azurerm_synapse_sql_pool" "db" {
  name                 = "${local.container_name_upper}DEMOPOOL"
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  sku_name             = var.pool_sku_name
  create_mode          = "Default"
  storage_account_type = "GRS"
  

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id != null? var.user_id : null
    Owner = var.user_id != null? var.user_id : null
    ADAAutomation = "SQLDWSuspend"
    ProvId = var.provision_id
  }

}

resource "azurerm_synapse_role_assignment" "sqladministrator_aor" {
  
  depends_on = [
      azurerm_synapse_firewall_rule.azureservices,
      
      azurerm_synapse_firewall_rule.fw-a-rule1,
      azurerm_synapse_firewall_rule.fw-a-rule2,
      azurerm_synapse_firewall_rule.fw-a-rule3
  ]

  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  role_name            = "Synapse Administrator"
  principal_id         = "c583b5aa-2844-4baf-b8bf-b6a1ae0b1520" 

}

resource "azurerm_synapse_role_assignment" "sqladministrator" {
  
  count = (var.user_oid != null && var.user_oid != "c583b5aa-2844-4baf-b8bf-b6a1ae0b1520") ? 1 : 0

  depends_on = [
      azurerm_synapse_firewall_rule.azureservices,
      
      azurerm_synapse_firewall_rule.fw-a-rule1,
      azurerm_synapse_firewall_rule.fw-a-rule2,
      azurerm_synapse_firewall_rule.fw-a-rule3
  ]

  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  role_name            = "Synapse Administrator"
  principal_id         = var.user_oid != null? var.user_oid : "c583b5aa-2844-4baf-b8bf-b6a1ae0b1520" 

}

resource "azurerm_role_assignment" "role-user-reader-for-synapse" {

  count = (var.user_oid != null && var.user_oid != "c583b5aa-2844-4baf-b8bf-b6a1ae0b1520") ? 1 : 0

  scope                = azurerm_synapse_workspace.synapsews.id
  role_definition_name = "Reader"
  principal_id         = var.user_oid

}

