resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = nonsensitive(random_password.password.result)
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  network_interface_ids = [ module.qmi-nic.id ]

  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_id =  var.image_reference

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }
}

module "win-common" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//win-common"

  depends_on = [
    azurerm_windows_virtual_machine.vm
  ]
  
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = local.admin_password  
}

/*
resource "null_resource" "post-win-common" {
    
    depends_on = [
      module.win-common
    ]

    provisioner "remote-exec" {
      connection {
        type     = "winrm"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = local.admin_password
        port     = 5985
        insecure = true
        use_ntlm = false
        https    = false
        timeout  = "3m"
      }

      inline = [
        "powershell.exe -File C:/provision/win-common/onedrive-install.ps1",      
      ]
    }
}
*/