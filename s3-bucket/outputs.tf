output "bucket" {
  value = module.s3_bucket
}

output "iam_name" {
  value = module.iam_user.this_iam_user_name
}

output "iam_access_key" {
  value = module.iam_user.this_iam_access_key_id
}

output "iam_access_secret" {
  value = nonsensitive(module.iam_user.this_iam_access_key_secret)
}

output "iam_role_arn" {
  value = var.tenant_id!=null?  aws_iam_role.qlik_s3[0].arn : null
}
