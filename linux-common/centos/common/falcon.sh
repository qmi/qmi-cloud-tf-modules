#!/bin/bash

echo "--- Executing: $0 $@"

BASEDIR=$(dirname "$0")

cVer=`rpm -E %{rhel}`
binary='falcon-sensor-5.29.0-9403.el7.x86_64.rpm'
if [ $cVer -eq '8' ]; then
    binary='falcon-sensor-5.27.0-9104.el8.x86_64.rpm'
fi

echo "--- Installing CrowdStrike falcon agent --> $binary"
echo "CID=$1"

wget --quiet https://d7ipctdjxxii4.cloudfront.net/others/$binary -O $BASEDIR/$binary

if ! rpm -qa | grep -qw falcon-sensor; then
    sudo yum -y --quiet install $BASEDIR/$binary
fi
sudo /opt/CrowdStrike/falconctl -s -f --cid=$1
sudo systemctl start falcon-sensor 
echo "CrowdStrike falcon agent done!"
