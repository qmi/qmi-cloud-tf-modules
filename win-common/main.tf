data "azurerm_key_vault_secret" "falconcid" {
  name         = "falcon-cid"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "tenable-key" {
  name         = "tenable-key"
  key_vault_id = var.key_vault_id
}

locals {
  falcon_cid     = nonsensitive(data.azurerm_key_vault_secret.falconcid.value)
  tenable_key   = nonsensitive(data.azurerm_key_vault_secret.tenable-key.value)
}

resource "null_resource" "post-win-vm" {
    
    provisioner "file" {
        connection {
            type     = "winrm"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            port     = 5985
            insecure = true
            use_ntlm = false
            https    = false
            timeout  = "30m"
        }
        source      = "${path.module}/main"
        destination = "C:/provision/win-common"
    }

    provisioner "remote-exec" {
        connection {
            type     = "winrm"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            port     = 5985
            insecure = true
            use_ntlm = false
            https    = false
            timeout  = "30m"
        }

        inline = [
            "powershell.exe -NoProfile -File C:/provision/win-common/prep-files.ps1",
            "powershell.exe -NoProfile -File C:/provision/win-common/resize-disk-tomax.ps1",
            "powershell.exe -NoProfile -File C:/provision/win-common/disable-ie-sec.ps1",
        ]
    }
}

resource "null_resource" "carbonblack" {

    count = var.carbonblack? 1 : 0
    
    depends_on = [
        null_resource.post-win-vm
    ]

    provisioner "remote-exec" {
        connection {
            type     = "winrm"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            port     = 5985
            insecure = true
            use_ntlm = false
            https    = false
            timeout  = "30m"
        }

        inline = [
            "powershell.exe -NoProfile -File C:/provision/win-common/carbonblack-uninstall.ps1 -CCID \"${local.falcon_cid}\"",
        ]
    }
}

resource "null_resource" "tenable" {

    count = var.tenable? 1 : 0
    
    depends_on = [
        null_resource.post-win-vm,
        null_resource.carbonblack
    ]

    provisioner "remote-exec" {
        connection {
            type     = "winrm"
            host     = var.private_ip_address
            user     = var.admin_username
            password = var.admin_password
            port     = 5985
            insecure = true
            use_ntlm = false
            https    = false
            timeout  = "30m"
        }

        inline = [
            "powershell.exe -NoProfile -File C:/provision/win-common/tenable-install.ps1 -KEY \"${local.tenable_key}\"",
        ]
    }
}