### Variables
variable "prefix" {
    default = "QMI-QDC"
}

variable "location" {
    default = "East US"
}

variable "image_reference" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/galleries/QMICloud/images/QDC/versions/4.5.0"
}

variable "vm_type" {
  default = "Standard_D4s_v3"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "resource_group_name" { 
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
  default = null
}

variable "key_vault_id" {
  default = "/subscriptions/73c75d40-8c7d-45cf-b4f9-afdb210da92d/resourceGroups/QMI-infra/providers/Microsoft.KeyVault/vaults/qmicloud-secrets"
}

variable "vm_subnet_id" {
  
}



