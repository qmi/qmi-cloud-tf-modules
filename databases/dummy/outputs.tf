output "dbs" {
  value       = var.type == "postgres"? "'AdventureWorks' and 'dvdrental'": (var.type == "mysql")? "'sakila', 'employees' and 'classicmodels'" : (var.type == "mssql")? "'BikeStores' and 'Northwind'" : null
}