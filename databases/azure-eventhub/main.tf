locals {
  myRegex = "/[^[:alpha:]]/"
  splitLower = split(" ", lower(var.user_id))
  np0 = replace(element(local.splitLower,0), local.myRegex, "")
  np1 = replace(element(local.splitLower,1), local.myRegex, "")
  container_n1 = substr(local.np0, 0, 3)
  container_n2 = substr(local.np1, 0, 1)
  container_n3 = substr(strrev(local.np1), 0, 1)
  
  scnamelower = "${local.container_n1}${local.container_n2}${local.container_n3}"

}


resource "azurerm_eventhub_namespace" "ehbnamespace" {
  name                  = "qlik${local.scnamelower}ns"
  location              = var.location
  resource_group_name   = var.resource_group_name
  sku                   = "Standard"
  capacity              = 1
  auto_inflate_enabled  = true
  maximum_throughput_units = 2

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner    = var.user_id
    ProvId = var.provision_id
  }

  network_rulesets {
      
      default_action = "Deny"
      trusted_service_access_enabled = true
      
      virtual_network_rule {
          subnet_id = var.subnet_id
      }
  }
}

resource "azurerm_eventhub" "attrep_apply_exceptions" {
  name                = "attrep_apply_exceptions"
  namespace_name      = azurerm_eventhub_namespace.ehbnamespace.name
  resource_group_name = var.resource_group_name
  partition_count     = 1
  message_retention   = 1
}

resource "azurerm_eventhub" "hr_employees" {
  name                = "hr.employees"
  namespace_name      = azurerm_eventhub_namespace.ehbnamespace.name
  resource_group_name = var.resource_group_name
  partition_count     = 1
  message_retention   = 1
}

resource "azurerm_eventhub" "hr_jobs" {
  name                = "hr.jobs"
  namespace_name      = azurerm_eventhub_namespace.ehbnamespace.name
  resource_group_name = var.resource_group_name
  partition_count     = 1
  message_retention   = 1
}

resource "azurerm_eventhub" "metadatahub" {
  name                = "metadatahub"
  namespace_name      = azurerm_eventhub_namespace.ehbnamespace.name
  resource_group_name = var.resource_group_name
  partition_count     = 1
  message_retention   = 1
}