
Param(
    [string] $NewName,
    [switch] $NotApply = $False,
    [switch] $RestartAfter = $False
)



if ($NotApply){
    Write-Host "Not renaming machine"
} else {
    Write-Host "Machine is going to be renamed as $NewName"
    if ($RestartAfter) {
        Write-Host "Machine is going to be renamed as $NewName. Then it will restart."
        Rename-Computer -NewName $NewName -PassThru -Force -Restart
    } else {
        Write-Host "Machine is going to be renamed as $NewName."
        Write-Host "Machine won't restart now. Please restart afterwards for rename to take effect."
        Rename-Computer -NewName $NewName -PassThru -Force
    }
    
}