data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

locals {
  cert_password = nonsensitive(data.azurerm_key_vault_secret.cert-password.value)
}

# Install and configure Compose
resource "null_resource" "install" {

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = var.vm_private_ip_address
      user     = var.vm_admin_username
      password = var.vm_admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }
    source      = "${path.module}/main"
    destination = "C:/provision/compose-install"
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = var.vm_private_ip_address
      user     = var.vm_admin_username
      password = var.vm_admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/provision/compose-install/prep-files.ps1",
      "powershell.exe -File C:/provision/compose-install/di-compose-getBinary.ps1 -url ${var.download_url}",
      "powershell.exe -File C:/provision/compose-install/di-compose-install.ps1 -url ${var.download_url}",
      "powershell.exe -File C:/provision/compose-install/di-compose-setlicense.ps1 -version ${var.c_version}",
      "powershell.exe -File C:/provision/compose-install/di-compose-installQMICertificate.ps1 -CertPwd \"${local.cert_password}\" -version ${var.c_version}",
    ]

  }
}