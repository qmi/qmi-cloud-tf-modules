variable "resource_group_name" {
}

variable "location" {

}

variable "storage_account_id" {
}

variable "subnet_id" {
    default = null
}

variable "pool_sku_name" {
    default = "DW100c"
}

variable "user_id" {
    default = null
}

variable "user_oid" {
    default = null
}

variable "sql_administrator_login" {
    default = "scdemoadmin"
}

variable "sql_administrator_login_password" {
    default = "Attunity123123123"
}

variable "provision_id" {
    default = null
}