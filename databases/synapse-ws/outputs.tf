output "sqlserver_id" {
    value = azurerm_synapse_workspace.synapsews.id
}

output "sqlserver_name" {
    value = azurerm_synapse_workspace.synapsews.name
}

output "sqlserver_fully_qualified_domain_name" {
    value = azurerm_synapse_workspace.synapsews.connectivity_endpoints.sql
}

output "connectivity_endpoints" {
    value = azurerm_synapse_workspace.synapsews.connectivity_endpoints
}

output "dbname" {
    value = azurerm_synapse_sql_pool.db.name
}

output "administrator_login" {
    value = var.sql_administrator_login
}

output "administrator_login_password" {
    value = nonsensitive(var.sql_administrator_login_password)
}

output "principal_id" {
  value = azurerm_synapse_workspace.synapsews.identity.0.principal_id
}


