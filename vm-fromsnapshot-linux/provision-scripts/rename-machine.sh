#!/bin/bash

echo "--- Renaming machine to $1"
echo "--- Restart after renamed: $2"
hostnamectl set-hostname $1

if [ "$2" = "YES" ]; then 
    echo "--- Linux machine will restart in 1 minute...." 
    shutdown -r +1
else
    echo "--- No need to restart linux machine."  
fi;