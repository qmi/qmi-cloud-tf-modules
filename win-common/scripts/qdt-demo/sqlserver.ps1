Write-Host "--- Installing SQL Server Express...."

$ProgressPreference = 'SilentlyContinue'

choco install --no-progress  sql-server-express -ia "/SECURITYMODE=SQL /SAPWD=Qlik1234" -y #--version=2017.20190916 
choco install --no-progress  sql-server-management-studio -y

# $ENV:PATH="$ENV:PATH;C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn"
$ENV:PATH="$ENV:PATH;C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn"

New-Item -ItemType Directory -Force -Path C:\Temp | Out-Null
New-Item -ItemType Directory -Force -Path C:\Samples | Out-Null

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Set-Location c:\Temp

# write-log "Installing AdventureWorks"
# Invoke-WebRequest -Uri https://github.com/Microsoft/sql-server-samples/releases/download/adventureworks/AdventureWorks-oltp-install-script.zip -OutFile c:\Temp\AdventureWorks-oltp-install-script.zip
# C:\ProgramData\chocolatey\tools\7z.exe x AdventureWorks-oltp-install-script.zip -oC:\Samples\AdventureWorks\
# cd C:\Samples\AdventureWorks
# SQLCMD.exe -S localhost\SQLEXPRESS -E -i C:\Samples\AdventureWorks\instawdb.sql 

Write-Host "Installing AdventureWorksDW"
Invoke-WebRequest -Uri https://github.com/Microsoft/sql-server-samples/releases/download/adventureworks/AdventureWorksDW-data-warehouse-install-script.zip -OutFile c:\Temp\AdventureWorksDW-data-warehouse-install-script.zip 
C:\ProgramData\chocolatey\tools\7z.exe x c:\Temp\AdventureWorksDW-data-warehouse-install-script.zip -oC:\Samples\AdventureWorksDW
Set-Location C:\Samples\AdventureWorksDW
Write-Output "SET QUOTED_IDENTIFIER ON
go
" > C:\Samples\AdventureWorksDW\instawdbdw2.sql
cat  C:\Samples\AdventureWorksDW\instawdbdw.sql >> C:\Samples\AdventureWorksDW\instawdbdw2.sql
& "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\SQLCMD.exe" -S localhost\SQLEXPRESS -E -i C:\Samples\AdventureWorksDW\instawdbdw2.sql 

Write-Host "Installing AdventureWorksLT"
Invoke-WebRequest -Uri https://github.com/Microsoft/sql-server-samples/releases/download/adventureworks/AdventureWorksLT2019.bak -OutFile c:\Temp\AdventureWorksLT2019.bak
Write-Output "USE [master]
RESTORE DATABASE [AdventureWorksLT2019] 
FROM  DISK = N'c:\Temp\AdventureWorksLT2019.bak' 
WITH  FILE = 1,  
MOVE N'AdventureWorksLT2012_Data' TO N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AdventureWorksLT2019.mdf',  
MOVE N'AdventureWorksLT2012_Log' TO N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\AdventureWorksLT2019_log.ldf',
NOUNLOAD,  STATS = 5
GO
"> C:\Temp\AdventureWorksLT2019.sql
& "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\SQLCMD.exe" -S localhost\SQLEXPRESS -E -i C:\Temp\AdventureWorksLT2019.sql


#choco install sql2012.nativeclient

& "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\SQLCMD.exe" -S localhost\SQLEXPRESS -E -Q "Create Login qmi with PASSWORD=N'Qlik1234', DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;"
& "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\170\Tools\Binn\SQLCMD.exe" -S localhost\SQLEXPRESS -E -Q "EXEC master..sp_addsrvrolemember @loginame = N'qmi', @rolename = N'sysadmin';"
#sqlcmd.exe -S localhost\SQLEXPRESS -E -d master -Q "EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'LoginMode', REG_DWORD, 2"

Restart-Service 'MSSQL$SQLEXPRESS'
#sqlcmd.exe -S localhost\SQLEXPRESS -E -d master -Q "xp_readerrorlog 0, 1, N'Server is listening on'"

Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force
Install-Module -Name SqlServer -Force
Import-Module "SqlServer"

# Modify TCP/IP properties to enable an IP address
# https://stackoverflow.com/questions/9138172/enable-tcp-ip-remote-connections-to-sql-server-express-already-installed-databas

Get-CimInstance -Namespace root/Microsoft/SqlServer/ComputerManagement15 -ClassName ServerNetworkProtocol -Filter "InstanceName = 'SQLEXPRESS' and ProtocolName = 'Tcp'" |Invoke-CimMethod -Name SetEnable
$properties = Get-CimInstance -Namespace root/Microsoft/SqlServer/ComputerManagement15 -ClassName ServerNetworkProtocolProperty -Filter "InstanceName='SQLEXPRESS' and ProtocolName = 'Tcp' and IPAddressName='IPAll'"
$properties | Where-Object { $_.PropertyName -eq 'TcpPort' } | Invoke-CimMethod -Name SetStringValue -Arguments @{ StrValue = '1433' }
$properties | Where-Object { $_.PropertyName -eq 'TcpPortDynamic' } | Invoke-CimMethod -Name SetStringValue -Arguments @{ StrValue = '' }

# Restart SQL Server

Restart-Service 'MSSQL$SQLEXPRESS'

#Enabling SQL Server Ports
Write-Host "Opening firewall port 1433"
New-NetFirewallRule -DisplayName "SQL Server" -Direction Inbound -Protocol TCP -LocalPort 1433 -Action allow

Write-Host "--- SQL Server Express Completed"