variable "cluster_name" {
    default = null
}

variable "storage_account_name" {
}

variable "storage_account_accesskey" {
}

variable "user_id" {  
}

variable "user_email" {
}

variable "spark_version" {
    default = "10.4.x-scala2.12"
}

variable "node_type_id" {
    default = "Standard_DS3_v2"
}

variable "sku" {
    default = "standard"
}

variable "app_reg_id" {
  default = null
}

variable "app_reg_secret" {
  default = null
}

variable "cluster_size" {
  default = "Small"
}

variable "provision_id" {
  default = null
}