output "gateway-creds" {
  value = {
    username = "acctestusrgw"
    password = nonsensitive(random_password.password1.result)
  }
}

output "cluster-creds" {
  value = {
    username = "acctestusrvm"
    password = nonsensitive(random_password.password2.result)
  }
}

output "https_endpoint" {
    value = azurerm_hdinsight_hadoop_cluster.example.https_endpoint
}

output "ssh_endpoint" {
  value = azurerm_hdinsight_hadoop_cluster.example.ssh_endpoint
}


output "Azure_Active_Directory_Tenant_ID" {
  value = "c21eeb5f-f5a6-44e8-a997-124f2f7a497c"
}

output "Azure_Application_Registration_Client_ID" {
  value = var.dbricks_app_registration_application_id
}

output "Azure_Application_Registration_Secret" {
  value = "i3F8Q~FxhoyOP1-4r9sstaohnjxXaf~ulhVJFav_"
}


output "adls_StorageAccount-Name" {
  value = azurerm_storage_account.example.name
}

output "adls_StorageAccount-ContainerName" {
  value = azurerm_storage_container.example.name
}

output "adls_StorageAccount-AccessKey" {
  value = nonsensitive(azurerm_storage_account.example.primary_access_key)
}

output "adls_Azure_Active_Directory_Tenant_ID" {
  value = "c21eeb5f-f5a6-44e8-a997-124f2f7a497c"
}

output "adls_Azure_Application_Registration_Client_ID" {
  value = var.dbricks_app_registration_application_id
}

output "adls_Azure_Application_Registration_Secret" {
  value = "i3F8Q~FxhoyOP1-4r9sstaohnjxXaf~ulhVJFav_"
}