terraform {

  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.49.0"
    }
  }
}

module "qmi-aws-creds" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//aws-credentials"
}

provider "aws" {

  region     = var.region
  access_key = module.qmi-aws-creds.qmi-aws-access-key
  secret_key = module.qmi-aws-creds.qmi-aws-access-secret

  alias = "myaws"

}

locals {
  vpc_security_group_ids = (var.region == "eu-west-1")? var.vpc_security_group_ids_eu : (var.region == "us-east-1")? var.vpc_security_group_ids_us : var.vpc_security_group_ids_ap
  subnet_ids = (var.region == "eu-west-1")? var.subnet_ids_eu : (var.region == "us-east-1")? var.subnet_ids_us : var.subnet_ids_ap
  ami = (var.region == "eu-west-1")? var.ami_eu : (var.region == "us-east-1")? var.ami_us : var.ami_ap
}


resource "aws_eip" "lb" {

  provider = aws.myaws

  instance = module.ec2_cluster.id[0]
  vpc      = true
}

module "ec2_cluster" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  providers = {
    aws = aws.myaws
  }

  version = "~> 2.0"

  name                          = "fort-${var.provision_id}"
  instance_count                = 1

  ami                           = local.ami
  instance_type                 = var.instance_type
  key_name                      = var.key_name
  monitoring                    = false
  vpc_security_group_ids        = local.vpc_security_group_ids
  subnet_ids                    = local.subnet_ids
  #associate_public_ip_address   = true

  root_block_device = [{
    iops          = (var.volume_type == "io2")? 32000 : (var.volume_type == "io1")? 6400 : 16000
    volume_size   = "128"
    throughput    = (var.volume_type == "io2" || var.volume_type == "io1" )? null : "1000"
    volume_type   = (var.volume_type != null)? var.volume_type : "gp3"
  }]

  tags = {
    "24x7"          = ""
    "Owner"         = var.user_id
    "Cost Center"   = "3100"
    Deployment      = "QMI"
    ProvId          = var.provision_id
  }

}




