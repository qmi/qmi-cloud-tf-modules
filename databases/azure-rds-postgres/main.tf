resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_)"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}


resource "azurerm_postgresql_server" "postgresql-server" {
  name = "qmi-postgresql-${var.provision_id}"
  location = var.location
  resource_group_name = var.resource_group_name
 
  administrator_login          = var.admin_login
  administrator_login_password = random_password.password.result
 
  sku_name = var.postgresql-sku-name
  version  = var.postgresql-version
 
  storage_mb        = var.postgresql-storage
  auto_grow_enabled = true
  
  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  public_network_access_enabled     = true
  ssl_enforcement_enabled           = false
  ssl_minimal_tls_version_enforced  = "TLSEnforcementDisabled"

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    ProvId = var.provision_id
  }


}

resource "azurerm_postgresql_database" "postgresql-db" {
  name                = "QlikPostgresqlDB"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.postgresql-server.name
  charset             = "utf8"
  collation           = "English_United States.1252"
}