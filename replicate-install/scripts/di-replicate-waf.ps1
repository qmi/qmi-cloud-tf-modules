<#
Module:             di-replicate-waf
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - First release 21/04/2020
last updated:       21/04/2020
Intent: Setting all connection aliases for waf
#>

Param(
    [string] $PRIVATE_HOST,
    [string] $PUBLIC_HOST
)
Write-Log -Message "Starting di-replicate-waf.ps1"

Write-Log -Message "PRIVATE_HOST : $($PRIVATE_HOST)"
Write-Log -Message "PUBLIC_HOST : $($PUBLIC_HOST)"
$configFile = "C:\Program Files\Attunity\Replicate\data\ServiceConfiguration.xml"


Rename-Item -Path $configFile -NewName "ServiceConfiguration.save.xml"
echo '<ServiceConfiguration url="https://'$PUBLIC_HOST':443/attunityreplicate;http://'$PUBLIC_HOST':80/attunityreplicate" allowUnsafeProtocols="false" />' > $configFile


		

