
variable "subnet_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-VM-Deployments"
}

variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-CENTOS"
}

variable "location" {
  default = "East US"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_B2s"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "qmi"
}

variable "sku" {
  default = "7.5"
}

variable "publisher" {
  default = "OpenLogic"
}

variable "offer" {
  default = "CentOS"
}

variable "user_id" {
  default = null
}

variable "provId" {
  default = null
}

variable "is_24x7"{
  type = bool
  default = null
}

variable "shutdownTime"{
  default = null
}

variable "startupTime"{
  default = null
}

variable "isExternal" {
  type = bool
  default = false
}