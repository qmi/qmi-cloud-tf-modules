

resource "azurerm_mssql_virtual_network_rule" "sqlvnetrule" {

  count = var.subnet_id != null? 1 : 0
  
  name                = "vnet-rule-${local.provision_id}"
  server_id           = azurerm_mssql_server.sqlserver.id
  subnet_id           = var.subnet_id
}

resource "azurerm_mssql_virtual_network_rule" "sqlvnet_qmiinfra" {

  count = var.envbranch == "master" && var.location == "eastus" ? 1 : 0
  
  name                = "vnet-qmiinfra-${local.provision_id}"
  server_id           = azurerm_mssql_server.sqlserver.id
  subnet_id           = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-Infrastructure"
}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}


resource "azurerm_mssql_firewall_rule" "fw_rule" {
  for_each = module.fw-ips.ips

  name                = each.key
  server_id           = azurerm_mssql_server.sqlserver.id
  start_ip_address    = each.value.0
  end_ip_address      = each.value.1

}

module "dummy-data" {

  count = var.dummydata != null? 1 : 0

  depends_on = [ azurerm_mssql_firewall_rule.fw_rule ]
  
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/dummy"

  type      = "mssql"
  host      = azurerm_mssql_server.sqlserver.fully_qualified_domain_name
  username  = var.sql_administrator_login
  password  = nonsensitive(local.sql_administrator_login_password)
  database  = ""
}