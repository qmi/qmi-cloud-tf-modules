 output "db_username" {
  description = "The master username for the database"
  value       = (local.aurora)? nonsensitive(module.aurora_rds_instance[0].cluster_master_username) : nonsensitive(module.common_rds_instance[0].db_instance_username)
} 

 output "db_instance_password" {
  description = "The database password (this password may be old, because Terraform doesn't track it after initial creation)"
  value       = nonsensitive(random_password.password.result)
}

 output "db_instance_port" {
  description = "The database port"
  value       = (local.aurora)? module.aurora_rds_instance[0].cluster_port : module.common_rds_instance[0].db_instance_port
}

output "db_instance_endpoint" {
  description = "The connection endpoint"
  value       = (local.aurora)? module.aurora_rds_instance[0].cluster_endpoint : module.common_rds_instance[0].db_instance_endpoint
}

output "db_instance_id" {
  value       = (local.aurora)? null : "${var.engine}${var.provision_id}"
}

output "db_instance_name" {
  value       = local.name
}

output "dummy_data_databases_available" {
   value = var.dummydata != null && local.type != null? module.dummy-data[0].dbs : null
}
