<#
Module:             di-replicate-install
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - First release 21/04/2020
last updated:       21/04/2020
Intent: Install the selected version of Attunity Replicate
#>

Param(
    [string] $url
)

$issFile="Replicate_install.iss"

if ( $url -Match "2021" ) {
	$issFile="Replicate_install_2021.iss"
}

if ( $url -Match "2022" ) {
	$issFile="Replicate_install_2022.iss"
}

Import-Module $PSScriptRoot\q-helper.psm1 | Out-Null

$fileName = $url.Substring($url.LastIndexOf("/") + 1)

$bin = "$PSScriptRoot\binaries\Attunity"

if (Test-Path "$($bin)\Replicate_silent_x64_install.log") {
	Remove-Item -Path "$($bin)\Replicate_silent_x64_install.log" -Force
}

if (Test-Path "C:\Program Files\Attunity\Replicate") {
	Remove-Item -Path "C:\Program Files\Attunity\Replicate" -Recurse -Force
}


Write-Host "Using ISS file: $issFile"

If ((Test-Path $bin\$fileName))
{
	Write-Host "Installing Attunity Replicate Server from $($bin)\$($fileName)"
	$fileNoExtension = [IO.Path]::GetFileNameWithoutExtension("$bin\$fileName")
	Expand-Archive $bin\$fileName -DestinationPath $bin\$fileNoExtension -Force
	$exec = Get-ChildItem $bin\$fileNoExtension\*.exe | Select-Object -ExpandProperty Name
    #Write-Host "Exec: $($exec)"
	$ReplicateInstall = "$($bin)\$($fileNoExtension)\$($exec) /s /f1$($PSScriptRoot)\$($issFile) /f2$($bin)\Replicate_silent_x64_install.log"
	Write-Host "Run Replicate silent installation : $($ReplicateInstall)"
    Invoke-Expression $ReplicateInstall
    while (!(Test-Path "$($bin)\Replicate_silent_x64_install.log")) { 
        Write-Host "[Waiting Replicate to be installed] ..."
        Start-Sleep 2
    }
	$ReplicateResults = Get-IniFile "$($bin)\Replicate_silent_x64_install.log"
	$testResult = $ReplicateResults.ResponseResult.ResultCode
	Write-Host "Installation return code : $($testResult)"

}
else
{
    Write-Host "No Binary found: $($bin)\$($fileName)"
}
		

