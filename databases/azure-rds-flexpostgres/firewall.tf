/*resource "azurerm_postgresql_flexible_server_firewall_rule" "all-azure-services" {
  name                = "AllAzureServices"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}*/


resource "azurerm_postgresql_flexible_server_firewall_rule" "fw-a-rule1" {
  name                = "a1"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  start_ip_address    = "52.249.189.38"
  end_ip_address      = "52.249.189.38"
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "fw-a-rule2" {
  name                = "a2"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  start_ip_address    = "13.67.39.86"
  end_ip_address      = "13.67.39.86"
}

resource "azurerm_postgresql_flexible_server_firewall_rule" "fw-a-rule3" {
  name                = "a3"
  server_id = azurerm_postgresql_flexible_server.postgresql-server.id
  start_ip_address    = "20.67.110.207"
  end_ip_address      = "20.67.110.207"
}