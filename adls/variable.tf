variable "resource_group_name" {
}

variable "location" {
  description = "The Azure Region in which the resources in this example should exist"
  default = "East US"
}

variable "storage_account_name" {
  default = null
}

variable "container_name" {
  default = null
}

variable "tags" {
    default = null
}

variable "tpm_app_registration_principal_id" {
    description = "tpm"
    default = "163a72e3-8ce3-4e33-baae-954383f87e3e"
}

variable "dbricks_app_registration_principal_id" {
    description = "databricks-qmi"
    default = "efeee17c-d2b3-4e7c-a163-9995b7d281e2"
}

variable "dbricks_app_registration_application_id" {
    description = "databricks-qmi"
    default = "9ccb0d99-3bba-4695-aa47-df77bf512084"
}

variable "principal_id_storage_blob_contributor" {
  default = null
}

variable "sa_config_type" {
  default = "1"
}

variable "account_replication_type" {
  default = null
}

variable "catalog_access_connector" {
  default = {
    eu = "qmi-databricks-accessconnector-eu"
    sea = "qmi-databricks-accessconnector-sea"
    us = null
  }
}