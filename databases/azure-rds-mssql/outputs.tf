output "sqlserver_id" {
    value = azurerm_mssql_server.sqlserver.id
}

output "sqlserver_name" {
    value = azurerm_mssql_server.sqlserver.name
}

output "sqlserver_fully_qualified_domain_name" {
    value = azurerm_mssql_server.sqlserver.fully_qualified_domain_name
}

output "dbname" {
    value = azurerm_mssql_database.db.name
}

output "administrator_login" {
    value = var.sql_administrator_login
}

output "administrator_login_password" {
    value = nonsensitive(local.sql_administrator_login_password)
}

output "principal_id" {
  value = azurerm_mssql_server.sqlserver.identity.0.principal_id
}

output "dummy_data_databases_available" {
  value = var.dummydata != null? module.dummy-data[0].dbs : null
}


