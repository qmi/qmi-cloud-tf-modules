terraform {

  required_version = ">= 0.14"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  provid5              = substr(var.provision_id, 0, 5)
  aurora               = (var.engine == "aurora-mysql") ? true : (var.engine == "aurora-postgresql") ? true : false
  vpc_id               = (var.region == "eu-west-1") ? var.vpc_id_eu : (var.region == "us-east-1") ? var.vpc_id_us : var.vpc_id_ap
  subnet_ids           = (var.region == "eu-west-1") ? var.subnet_ids_eu : (var.region == "us-east-1") ? var.subnet_ids_us : var.subnet_ids_ap
  
  name                 = (var.engine == "sqlserver-ex") ? null : (var.engine == "oracle-se2") ? "ora${local.provid5}" : "qmi${var.provision_id}"
  license              = (local.aurora == true) ? "general-public-license" : (var.engine == "mariadb") ? "general-public-license" : (var.engine == "postgres") ? "postgresql-license" : (var.engine == "mysql") ? "general-public-license" : "license-included"
  
  port                 = var.port[var.engine] 
  engine_version       = var.engine_version[var.engine]
  major_engine_version = var.major_engine_version[var.engine]
  family               = var.family[var.engine]

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    ProvID   = var.provision_id
    Name     = "qmi-${var.provision_id}"
    Owner    = var.user_id
    forced_destroy = var.forced_destroy
  }

  is_postgres = length(regexall("postgres", local.family)) > 0 ? true : false
  is_mysql = length(regexall("mysql", local.family)) > 0 ? true : false
}


module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}

module "security_group_2" {
  
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG2"
  description = "${var.provision_id}-SG2"
  vpc_id      = local.vpc_id


  # ingress

  ingress_cidr_blocks = module.fw-ips.cidr_blocks_others
  

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "RDS"

    },
  ]

  # egress

  egress_cidr_blocks = module.fw-ips.cidr_blocks_others

  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "RDS"

    },
  ]

  tags = local.tags
}

module "security_group" {
  
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG"
  description = "${var.provision_id}-SG"
  vpc_id      = local.vpc_id


  # ingress

  ingress_cidr_blocks = module.fw-ips.cidr_blocks
  

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "RDS"

    },
  ]

  # egress

  egress_cidr_blocks = module.fw-ips.cidr_blocks

  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "RDS"

    },
  ]

  tags = local.tags
}

module "common_rds_instance" {
  source  = "terraform-aws-modules/rds/aws"
  version = "= 6.1.1"

  count = local.aurora? 0 : 1

  identifier = "${var.engine}${var.provision_id}"

  engine               = var.engine
  engine_version       = local.engine_version
  family               = local.family               # DB parameter group
  major_engine_version = local.major_engine_version # DB option group
  instance_class       = var.instance_size
  allocated_storage    = var.storage
  storage_encrypted    = (var.engine == "sqlserver-ex")? false : true

  license_model = local.license

  db_name                = local.name
  username               = "qmirdsuser"
  password               = random_password.password.result
  manage_master_user_password = false
  port                   = local.port

  multi_az               = false
  subnet_ids             = local.subnet_ids
  vpc_security_group_ids = [
    module.security_group.security_group_id,
    module.security_group_2.security_group_id
  ]
  publicly_accessible    = true

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  backup_retention_period = 1
  skip_final_snapshot     = true
  deletion_protection     = false

  parameters = local.is_postgres? [
    {
      apply_method = "pending-reboot"
      name  = "rds.logical_replication"
      value = 1
    },
    {
      apply_method = "pending-reboot"
      name  = "max_wal_senders"
      value = 10
    },
    {
      apply_method = "pending-reboot"
      name  = "max_replication_slots"
      value = 10
    }
  ] : local.is_mysql? [{
      name  = "binlog_format"
      value = "row"
  }]: []

  tags = local.tags

}


resource "aws_rds_cluster_parameter_group" "pg-postgres" {

  count =  var.engine == "aurora-postgresql"? 1 : 0

  name        = "rds-cluster-pg-${var.provision_id}"
  family      = "aurora-postgresql14"
  description = "RDS aurora-postgresql14 cluster parameter group"

  parameter {
    apply_method = "pending-reboot"
    name  = "rds.logical_replication"
    value = 1
  }

  parameter {
    apply_method = "pending-reboot"
    name  = "max_wal_senders"
    value = 10
  }

  parameter {
    apply_method = "pending-reboot"
    name  = "max_replication_slots"
    value = 10
  }

}

resource "aws_rds_cluster_parameter_group" "pg-mysql" {

  count =  var.engine == "aurora-mysql"? 1 : 0

  name        = "rds-cluster-pg-${var.provision_id}"
  family      = "aurora-mysql8.0"
  description = "RDS aurora-mysql8.0 cluster parameter group"

  parameter {
    apply_method = "pending-reboot"
    name  = "binlog_format"
    value = "row"
  }

}

module "aurora_rds_instance" {

  depends_on = [  
    aws_rds_cluster_parameter_group.pg-postgres, 
    aws_rds_cluster_parameter_group.pg-mysql
  ]

  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "~> 8.3.1"

  count = local.aurora? 1 : 0

  name           = local.name
  engine         = var.engine
  engine_version = local.engine_version
  instance_class  = var.instance_size

  apply_immediately = true

  database_name = local.name

  instances       = { 1 = {} }

  autoscaling_enabled      = true
  autoscaling_min_capacity = 1
  autoscaling_max_capacity = 3

  vpc_id                 = local.vpc_id
  subnets                = local.subnet_ids
  create_security_group  = false
  vpc_security_group_ids = [
      module.security_group.security_group_id,
      module.security_group_2.security_group_id
  ]
  port                   = local.port
  publicly_accessible    = true

  master_username         = "qmirdsuser"
  manage_master_user_password = false
  master_password = random_password.password.result
  create_db_subnet_group = true


  backup_retention_period = 1
  skip_final_snapshot     = true
  deletion_protection     = false

  tags = local.tags

  db_cluster_parameter_group_name = "rds-cluster-pg-${var.provision_id}"

}

locals {
    
    type      = (var.engine == "mysql" || var.engine == "mariadb")? "mysql" :  (var.engine == "postgres" || var.engine == "aurora-postgres")? "postgres" : (var.engine == "sqlserver-ex")? "mssql" : null
    port4dummy  = (local.aurora)? module.aurora_rds_instance[0].cluster_port : module.common_rds_instance[0].db_instance_port
    host      = (local.aurora)? "${module.aurora_rds_instance[0].cluster_endpoint}:${local.port4dummy}" : module.common_rds_instance[0].db_instance_endpoint
    username  = (local.aurora)? nonsensitive(module.aurora_rds_instance[0].cluster_master_username) : nonsensitive(module.common_rds_instance[0].db_instance_username)
    password  = nonsensitive(random_password.password.result)
    database  = (var.engine == "postgres" || var.engine == "aurora-postgres")? "postgres" : local.name
}


module "dummy-data" {

  count = var.dummydata != null && local.type != null? 1 : 0

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/dummy"

  type      = local.type
  host      = local.host
  username  = local.username
  password  = local.password
  database  = local.database
}
