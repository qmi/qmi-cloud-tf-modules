[ "$(whoami)" = "root" ] && echo "Do not run as root, run as service user (e.g., qdc)" && exit 1
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-11.0.16.0.8-1.el7_9.x86_64

export JAVA_OPTS="$JAVA_OPTS -Xms1g -Xmx8g -Dpodium.conf.dir=$CATALINA_BASE/conf"
export JAVA_OPTS="$JAVA_OPTS -Djava.library.path=/lib64"
export JAVA_OPTS="$JAVA_OPTS -Dpodium.conf.dir=$CATALINA_BASE/conf"
export JAVA_OPTS="$JAVA_OPTS -Djavax.security.auth.useSubjectCredsOnly=false -Djava.security.egd=file:/dev/./urandom"
export JAVA_OPTS="$JAVA_OPTS -Dqdc.home=/usr/local/qdc"