resource "azurerm_synapse_firewall_rule" "azureservices" {
  name                 = "AllowAllWindowsAzureIps"
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  // https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/synapse_firewall_rule
  start_ip_address     = "0.0.0.0"
  end_ip_address       = "0.0.0.0"
}


resource "azurerm_synapse_firewall_rule" "fw-a-rule1" {

  name                = "az1"
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  start_ip_address    = "52.249.189.38"
  end_ip_address      = "52.249.189.38"

}

resource "azurerm_synapse_firewall_rule" "fw-a-rule2" {

  name                = "az2"
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  start_ip_address    = "13.67.39.86"
  end_ip_address      = "13.67.39.86"

}

resource "azurerm_synapse_firewall_rule" "fw-a-rule3" {

  name                = "az3"
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  start_ip_address    = "20.67.110.207"
  end_ip_address      = "20.67.110.207"

}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}

resource "azurerm_synapse_firewall_rule" "fw_rule" {
  for_each = module.fw-ips.ips2

  name                = each.key
  synapse_workspace_id = azurerm_synapse_workspace.synapsews.id
  start_ip_address    = each.value.0
  end_ip_address      = each.value.1

}