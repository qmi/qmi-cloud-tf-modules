Param(
    [string] $QDC_HOST = "QMI-QDC-SN",
    [string] $POSTGRES_CONN_PW = "postgres"
)

function New-Credential($u,$p) {
    $secpasswd = ConvertTo-SecureString $p -AsPlainText -Force
    return New-Object System.Management.Automation.PSCredential ($u, $secpasswd)
}

Write-Log -Message "Setting up QDC pre-requisites in Qlik Sense"

Import-Module Qlik-Cli

### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {write-log -Message "Connecting to Qlik Sense Repository..."; start-sleep 15} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )

### Add the Qlik local user to Qlik Sense
$json = (@{userId = "qlik-data-catalyst";
            userDirectory = "QLIK-EXTERNAL-SERVICE";
            name = "qlik-data-catalyst";
        } | ConvertTo-Json -Compress -Depth 10 )
Write-Log -Message "Adding Qlik Service user."
try {
    Invoke-QlikPost "/qrs/user" $json | Out-Null
} catch {
    Write-Log -Message $_.Exception.Message -Severity "Error"
}
try {
    Write-Log -Message "Set 'qlik-data-catalyst' as AuditAdmin"
    Update-QlikUser -id $(Get-QlikUser -filter "name eq 'qlik-data-catalyst'").id -roles AuditAdmin | Out-Null
} catch {
    Write-Log -Message $_.Exception.Message -Severity "Error"
}

# C:\QVDs SMB FOLDER
Write-Log -Message "Creating 'C:\QVDs' folder and set SMB"
New-Item "C:\QVDs" -type directory | Out-Null
New-SMBShare -Name "qvds" -Path "C:\QVDs" | Out-Null 
Grant-SmbShareAccess -Name qvds -AccountName Everyone -AccessRight Change -Force | Out-Null


# certs needed for qdc
Write-Log -Message "SMB Qlik Sense certificates folder" 
New-SMBShare -Name "certs" -Path "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates" | Out-Null
Grant-SmbShareAccess -Name certs -AccountName Everyone -AccessRight Read -Force | Out-Null


Write-Log -Message "Creating 'QVD Catalog' tag"
New-QlikTag -name "QVD Catalog" | Out-Null


# create podium user
Import-Module "Carbon"
Write-Log -Message "Adding 'podium' user to Windows system"
$cred = New-Credential "podium" $POSTGRES_CONN_PW
Install-CUser -Credential $cred | Out-Null


Write-Log -Message "Creating Qlik Sense data connection for C:\QVDs"
# ---- Workaround qlik-cli QS April 2020
# Qlik CLI New-QlikDataConnection fails with only 1 tag, that's why I create and use FakeTag too.
New-QlikTag -name "FakeTag" | Out-Null
$qvdsDC = New-QlikDataConnection -connectionstring "\\$($env:COMPUTERNAME)\qvds" -name "QVDs" -tags "QVD Catalog","FakeTag" -type "folder"
# ----

Write-Log -Message "Grant access to all user to this connection"
New-QlikRule -name "QVDs allow to all" -comment "Allow QVDs Connection to all users" -category "Security" -resourceFilter  "DataConnection_$($qvdsDC.id)" -actions 23 -rule '((user.name like "*"))' | Out-Null


Write-Log -Message "Setting firewall rules for QDC"
New-NetFirewallRule -DisplayName "QDC QVD Metadata" -Action allow -LocalPort 7007 -Protocol TCP | Out-Null
New-NetFirewallRule -DisplayName "pub2qlik" -Action allow -LocalPort 4243,4747 -Protocol TCP | Out-Null


Write-Log -Message "Creating 'podium_dist' postgres connection"
$cred = New-Credential "postgres" $POSTGRES_CONN_PW
$theString = "CUSTOM CONNECT TO ""provider=QvOdbcConnectorPackage.exe;driver=postgres;host=$QDC_HOST;port=5432;db=podium_dist;SSLMode=prefer;UseSystemTrustStore=false;ByteaAsLongVarBinary=0;TextAsLongVarchar=0;UseUnicode=1;FetchTSWTZasTimestamp=1;MaxVarcharSize=262144;UseDeclareFetch=1;Fetch=200;EnableTableTypes=1;MoneyAsDecimal=1;TimetzAsVarchar=1;allowNonSelectQueries=false;QueryTimeout=30;useBulkReader=false;maxStringLength=4096;"""
$podiumDistDC = New-QlikDataConnection -connectionstring $theString `
 -name podium_dist  -type 'QvOdbcConnectorPackage.exe' -Credential $cred 

Write-Log -Message "Grant access to all user to this connection"
New-QlikRule -name "Podium_Dist allow to all" -comment "Allow Podium_Dist Connection to all users" -category "Security" -resourceFilter  "DataConnection_$($podiumDistDC.id)" -actions 23 -rule '((user.name like "*"))' | Out-Null


Write-Log "Copy Sample QVDs into C:/QVDs folder"
Copy-Item $PSScriptRoot\*.qvd C:\QVDs
Copy-Item $PSScriptRoot\*.csv C:\QVDs

$x=1
$files = Get-ChildItem C:\QVDs\*.qvd|sort LastWriteTime
foreach ($file in $files) #{ echo $file }
{

    if ($x -lt 3) {
        Set-ItemProperty -Path $file -Name LastWriteTime -Value (get-date) 
        sleep 2
    }
    $x= $x +1
}

Write-Log "QDC Config Completed."