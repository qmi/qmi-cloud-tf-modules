variable "private_ip_address" {
}

variable "admin_username" {
}

variable "admin_password" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "carbonblack" {
  default = true
}

variable "tenable" {
  default = true
}