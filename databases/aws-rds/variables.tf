variable "region" {
  default = "us-east-1"
}

variable "provision_id" {
  
}

variable "user_id" {
}

variable "vpc_id_ap" {
  default = "vpc-22ee2844"
}

variable "vpc_id_eu" {
  default = "vpc-73f0500a"
}

variable "vpc_id_us" {
  default = "vpc-c079f5bd"
}

variable "subnet_ids_ap" {
  default = ["subnet-658aec3c", "subnet-e030eba8", "subnet-94309bf2"]
}

variable "subnet_ids_eu" {
  default = ["subnet-4d441b17", "subnet-95c22fde", "subnet-70938116"]
}

variable "subnet_ids_us" {
  default = ["subnet-4d26552b", "subnet-0414685b", "subnet-c97f7c84", "subnet-7f695c71", "subnet-96acd2b7", "subnet-88ab2cb9"]
}

variable "engine" {
  default = "mysql"
  description = "Accepted values are mysql, sqlserver-ex, postgres, oracle-se2, aurora-mysql, aurora-postgresql, mariadb"
}

variable "instance_size" {
  default = "db.t3.large"
}

variable "storage" {
  default = "100"
}

variable "engine_version" {
  type = map

  default = {
    "mysql"  = "8.0.36"
    "postgres"  = "14.12" 
    "oracle-se2"  = "19.0.0.0.ru-2023-04.rur-2023-04.r1"
    "sqlserver-ex" = "15.00.4236.7.v1"
    "mariadb" = "10.5"
    "aurora-mysql" = "8.0.mysql_aurora.3.05.2" #"5.7.mysql_aurora.2.11.2"
    "aurora-postgresql"  = "14.9"
  }
  
}

variable "major_engine_version" {
  type = map

  default = {
    "mysql"  = "8.0"  
    "postgres"  = "14"
    "oracle-se2"  = "19"
    "sqlserver-ex" = "15.00"
    "mariadb" = "10.5"
    # for script compatibility only
    "aurora-mysql" = "not_used"
    "aurora-postgresql"  = "not_used"
  }
  
}


variable "family" {
  type = map

  default = {
    "mysql"  = "mysql8.0"
    "postgres"  = "postgres14"
    "oracle-se2"  = "oracle-se2-19"
    "sqlserver-ex" = "sqlserver-ex-15.0"
    "mariadb" = "mariadb10.5"
    # for script compatibility only
    "aurora-mysql" = "not_used"
    "aurora-postgresql"  = "not_used"
  }
}

variable "port" {
  type = map

  default = {
    "mysql"  = "3306"
    "postgres"  = "5432"
    "oracle-se2"  = "1521"
    "sqlserver-ex" = "1433"
    "mariadb" = "3306"
    "aurora-mysql" = "3306"
    "aurora-postgresql"  = "5432"
  }
}

variable "dummydata" {
  default = null
}

variable "forced_destroy" {
  default = null
}
