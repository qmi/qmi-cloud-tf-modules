resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = nonsensitive(random_password.password.result)
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  network_interface_ids = [ module.qmi-nic.id ]

  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_id =  var.image_reference

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
      use_ntlm = false
      insecure = true
    }
    source      = "${path.module}/main"
    destination = "C:/tmp/provision"
  }
  
}

/*
resource "azurerm_windows_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  network_interface_ids = [ module.qmi-nic.id ]

  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = var.image_reference
    version   = "latest"
  }

  custom_data = filebase64("${path.module}/winrmconfig/winrm.ps1")

  winrm_listener {
      protocol = "Http" 
  }

  additional_unattend_content {
    setting = "AutoLogon"
    content      = "<AutoLogon><Password><Value>${local.admin_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${local.admin_username}</Username></AutoLogon>"
  }

  additional_unattend_content {
      setting      = "FirstLogonCommands"
      content      = file("${path.module}/winrmconfig/FirstLogonCommands.xml")
  }

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }
}

resource "azurerm_virtual_machine_extension" "prepfiles" {
  depends_on = [
    azurerm_windows_virtual_machine.vm
  ]

  name = "vm-extension-prepfiles"
  virtual_machine_id   = azurerm_windows_virtual_machine.vm.id
  
  publisher = "Microsoft.Compute"
  type = "CustomScriptExtension"
  type_handler_version = "1.10"
  auto_upgrade_minor_version  = true
  
  settings = <<SETTINGS
    {
        "fileUris": [
            "https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/vm-win/main/prep-files.ps1"
        ],
        "commandToExecute": "start powershell.exe -NoProfile -ExecutionPolicy unrestricted -File prep-files.ps1"
    }
    SETTINGS
}
*/

resource "null_resource" "vm-post" {

  depends_on = [
    azurerm_windows_virtual_machine.vm
  ]

  provisioner "local-exec" {
    command = "echo 'Waiting 20 seconds for PrepFiles to finish'; sleep 20;"
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
      use_ntlm = false
      insecure = true
    }

    inline = [
      "powershell.exe -NoProfile -File C:/tmp/provision/prep-files.ps1",
      "powershell.exe -NoProfile -File C:/provision/bootstrap.ps1"
    ]
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
      use_ntlm = false
      insecure = true
    }

    inline = [
      "powershell.exe -NoProfile -File C:/provision/q-user-setup.ps1"
    ]
  }

}


module "win-common" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//win-common"

  depends_on = [
    azurerm_windows_virtual_machine.vm,
    null_resource.vm-post

  ]
  
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = local.admin_password  
}

resource "null_resource" "post-win-common" {
    
    depends_on = [
      module.win-common
    ]

    provisioner "remote-exec" {
      connection {
        type     = "winrm"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = local.admin_password
        port     = 5985
        https    = false
        timeout  = "30m"
        use_ntlm = false
        insecure = true
      }

      inline = [
        "powershell.exe -NoProfile -File C:/provision/win-common/chrome-install.ps1",
        "powershell.exe -NoProfile -File C:/provision/win-common/onedrive-install.ps1",
        "powershell.exe -NoProfile -File C:/provision/win-common/vscode-install.ps1",
      ]
    }
}


