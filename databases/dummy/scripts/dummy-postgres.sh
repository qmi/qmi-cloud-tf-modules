#!/bin/bash

IN="$3"
arrIN=(${IN//:/ })
host=`echo ${arrIN[0]}` 

echo "---- Load Dummy Data -----"
echo "Username: $1"
echo "Password: $2"
echo "Host: $host"
echo "DB: $4"
echo "--------------------------"

echo "---- Install Adventureworks for Postgres ----"
cd /tmp
wget -q https://github.com/lorint/AdventureWorks-for-Postgres/archive/master.zip
unzip master.zip
cd AdventureWorks-for-Postgres-master/
wget -q https://github.com/microsoft/sql-server-samples/releases/download/adventureworks/AdventureWorks-oltp-install-script.zip
unzip AdventureWorks-oltp-install-script.zip

ruby update_csvs.rb

export PGPASSWORD=$2

psql -h $host -U $1 -d $4 -c "CREATE DATABASE \"AdventureWorks\";" 
psql -h $host -U $1 -d "AdventureWorks" < ./install.sql > /tmp/AdventureWorks_load.log


echo "---- Install DvdRental sample database for Postgres ----"
cd /tmp
curl https://gitlab.com/qmi/qmi-cloud-tf-modules/-/archive/master/qmi-cloud-tf-modules-master.zip?path=databases/dummy/scripts -o qmi-cloud-tf-modules-master-databases-dummy-scripts.zip
unzip -o qmi-cloud-tf-modules-master-databases-dummy-scripts.zip
cd qmi-cloud-tf-modules-master-databases-dummy-scripts/databases/dummy/scripts

psql -h $host -U $1 -d $4 -c "CREATE DATABASE \"dvdrental\";" 
psql -h $host -U $1 -d "dvdrental" < ./postgres/dump_dvdrental.sql > /tmp/dvdrental_load.log


rm -fr /tmp/*


