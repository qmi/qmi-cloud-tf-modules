 
Param(
    [string] $CertPwd,
    [string] $version = "gen2"
)

try {
    Write-Host "Install qmi_qlik-poc_com certificate on Windows"

    $ProgressPreference = 'SilentlyContinue'
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
    (New-Object System.Net.WebClient).DownloadFile("https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pfx", "$PSScriptRoot\wildcard_qmi_qlik-poc_com.pfx") 

    $secpasswd = ConvertTo-SecureString $CertPwd -AsPlainText -Force
    $sslCert = Import-PfxCertificate -FilePath $PSScriptRoot/wildcard_qmi_qlik-poc_com.pfx -CertStoreLocation Cert:\LocalMachine\My -Password $secpasswd
    $thumb=$($sslCert.Thumbprint)
    Write-Host "Set SSL qmi_qlik-poc_com for Compose"
    
    if ( $version -eq "c4dw" ) {
        Stop-Service AttunityComposeForDataWarehouses
        Start-Process -FilePath "C:\Program Files\Attunity\Compose for Data Warehouses\bin\ComposeCtl.exe" -ArgumentList "certificate clean" -Wait -NoNewWindow
        netsh http add sslcert ipport=0.0.0.0:443 certhash=$thumb appid='{4dc3e181-e14b-4a21-b022-59fc669b0914}'

        Start-Service AttunityComposeForDataWarehouses
    } else {
        Stop-Service QlikCompose
        Start-Process -FilePath "C:\Program Files\Qlik\Compose\bin\ComposeCtl.exe" -ArgumentList "certificate clean" -Wait -NoNewWindow
        netsh http add sslcert ipport=0.0.0.0:443 certhash=$thumb appid='{4dc3e181-e14b-4a21-b022-59fc669b0914}'

        Start-Service QlikCompose
        
    }

} catch {
    Write-Host -Message $_.Exception.Message -Severity "Error"
}



 
