resource "random_id" "randomServerId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_)"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  sql_administrator_login_password = var.sql_administrator_login_password != null? var.sql_administrator_login_password : random_password.password.result
  provision_id = var.provision_id != null? var.provision_id : random_id.randomServerId.hex
}

resource "azurerm_mssql_server" "sqlserver" {
 name                         = "sqlserver-${local.provision_id}"
  resource_group_name          = var.resource_group_name
  location                     = var.location
  version                      = "12.0"
  administrator_login          = var.sql_administrator_login
  administrator_login_password = local.sql_administrator_login_password
  minimum_tls_version          = "1.2"

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
    ProvId = var.provision_id
  }

  identity {
    type = "SystemAssigned"
  }
}

resource "azurerm_mssql_database" "db" {
  name           = "demoDB"
  server_id      = azurerm_mssql_server.sqlserver.id
  create_mode    = "Default"
 
  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
  }
}

# FOR QDI scenario
resource "azurerm_mssql_database" "dbname_source" {
  name           = "source"
  server_id      = azurerm_mssql_server.sqlserver.id
  create_mode    = "Default"
 
  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
  }
}

resource "azurerm_mssql_database" "dbname_target" {
  name           = "target"
  server_id      = azurerm_mssql_server.sqlserver.id
  create_mode    = "Default"
 
  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
  }
}