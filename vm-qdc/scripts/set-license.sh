#!/bin/bash

echo "--- Start set-license.sh"

licenseBody='{"licKeyString": "'"$1"'"}'

echo "Wait 120 seconds to make sure QDC is running"
sleep 120

FILE=/usr/local/qdc/dcaasIntegration/docker-compose.dcaas-connectors.yml

if [ -f "$FILE" ]; then

    echo "Use Sept 2020 onwards licence key"
    licenseBody='{"licKeyString": "'"$2"'"}'

    echo ""
    URL="http://localhost:8080/qdc/j_spring_security_check"
    echo "--> Login to QDC: $URL"
    
    ## Login
    curl $URL \
      -H "Connection: keep-alive" \
      -H "Cache-Control: max-age=0" \
      -H "Upgrade-Insecure-Requests: 1" \
      -H "Origin: http://localhost:8080" \
      -H "Content-Type: application/x-www-form-urlencoded" \
      -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" \
      -H "Referer: http://localhost:8080/qdc/login" \
      -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
      --data "j_password=nvs2014%21&j_username=podium" \
      --compressed \
      --insecure \
      -c qdc.cookie.txt

    echo "Cookie: "
    cat qdc.cookie.txt

    curl "http://localhost:8080/qdc/license/register/" -X PUT -H "Content-Type: application/json;charset=UTF-8" -d "$licenseBody"  --compressed --insecure -b qdc.cookie.txt

else

    curl "http://localhost:8080/qdc/license/register" -X PUT -H "Content-Type: application/json" -d "$licenseBody"

fi

echo ""
echo "--- Done set-license.sh"
