resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "random_password" "qlikpassword" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

data "azurerm_key_vault_secret" "serial" {
  name         = "qs-license-pa-serial"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "control" {
  name         = "qs-license-pa-control"
  key_vault_id = var.key_vault_id
}

data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = nonsensitive(random_password.password.result)
  serial = nonsensitive(data.azurerm_key_vault_secret.serial.value)
  control = nonsensitive(data.azurerm_key_vault_secret.control.value)
  cert_password = nonsensitive(data.azurerm_key_vault_secret.cert-password.value)
  qlikpassword = nonsensitive(random_password.qlikpassword.result)
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  isExternal = var.isExternal
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_windows_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  network_interface_ids = [
    module.qmi-nic.id,
  ]

  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_id =  var.image_reference

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "10m"
      use_ntlm = false
      insecure = true
    }
    source      = "${path.module}/main"
    destination = "C:/provision/"
  }
}


/*resource "azurerm_virtual_machine_extension" "prepfiles" {
  depends_on = [
    azurerm_windows_virtual_machine.vm
  ]

  name = "vm-extension-prepfiles"
  virtual_machine_id   = azurerm_windows_virtual_machine.vm.id
  
  publisher = "Microsoft.Compute"
  type = "CustomScriptExtension"
  type_handler_version = "1.10"
  auto_upgrade_minor_version  = true
  
  settings = <<SETTINGS
    {
        "fileUris": [
            "https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/vm-qs/main/prep-files.ps1"
        ],
        "commandToExecute": "start powershell.exe -NoProfile -ExecutionPolicy unrestricted -File prep-files.ps1"
    }
    SETTINGS
}*/


resource "null_resource" "vm-qs-post" {

  depends_on = [
    azurerm_windows_virtual_machine.vm
  ]

  /*provisioner "local-exec" {
    command = "echo 'Waiting 20 seconds for PrepFiles to finish'; sleep 20;"
  }*/

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "10m"
      use_ntlm = false
      insecure = true
    }

    inline = [
      "powershell.exe -NoProfile -File C:\\provision\\prep-files.ps1 -scenario vm-qs",
      "powershell.exe -NoProfile -File C:\\provision\\bootstrap-qs.ps1 -ModuleName vm-qs",
      "powershell.exe -NoProfile -File C:\\provision\\qs-post-cfg.ps1 -Hostname ${var.resource_group_name}.qmi.qlik-poc.com -Serial ${local.serial} -Control ${local.control} -CertPwd \"${local.cert_password}\" -QlikUserPwd ${local.qlikpassword}",
      "powershell.exe -NoProfile -File C:\\provision\\q-WebConnectors.ps1",
    ]
  }

}


resource "null_resource" "vm-is-qdc" {

  count = var.isWithQDC? 1 : 0
  
  depends_on = [
    azurerm_windows_virtual_machine.vm,
    null_resource.vm-qs-post
    
  ]

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "10m"
      use_ntlm = false
      insecure = true
    }
    source      = "${path.module}/scripts-qdc"
    destination = "C:/provision/qdc"
  }

}

module "win-common" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//win-common"

  depends_on = [
    azurerm_windows_virtual_machine.vm,
    null_resource.vm-qs-post,
    null_resource.vm-is-qdc
  ]
  
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = local.admin_password  
}