Write-Log -Message "Generating JWT for QDC"

Expand-Archive -LiteralPath "$PSScriptRoot\jwt-generator.zip" -DestinationPath $PSScriptRoot\
$ENV:PATH += ";C:\Program Files\Qlik\Sense\ServiceDispatcher\Node"
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\server*.pem" c:\provision\jwt-generator\.
node $PSScriptRoot\jwt-generator\index.js

Copy-Item $PSScriptRoot\jwt-generator\qdc.jwt "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\."


