variable "resource_group_name" {
  type = string
}

variable "provision_id" {
  type = string
}

variable "location" {
  type = string
  default = "EAST US"
}

variable "tags" {
    default = null
}

variable "dbricks_app_registration_principal_id" {
    description = "databricks-qmi"
    default = "efeee17c-d2b3-4e7c-a163-9995b7d281e2"
}

variable "dbricks_app_registration_application_id" {
    description = "databricks-qmi"
    default = "9ccb0d99-3bba-4695-aa47-df77bf512084"
}