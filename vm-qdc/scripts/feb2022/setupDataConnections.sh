#!/bin/bash

BASEDIR=$(dirname "$0")

echo "Start setupDataconnections.sh"
echo "This will configure Qlik Catalog with sources data"

QDC_IP="localhost"

USER_AGENT="Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36"

csrf=`awk 'NR == 5 {print $7}' $BASEDIR/qdc.cookie.txt`
echo "CSRF = $csrf"

echo ""
URL="http://$QDC_IP:8080/qdc/srcConnection/save"
echo "--> Import DB sales"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "'X-XSRF-TOKEN': $csrf" \
  -H "'User-Agent': $USER_AGENT" \
  -H "'Accept-Language': en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" -H "'Content-Type': application/json;charset=UTF-8" \
  -H "Origin: http://$QDC_IP:8080" \
  -H "Referer: http://$QDC_IP:8080/qdc/" \
  --data '{"name":"sales","businessDescription":"Sales Database","connectionType":"JDBC","commProtocol":"JDBC","uri":"jdbc:mysql://localhost:3306/sales","username":"demo","password":"Qlik1234\u0021","jdbcSourceTypeName":"MYSQL","properties":[],"keyFile":null,"groups":[{"id":1,"lastUpdTs":1605288850523,"name":"SUPER_GROUP","version":0}]}' \
  --compressed \
  --insecure \
  -b $BASEDIR/qdc.cookie.txt


echo ""
URL="http://$QDC_IP:8080/qdc/srcConnection/save"
echo "--> Import DB sales-errors"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "X-XSRF-TOKEN: $csrf" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Origin: http://$QDC_IP:8080" \
  -H "Referer: http://$QDC_IP:8080/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \  
  --data $'{"name":"sales-errors","businessDescription":"Sales Database","connectionType":"JDBC","commProtocol":"JDBC","uri":"jdbc:mysql://localhost:3306/sales-errors","username":"demo","password":"Qlik1234\u0021","jdbcSourceTypeName":"MYSQL","properties":[],"keyFile":null,"groups":[{"id":1,"lastUpdTs":1605288850523,"name":"SUPER_GROUP","version":0}]}' \
  --compressed \
  --insecure \
  -b $BASEDIR/qdc.cookie.txt

  echo ""
URL="http://$QDC_IP:8080/qdc/srcConnection/save"
echo "--> Import DB classic-models"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "X-XSRF-TOKEN: $csrf" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Origin: http://$QDC_IP:8080" \
  -H "Referer: http://$QDC_IP:8080/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \  
  --data $'{"name":"classic-models","businessDescription":"Sales Database","connectionType":"JDBC","commProtocol":"JDBC","uri":"jdbc:mysql://localhost:3306/classic-models","username":"demo","password":"Qlik1234\u0021","jdbcSourceTypeName":"MYSQL","properties":[],"keyFile":null,"groups":[{"id":1,"lastUpdTs":1605288850523,"name":"SUPER_GROUP","version":0}]}' \
  --compressed \
  --insecure \
  -b $BASEDIR/qdc.cookie.txt

  echo ""
URL="http://$QDC_IP:8080/qdc/srcConnection/save"
echo "--> Import DB : classic-models-errors"
curl $URL \
  -X "PUT" \
  -H "Connection: keep-alive" \
  -H "Accept: application/json, text/plain, */*" \
  -H "X-XSRF-TOKEN: $csrf" \
  -H "User-Agent: $USER_AGENT" \
  -H "Content-Type: application/json;charset=UTF-8" \
  -H "Origin: http://$QDC_IP:8080" \
  -H "Referer: http://$QDC_IP:8080/qdc/" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \  
  --data $'{"name":"classic-models-errors","businessDescription":"Sales Database","connectionType":"JDBC","commProtocol":"JDBC","uri":"jdbc:mysql://localhost:3306/classic-models-errors","username":"demo","password":"Qlik1234\u0021","jdbcSourceTypeName":"MYSQL","properties":[],"keyFile":null,"groups":[{"id":1,"lastUpdTs":1605288850523,"name":"SUPER_GROUP","version":0}]}' \
  --compressed \
  --insecure \
  -b $BASEDIR/qdc.cookie.txt
