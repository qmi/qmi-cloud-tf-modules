variable "resource_group_name" {

}

variable "provision_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "location" {
  type = string
  description = "(optional) describe your variable"
  default = "EAST US"
}

variable "subnet_id" {
  default = null
}

variable "user_id" {
}

variable "admin_login" {
  type = string
  description = "Login to authenticate to MySQL Server"
  default = "qmi"
}

variable "db-version" {
  type = string
  description = "MariaDB Server version to deploy"
  default = "10.2"
}
variable "sku-name" {
  type = string
  description = "MariaDB SKU Name"
  default = "GP_Gen5_2"
}
variable "storage" {
  type = string
  description = "MariaDB Storage in MB"
  default = "5120"
  }
