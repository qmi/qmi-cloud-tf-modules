output "db_server_fqdn" {
  value = azurerm_postgresql_flexible_server.postgresql-server.fqdn
}

output "root_username" {
  value = var.admin_login
}

output "root_username_password" {
  value = nonsensitive(random_password.password.result)
}
