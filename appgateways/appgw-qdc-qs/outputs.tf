output "appgw_hostname" {
  value = local.appgw_hostname
}

output "appgw_id" {
  value = azurerm_public_ip.appgw-ip.id
}

output "appgw_public_ip" {
  value = azurerm_public_ip.appgw-ip.ip_address
}

locals {
  backend_pools = tolist(azurerm_application_gateway.qmi-app-gw.backend_address_pool)
}

output "appgw_backend_address_pool_0_id" {
  value = local.backend_pools[index(local.backend_pools.*.name, "${var.appgw_hostname}-qs-bp")].id
}

output "appgw_backend_address_pool_1_id" {
  value = local.backend_pools[index(local.backend_pools.*.name, "${var.appgw_hostname}-qdc-bp")].id
}
