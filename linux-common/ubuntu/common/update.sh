#!/bin/bash

echo "--- Executing: $0 $@"

echo 'Updating OS. This will take a around 10 minutes'
apt -qq -y update
apt --fix-broken -qq -y upgrade
