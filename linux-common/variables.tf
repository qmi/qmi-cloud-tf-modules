variable "private_ip_address" {
}

variable "admin_username" {
}

variable "admin_password" {
}

variable "os_type" { 
  description = "Either 'centos' or 'ubuntu'"
  default = "centos"
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "update" {
  type = bool
  default = true
}

variable "resize" {
  type = bool
  default = true
}

