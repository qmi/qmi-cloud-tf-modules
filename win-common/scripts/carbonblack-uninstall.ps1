Param(
    [string] $CCID  
)

try {
    Write-Host "--- Uninstalling CarbonBlack!!!!"
    C:\windows\CarbonBlack\uninst.exe /S
    Start-Sleep -s 15
} catch {
    Write-Host "--- CarbonBlack already uninstalled!!!"
}

    
Write-Host "--- Installing CrowdStrike WindowsSensor..."
C:\provision\win-common\crowdstrike\WindowsSensor.exe /install /quiet /norestart CID=$CCID
   