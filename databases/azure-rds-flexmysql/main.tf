resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "azurerm_mysql_flexible_server" "mysql-server" {
  name = "qmi-mysql-${var.provision_id}"
  location = var.location
  resource_group_name = var.resource_group_name
 
  administrator_login = var.admin_login
  administrator_password = random_password.password.result

  storage {
    size_gb = var.mysql-storage
  }

  backup_retention_days  = 7
  sku_name = var.mysql-sku-name
  version = var.mysql-version

  geo_redundant_backup_enabled = false
  //public_network_access_enabled = true

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner = var.user_id
    ProvId = var.provision_id
  }
}

resource "azurerm_mysql_flexible_database" "example" {
  name                = "QlikDB"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mysql_flexible_server.mysql-server.name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_flexible_server_configuration" "example" {
  name                = "binlog_row_image"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mysql_flexible_server.mysql-server.name
  value               = "full"
}