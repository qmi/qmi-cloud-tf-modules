data "azurerm_key_vault_secret" "cert-password" {
  name         = "star-qmi-qlikpoc-com-password"
  key_vault_id = var.key_vault_id
}

locals {
  domain = var.domain != null? var.domain : "qem.qmi"
  cert_password = nonsensitive(data.azurerm_key_vault_secret.cert-password.value)
}

resource "null_resource" "prep" {

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = var.vm_private_ip_address
      user     = var.vm_admin_username
      password = var.vm_admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }
    source      = "${path.module}/main"
    destination = "C:/provision/replicate-install"
  }

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = var.vm_private_ip_address
      user     = var.vm_admin_username
      password = var.vm_admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/provision/replicate-install/prep-files.ps1",
      "powershell.exe -File C:/provision/replicate-install/di-database-prep.ps1",
    ]
  }
}

resource "null_resource" "install" {

  depends_on = [
    null_resource.prep
  ]

  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = var.vm_private_ip_address
      user     = var.vm_admin_username
      password = var.vm_admin_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/provision/replicate-install/di-replicate-getBinary.ps1 -url ${var.download_url}",
      "powershell.exe -File C:/provision/replicate-install/di-replicate-install.ps1 -url ${var.download_url}",
      "powershell.exe -File C:/provision/replicate-install/di-replicate-installQMICertificate.ps1 -CertPwd \"${local.cert_password}\" -url ${var.download_url}",
      "powershell.exe -File C:/provision/replicate-install/di-em-register-replicate.ps1 -user ${var.vm_admin_username} -pass ${var.vm_admin_password} -domain ${local.domain}",
    ]

  }
}
