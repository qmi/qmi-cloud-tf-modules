#!/bin/bash

echo "--- Executing: $0 $@"

BASEDIR=$(dirname "$0")

binary="falcon-sensor_5.17.0-8103_amd64.deb"

echo "--- Installing CrowdStrike falcon agent --> $binary"
echo "CID=$1"

#sudo apt -qq -y update
sudo apt install libnl-3-200 libnl-genl-3-200 -qq -y

wget --quiet https://d7ipctdjxxii4.cloudfront.net/others/$binary -O $BASEDIR/$binary

if ! dpkg -l | grep -qw falcon-sensor; then
    sudo dpkg -i $BASEDIR/$binary
fi

sudo /opt/CrowdStrike/falconctl -s -f --cid=$1
sudo service falcon-sensor start 
echo "CrowdStrike falcon agent done!"