variable "subnet_id" {
}

variable "prefix" {
}

variable "resource_group_name" {
}

variable "location" {
  default = "East US"
}

variable "user_id" { 
}

variable "isExternal" {
  default = false
}


