#!/bin/bash

sudo mount -o remount,rw /

echo "Restart Postgres and QDC"
sudo systemctl restart qdc_pg-11.8.service
sudo systemctl restart tomcat.service

echo "Restart Qlik Containers"
sudo systemctl daemon-reload
sudo service containerd restart
sudo service docker stop
sudo service docker start
sudo systemctl restart qlikContainers.service

echo "Restart NextGen-XML Containers"
sudo systemctl restart nextgen-xml.service

sudo mount /usr/local/qdc/source

