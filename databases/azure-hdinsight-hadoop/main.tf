
resource "random_password" "password1" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "random_password" "password2" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_numeric = 2
  min_lower = 2
  min_upper = 2
  min_special = 2
}

resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 5
}

resource "azurerm_storage_account" "example" {
  name                     = "hdinsightstor${random_id.randomMachineId.hex}"
  resource_group_name      = var.resource_group_name
  location                 = var.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = var.tags
}

resource "azurerm_storage_container" "example" {
  name                  = "hdinsight"
  storage_account_name  = azurerm_storage_account.example.name
  container_access_type = "private"
}

resource "azurerm_role_assignment" "data-contributor-dbricksapp1" {
  scope                = azurerm_storage_account.example.id
  role_definition_name = "Contributor"
  principal_id         = var.dbricks_app_registration_principal_id
}

resource "azurerm_role_assignment" "data-contributor-dbricksapp2" { 
  scope                = azurerm_storage_account.example.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = var.dbricks_app_registration_principal_id
}

resource "azurerm_hdinsight_hadoop_cluster" "example" {
  name                = "hdicluster-${var.provision_id}"
  resource_group_name = var.resource_group_name
  location            = var.location
  cluster_version     = "4.0"
  tier                = "Standard"

  tags = var.tags

  component_version {
    hadoop = "3.1"
  }

  gateway {
    username = "acctestusrgw"
    password = random_password.password1.result
  }

  storage_account {
    storage_container_id = azurerm_storage_container.example.id
    storage_account_key  = azurerm_storage_account.example.primary_access_key
    is_default           = true
  }

  roles {
    head_node {
      vm_size  = "Standard_D3_V2"
      username = "acctestusrvm"
      password = random_password.password2.result
    }

    worker_node {
      vm_size               = "Standard_D3_V2"
      username              = "acctestusrvm"
      password              = random_password.password2.result
      target_instance_count = 2
    }

    zookeeper_node {
      vm_size  = "Standard_D3_V2"
      username = "acctestusrvm"
      password = random_password.password2.result
    }
  }
}
