variable "resource_group_name" {
  type = string
}

variable "provision_id" {
  type = string
}

variable "subnet_id" {

}

variable "location" {
  type = string
  default = "EAST US"
}

variable "user_id" {
  type = string
}