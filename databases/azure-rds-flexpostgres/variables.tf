variable "resource_group_name" {

}

variable "provision_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "location" {
  type = string
  description = "(optional) describe your variable"
  default = "EAST US"
}


variable "admin_login" {
  type = string
  description = "Login to authenticate to PostgreSQL Server"
  default = "qmi"
}

variable "postgresql-version" {
  type        = string
  description = "PostgreSQL Server version to deploy"
  default     = "12"
}
variable "postgresql-sku-name" {
  type        = string
  description = "PostgreSQL SKU Name"
  default     = "GP_Standard_D2s_v3"
}
variable "postgresql-storage" {
  type        = string
  description = "PostgreSQL Storage in MB"
  default     = "32768"
}

variable "user_id" {

}