
Param(
    [string] $Hostname,
    [string] $Serial,
    [string] $Control,
    [string] $CertPwd,
    [string] $QlikUserPwd,
    [string] $QDC_HOST,
    [string] $CCID
)

& "$PSScriptRoot\bootstrap-module.ps1"

Write-Log "---- Bootstrap Qlik Sense ----"

& "C:\Provision\bootstrap-qs.ps1" -ModuleName vm-qs
& "C:\Provision\qs-post-cfg.ps1" -Hostname $Hostname -Serial $Serial -Control $Control -CertPwd "$CertPwd" -QlikUserPwd $QlikUserPwd
& "C:\Provision\webconnectors\q-WebConnectors.ps1"
& "C:\Provision\gen-jwt.ps1"

#Win-Common
& "C:\Provision\win-common\resize-disk-tomax.ps1"
& "C:\Provision\win-common\disable-ie-sec.ps1"
& "C:\provision\win-common\carbonblack-uninstall.ps1" -CCID $CCID


& "C:\Provision\qdc\qdc-setup-sept20.ps1" -QDC_HOST $QDC_HOST