#POSTGRES_PASSWD=$1
echo "START import podium_md database"

#Stop QDC services
sudo service tomcat stop
echo "Stopping tomcat"

#Create passwd file
#echo *:*:*:postgres:$POSTGRES_PASSWD > /home/qmi/.pgpass
#chmod 0600 /home/qmi/.pgpass

#echo *:*:*:postgres:$POSTGRES_PASSWD > /root/.pgpass
#chmod 0600 /root/.pgpass

#echo "Create pgpass file"

#Rename original podium_md database
# --> ALTER DATABASE podium_md RENAME TO podium_md.ori;
psql -h localhost -U postgres -w -c "ALTER DATABASE podium_md RENAME TO podium_md_ori" > /home/qmi/scripts/podium_sql_migration.log
echo "Original DB Renamed"
psql -h localhost -U postgres -w -c "CREATE DATABASE podium_md" >> /home/qmi/scripts/podium_sql_migration.log
echo "Created new podium_md database"

#Restore Database
psql -h localhost -U postgres -d podium_md -f /home/qmi/scripts/feb2022/podium_md.backup.sql >> /home/qmi/scripts/podium_sql_migration.log
echo "Restored DB"

#Copy lineage icons
cp /home/qmi/scripts/feb2022/*.svg /usr/local/qdc/apache-tomcat-9.0.38/webapps/qdc/resources/assets/images/lineage
chown qdc:qdc *.svg

#Restart Catalog
sudo service tomcat start
echo "Started Tamcat"
