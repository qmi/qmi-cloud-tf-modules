module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}

resource "azurerm_mysql_flexible_server_firewall_rule" "fw_rule" {
  for_each = module.fw-ips.ips_az_qcs

  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_mysql_flexible_server.mysql-server.name
  start_ip_address    = each.value.0
  end_ip_address      = each.value.1

}

module "dummy-data" {

  count = var.dummydata != null? 1 : 0

  depends_on = [ azurerm_mysql_flexible_server_firewall_rule.fw_rule ]

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/dummy"

  type      = "mysql"
  host      = "${azurerm_mysql_flexible_server.mysql-server.fqdn}:3306"
  username  = var.admin_login
  password  = nonsensitive(random_password.password.result)
  database  = ""
}