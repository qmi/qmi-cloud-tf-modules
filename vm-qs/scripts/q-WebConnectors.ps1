<#
Module:             q-WebConnectors
Author:             Clint Carr
Modified by:        Byron Ainsworth
                    Clint Carr
last updated:       05/30/2019
Modification History:
 - Updated to May 2019
 - Added code to accept EULA
 - Added code to license premium web connectors
 - Added code to enable beta connectors and sensitive content
 - Added comments
Intent: Acquire, install and configure Qlik Web Connectors
Dependencies:
 - Addition of LicenseSetttings.xml and UserSettings.xml to c:\shared-content\files\QlikWebConnectors\
#>

Param(
    [string] $DownloadURL = "https://github.com/qlik-download/qwc-standalone/releases/download/v2.140.0/QlikWebConnectorsDecember2022.zip"
)

if(!(Test-Path c:\qmi\QMIError)){

    $ProgressPreference = 'SilentlyContinue'

    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

    Write-Log -Message "Starting q-WebConnectors"
    ### Download Connectors
    Write-Log -Message "Downloading Web Connectors"
    (New-Object System.Net.WebClient).DownloadFile($DownloadURL, "$env:USERPROFILE\Downloads\QlikWebConnectors.zip")
    Expand-Archive $env:USERPROFILE\Downloads\QlikWebConnectors.zip "c:\QlikWebConnectors\" -force

    ### Allowing non local access to connectors
    #Write-Log -Message "Allowing local host connection to connectors and non-localhost access to Connector URL."
    #copy-item "$PSScriptRoot\deploy.config" "c:\QlikWebConnectors\" -Force

    ### Creating QlikWebConnectors Service
    Write-Log -Message "Creating Windows Service QlikWebConnectors"
    Start-Process "c:\QlikWebConnectors\BatchFiles\Install Service.bat"

    Start-Sleep 10

    ### Starting QlikWebConnectors Service
    write-Log -Message "Starting QlikWebConnectors Service"
    Start-Service "Qlik Web Connectors" | Out-Null

    ### License Web Connectors
    #Write-Log -Message "Licensing Premium Connectors"
    #copy-item "$PSScriptRoot\LicenceSettings.xml" "c:\qlikWebConnectors\App_Data\LicenceSettings.xml" -Force
    #copy-item "$PSScriptRoot\UserSettings.xml" "c:\qlikWebConnectors\App_Data\DEFAULT0-0000-0000-0000-000000000000\UserSettings.xml" -Force

    ### Start service
    #write-Log -Message "Restarting QlikWebConnectors Service"
    #restart-service QlikWebConnectors | Out-Null
}
 
