
variable "prefix" {
  description = "The Prefix used for all resources in this example"
  default = "QMI-QS"
}

variable "location" {
  default = "East US"
}

variable "image_reference" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Compute/galleries/QMICloud/images/QlikSenseEnterprise/versions/13.72.3"
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_D4s_v3"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "128"
}

variable "admin_username" {
  default = "qmi"
}

variable "user_id" {
}

variable "key_vault_id" {
  default = "/subscriptions/73c75d40-8c7d-45cf-b4f9-afdb210da92d/resourceGroups/QMI-infra/providers/Microsoft.KeyVault/vaults/qmicloud-secrets"
}

variable "vm_subnet_id" {
  
}

variable "script1" {
  default = "./scripts/resethostname.ps1"
}
