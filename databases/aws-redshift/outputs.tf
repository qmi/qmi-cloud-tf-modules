output "redshift_cluster_id" {
  description = "The availability zone of the RDS instance"
  value       = module.redshift.cluster_id
}

output "redshift_cluster_endpoint" {
  description = "Redshift endpoint"
  value       = module.redshift.cluster_endpoint
}

output "redshift_cluster_hostname" {
  description = "Redshift hostname"
  value       = module.redshift.cluster_hostname
}

output "redshift_cluster_port" {
  description = "Redshift port"
  value       = module.redshift.cluster_port
}

output "redshift_cluster_database_name" {
    value = module.redshift.cluster_database_name
}

output "redshift_cluster_master_username" {
    value = var.cluster_master_username
}

output "redshift_cluster_master_password" {
    value = nonsensitive(random_password.password.result)
}

output "s3_bucket_name" {
  value = module.qmi-s3-bucket.bucket.s3_bucket_id
}

output "s3_bucket_region" {
  value = module.qmi-s3-bucket.bucket.s3_bucket_region
}

output "s3_iam_user_access_key" {
  value = module.qmi-s3-bucket.iam_access_key
}

output "s3_iam_user_access_secret" {
  value = module.qmi-s3-bucket.iam_access_secret
}