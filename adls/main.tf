resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 5
}


resource "azurerm_storage_account" "saccount" {

  name = var.storage_account_name != null? var.storage_account_name : "qmiadlsgen2${random_id.randomMachineId.hex}"

  resource_group_name       = var.resource_group_name
  location                  = var.location

  account_kind              = "StorageV2"
  account_replication_type  = var.account_replication_type != null? var.account_replication_type : "RAGRS"
  account_tier              = "Standard"
  access_tier               = "Hot"

  is_hns_enabled            = "true"

  tags = var.tags
}

locals {
  catalog_access_connector = (var.location == "westeurope" || var.location == "West Europe")? var.catalog_access_connector.eu : (var.location == "eastus" || var.location == "East US")? var.catalog_access_connector.us : var.catalog_access_connector.sea
}

resource "azurerm_storage_container" "scontainer" {

  name                  = var.container_name != null? var.container_name : "qmicontainer"
  storage_account_name  = azurerm_storage_account.saccount.name
  #container_access_type = "container"
}

resource "azurerm_role_assignment" "data-contributor-role" {
  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Contributor"
  principal_id         = var.tpm_app_registration_principal_id
}

resource "azurerm_role_assignment" "data-contributor-role2" {  
  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = var.tpm_app_registration_principal_id
}


resource "azurerm_role_assignment" "data-contributor-dbricksapp1" {
  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Contributor"
  principal_id         = var.dbricks_app_registration_principal_id
}

resource "azurerm_role_assignment" "data-contributor-dbricksapp2" { 
  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = var.dbricks_app_registration_principal_id
}

data "azurerm_databricks_access_connector" "dbaccssconn" {

  count = local.catalog_access_connector != null? 1 : 0
  name                = local.catalog_access_connector
  resource_group_name = "QMI-QDI-Shared"
}

resource "azurerm_role_assignment" "dbricks-1" {

  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = local.catalog_access_connector != null? data.azurerm_databricks_access_connector.dbaccssconn[0].identity[0].principal_id : "ed282f15-1796-46c9-89b1-712efbda33d4"
}

resource "azurerm_role_assignment" "dbricks-2" {
  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Storage Queue Data Contributor"
  principal_id         = local.catalog_access_connector != null? data.azurerm_databricks_access_connector.dbaccssconn[0].identity[0].principal_id : "ed282f15-1796-46c9-89b1-712efbda33d4"
}


###### EXTRA ASSIGN ROLE #######

resource "azurerm_role_assignment" "machine_role_assignment" { 

  count = var.principal_id_storage_blob_contributor != null? 1 : 0

  scope                = azurerm_storage_account.saccount.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = var.principal_id_storage_blob_contributor
}