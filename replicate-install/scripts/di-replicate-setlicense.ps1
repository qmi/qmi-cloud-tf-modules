<#
Module:             di-db-setup
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - First release 04/05/2020
last updated:       04/05/2020
Intent: Setting the DBs
#>

Param(
    [string] $composeLicense
)

Write-Host "ComposeLicenseJSON"
Write-Host $composeLicense

Set-Content $PSScriptRoot\compose-license.txt $composeLicense

# TODO set license
$bin = "C:\Program Files\Attunity\Compose for Data Warehouses\bin\"
$bin = $bin -replace ' ','` '

Write-Host "Connect to Compose"
$cmd = "$($bin)ComposeCli.exe connect"
Invoke-Expression $cmd

Write-Host "Apply Compose License"
$cmd = "$($bin)ComposeCli.exe register_license --req @$PSScriptRoot\compose-license.txt"
Invoke-Expression $cmd

