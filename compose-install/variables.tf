variable "vm_private_ip_address" {

}

variable "vm_admin_username" {
  
}

variable "vm_admin_password" {
  
}

variable "download_url" {
   //default = "https://da3hntz84uekx.cloudfront.net/QlikCompose/2021.8.0/139/_MSI/Qlik_Compose_2021.8.0.139.zip"
   default = "https://github.com/qlik-download/compose/releases/download/v2021.8.0.465/Qlik_Compose_2021.8.0.465.zip"
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "c_version" {
  default     = "gen2"
  description = "'c4dw' or 'gen2'"
}

variable "trigger_done" {
  default = null
}