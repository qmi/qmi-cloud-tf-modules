#!/bin/bash

echo "Start setup-QVDCatalog-DataManager.sh"
echo "This will configure QVD Catalog in DataManager for Qlik Sense"

# Install JQ for JSON parsing
echo "Install JQ for JSON parsing"
sudo yum install epel-release -y -q
sudo yum install jq -y -q

QS_HOST=$1
PASSWD=$2
QDC_HOST=$3

echo "QS_HOST: "$QS_HOST
echo "PASSWD: "$PASSWD
echo "QDC_HOST: "$QDC_HOST

rm -f ticket.txt
rm -f auth.txt

targetId=$(curl -I -X GET https://$QS_HOST/api/qdc/v1/setup/ui/page.html \
  -H 'Connection: keep-alive' \
  -H "x-qlik-xrfkey: 0123456789abcdef" \
  --compressed --insecure  | grep ?targetId= |awk -F'=' '{print $2}')


echo "TargetId:" $targetId

URL="https://$QS_HOST/internal_forms_authentication/?targetId=$targetId"
URL=${URL%$'\r'}
echo "URL: $URL"

curl -v -X POST $URL --data "username=.%5Cqlik&pwd=$PASSWD" --compressed --insecure > auth.txt 2>&1
ticket=$(cat auth.txt | grep ?qlikTicket= | awk -F'=' '{print $2}')

echo "TicketId:"$ticket

URL="https://$QS_HOST/api/qdc/v1/setup/ui/page.html?qlikTicket="$ticket
URL=${URL%$'\r'}
echo "URL:$URL"

curl -v -X GET $URL --insecure -c qdc.cookie.txt

echo "---> Start QVD Catalog Setup"
echo ""
echo "PG_TEST"
URL="https://$QS_HOST/api/qdc/v1/setup/pg_test"
URL=${URL%$'\r'}
echo "Connectiong to URL:$URL"


curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary '{"Server":"localhost","Port":"4432","User":"qliksenserepository","Password":"Qlik1234","Database":"SenseServices"}' \
  --compressed \
  --insecure \
  -b qdc.cookie.txt

echo ""
echo "PG_APPLY"
URL="https://$QS_HOST/api/qdc/v1/setup/pg_setup"
URL=${URL%$'\r'}
echo "Connectiong to URL:$URL"

curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary '{"Server":"localhost","Port":"4432","User":"qliksenserepository","Password":"Qlik1234","Database":"SenseServices"}' \
  --compressed \
  --insecure \
  -b qdc.cookie.txt


echo ""
echo "QRS_TEST"
URL="https://$QS_HOST/api/qdc/v1/setup/qrs_test"
URL=${URL%$'\r'}
echo "Connectiong to URL: $URL"

curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary '{"Server":"https://localhost:4242/qrs"}' \
  --compressed \
  --insecure \
  -b qdc.cookie.txt

echo ""
echo "QRS_APPLY"
URL="https://$QS_HOST/api/qdc/v1/setup/qrs_setup"
URL=${URL%$'\r'}
echo "Connectiong to URL: $URL"


curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary '{"Server":"https://localhost:4242/qrs"}' \
  --compressed \
  --insecure \
  -b qdc.cookie.txt


echo ""
echo "QDC TEST"
URL="https://$QS_HOST/api/qdc/v1/setup/qdc_test"
URL=${URL%$'\r'}
echo "Connectiong to URL: $URL"
#DATA="{\"Server\":\"http://$QDC_HOST\",\"User\":\"podium\",\"Password\":\"nvs2014\u0021\"}"
DATA='{"Server":"'${QDC_HOST}'","User":"podium","Password":"nvs2014!"}'
DATA=${DATA%$'\r'}

echo "DATA --> "$DATA

curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary $"$DATA" \
  --compressed \
  --insecure \
  -b qdc.cookie.txt  

echo ""
echo "QDC APPLY"
URL="https://$QS_HOST/api/qdc/v1/setup/qdc_setup"
URL=${URL%$'\r'}
echo "Connecting to URL: $URL"


curl $URL \
  -H 'Connection: keep-alive' \
  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
  -H 'Content-Type: application/json; charset=utf-8' \
  -H 'Accept: */*' \
  -H 'Origin: https://'$QS_HOST \
  -H 'Sec-Fetch-Site: same-origin' \
  -H 'Sec-Fetch-Mode: cors' \
  -H 'Sec-Fetch-Dest: empty' \
  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
  -H 'Accept-Language: en-US,en;q=0.9' \
  --data-binary $"$DATA" \
  --compressed \
  --insecure \
  -b qdc.cookie.txt   


#echo "---"
#echo "---> Generate JWT Token"
#URL="https://$QS_HOST/api/qdc/v1/setup/qdc_jwt"
#URL=${URL%$'\r'}
#echo "Connectiong to URL:$URL"
#DATA="{\"User\":\"qlik-data-catalyst\",\"Directory\":\"QLIK-EXTERNAL-SERVICE\",\"Prefix\":\"qdc\",\"Description\":\"QlikDataCatalyst\",\"Timeout\":\"30\",\"Cookie\":\"X-Qlik-QDC-Session\",\"Tag\":\"QVDCatalog\"}"
#echo "DATA --> "$DATA

#SAVED=$(curl -X POST $URL \
#  -H 'Connection: keep-alive' \
#  -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36' \
#  -H 'Content-Type: application/json; charset=utf-8' \
#  -H 'Accept: */*' \
#  -H 'Origin: https://'$QS_HOST \
#  -H 'Sec-Fetch-Site: same-origin' \
#  -H 'Sec-Fetch-Mode: cors' \
#  -H 'Sec-Fetch-Dest: empty' \
#  -H "Referer: https://$QS_HOST/api/qdc/v1/setup/ui/page.html" \
#  -H 'Accept-Language: en-US,en;q=0.9' \
#  --data-binary $DATA \
#  --compressed \
#  --insecure \
#  -b qdc.cookie.txt)

#echo "JWT RESPONSE ="$SAVED
#JWT=$(echo $SAVED | jq '.jwt')
#JWT=${JWT#"\""}
#JWT=${JWT%"\""}

yes | cp qdc.cookie.txt /usr/local/qdc/QVDs

#echo "JTW to save = "$JWT
#echo "__________________________________________________________________"
#echo $JWT > /root/jwt.save


