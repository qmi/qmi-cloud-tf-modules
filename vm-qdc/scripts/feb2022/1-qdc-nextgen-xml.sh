#!/bin/bash

echo "--- Start qdc-nextgen-xml.sh $1"
echo "Exec --- $0 $1"
echo "$2"
BASEQDC="/usr/local/qdc"
TOMCAT=$(ls -la $BASEQDC | grep apache-tomcat | grep '^d'| awk -F " " '{print $9}')
TOMCAT_HOME=$BASEQDC/$TOMCAT

BASEDIR=$(dirname "$0")

if [[ $2 == *"4.9.0"* ]]; then
 sudo cp /home/qmi/scripts/feb2022/tomcat.service /etc/systemd/system/
fi

wget --quiet https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pfx -O /home/qmi/scripts/wildcard_qmi_qlik-poc_com.pfx
sudo cp /home/qmi/scripts/wildcard_qmi_qlik-poc_com.pfx /usr/local/qdc

if [[ $2 == *"4.13.0"* ]]; then
 sudo cp /home/qmi/scripts/feb2022/docker-compose-qlikContainers.yml /usr/local/qdc/qlikcore/docker-compose-qlikContainers.yml
 sudo cp /tmp/podium/config/qlikContainers.service /etc/systemd/system/
 sudo systemctl daemon-reload
 sudo systemctl enable qlikContainers.service
fi


FILE=/usr/local/qdc/qlikcore/docker-compose-qlikContainers.yml
if [ -f "$FILE" ]; then  

    sudo systemctl daemon-reload
    sudo service containerd restart
    sudo service docker stop
    sudo service docker start

    sleep 20

    echo "Stopping qlikContainers service..."
    sudo systemctl stop qlikContainers.service
    echo "Done."

    echo "Setting machine IP ($1) in docker-compose-qlikContainers.yml"
    sed -i "s/host= port=5432/host=$1 port=5432/" $FILE
    sed -i "s/host=172.17.0.1 port=5432/host=$1 port=5432/" $FILE
    echo "Done."

    sleep 5

    sudo docker ps

    echo "Starting qlikContainers service..."
    sudo systemctl start qlikContainers.service
    echo "Done."

    sudo docker ps
    
fi

FILE=$TOMCAT_HOME/conf/core_env.properties
if [ -f "$FILE" ]; then
    echo "Setting machine IP ($1) in core_env.properties"
    sed -i "s/your-host-name:8080/$1:8080/" $FILE
    echo "Done."
fi

FILE=/usr/local/qdc/dcaasIntegration/docker-compose.dcaas-connectors.yml
if [ -f "$FILE" ]; then

    

    echo "Replacing server.xml for Tomcat - Stopping Tomcat"
    sudo systemctl stop tomcat.service
    mv $TOMCAT_HOME/conf/server.xml $TOMCAT_HOME/conf/server.xml_backup
    cp $BASEDIR/nextgen-xml_server.xml $TOMCAT_HOME/conf/server.xml
    sudo chown -R qdc:qdc $TOMCAT_HOME/conf
    echo "Starting Tomcat"
    sudo systemctl start tomcat.service
    echo "Done."
    
    echo "Setting machine IP ($1) in docker-compose.dcaas-connectors.yml"
    sed -i "s/\${DCAAS_HOST_NAME}/$1/" $FILE
    echo "Done."

    echo "Restarting nextgen-xml.service"
    sudo cp /tmp/podium/config/nextgen-xml.service /etc/systemd/system/
    sudo systemctl daemon-reload
    sudo systemctl enable nextgen-xml.service
    sudo systemctl restart nextgen-xml.service
    echo "Done."

    sudo docker ps
    
fi

echo "--- Done qdc-nextgen-xml.sh"



