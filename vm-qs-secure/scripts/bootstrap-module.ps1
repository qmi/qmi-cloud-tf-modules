

$DownloadUrl="https://gitlab.com/qmi/qmi-cloud-tf-modules/-/archive/master/qmi-cloud-tf-modules-master.zip?path=vm-qs"
$DownloadUrlWinCommon="https://gitlab.com/qmi/qmi-cloud-tf-modules/-/archive/master/qmi-cloud-tf-modules-master.zip?path=win-common"

New-Item -ItemType Directory -Force -Path C:\Temp

$ProgressPreference = 'SilentlyContinue'

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
(New-Object System.Net.WebClient).DownloadFile($DownloadUrl, "C:\Temp\vm-qs.zip") 
(New-Object System.Net.WebClient).DownloadFile($DownloadUrlWinCommon, "C:\Temp\win-common.zip") 

Expand-Archive "C:\Temp\vm-qs.zip" -DestinationPath "C:\Temp" -Force
Expand-Archive "C:\Temp\win-common.zip" -DestinationPath "C:\Temp" -Force

New-Item -ItemType Directory -Force -Path C:\Provision
New-Item -ItemType Directory -Force -Path C:\Provision\win-common

Copy-Item -Path "C:\Temp\qmi-cloud-tf-modules-master-vm-qs\vm-qs\scripts\*" -Destination "C:\Provision" -Recurse
Copy-Item -Path "C:\Temp\qmi-cloud-tf-modules-master-win-common\win-common\scripts\*" -Destination "C:\Provision\win-common" -Recurse

