Write-Host "--- Resize Partition C to max size"
try {
    $size = Get-PartitionSupportedSize -DriveLetter C
    Resize-Partition -DriveLetter C -Size $size.SizeMax | Out-Null
} catch {
    Write-Host "--- Disk already at its maximum size."
}
