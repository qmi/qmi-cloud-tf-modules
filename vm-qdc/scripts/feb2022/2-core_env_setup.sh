
echo "Exec --- $0 $1"

BASE="/usr/local/qdc"
TOMCAT=$(ls -la $BASE | grep apache-tomcat | grep '^d'| awk -F " " '{print $9}')
TOMCAT_HOME=$BASE/$TOMCAT
BASEDIR=$(dirname "$0")

echo "Replacing server.xml for Tomcat - Stopping Tomcat"
wget --quiet https://gitlab.com/qmi/qmi-cloud-tf-modules/-/raw/master/qmicerts/wildcard_qmi_qlik-poc_com.pfx -O /home/qmi/scripts/wildcard_qmi_qlik-poc_com.pfx
sudo cp /home/qmi/scripts/wildcard_qmi_qlik-poc_com.pfx /usr/local/qdc

sudo cp /home/qmi/scripts/wildcard_qmi_qlik-poc_com.pfx /usr/local/qdc
sudo systemctl stop tomcat.service
mv $TOMCAT_HOME/conf/server.xml $TOMCAT_HOME/conf/server.xml_backup
cp $BASEDIR/nextgen-xml_server.xml $TOMCAT_HOME/conf/server.xml
sudo chown -R qdc:qdc $TOMCAT_HOME/conf



## Add tha alternative classpath folder in the core_env.properties
#propFile=$TOMCAT_HOME/conf/core_env.properties
#(
#cat <<EOF
#
#### START -- JDBC ALTERNATIVE CLASSPATH
#jdbc.alternate.classpath.dir=/usr/local/qdc/lib
#### END -- JDBC ALTERNATIVE CLASSPATH
#
#EOF
#) >> $propFile


# Copy jar file to the alternative Classpath folder
echo "--- Adding following extra JARS in QDC '/usr/local/qdc/jdbcDrivers' folder"
BASEDIR=$(dirname "$0")
ls -asl $BASEDIR/../jar
mkdir -p $BASE/jdbcDrivers
cp $BASEDIR/../jar/* $BASE/jdbcDrivers
chown -R qdc:qdc $BASE/jdbcDrivers
echo "--- Done adding JARS!"

echo "Starting Tomcat"
sudo systemctl start tomcat.service
echo "Done."

#sudo cp $BASEDIR/../jar/redshift-jdbc42-no-awssdk-1.2.16.1027.jar $TOMCAT_HOME/webapps/qdc/WEB-INF/lib/.
#sudo chown -R qdc:qdc $TOMCAT_HOME/webapps/qdc/WEB-INF/lib
