#!/bin/bash
MODULE=$1

REPO="qmi-cloud-tf-modules-master"
mkdir -p /tmp
cd /tmp

URL=https://gitlab.com/qmi/qmi-cloud-tf-modules/-/archive/master/$REPO.tar.gz?path=$MODULE

echo "Getting module files..."
echo "$URL"

wget $URL -O /tmp/$MODULE.tar.gz
tar -xvf $MODULE.tar.gz

cp -R /tmp/$REPO-$MODULE/$MODULE/* /home/qmi/.

for i in $(find /home/qmi -name '*.sh' );  do chmod a+x "$i"; done

chown -R qmi:qmi /home/qmi
