variable "vm_private_ip_address" {

}

variable "vm_admin_username" {
  
}

variable "vm_admin_password" {
  
}

variable "download_url" {
}

variable "key_vault_id" {
  default = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.KeyVault/vaults/qmisecrets"
}

variable "domain" {
  default = null
}