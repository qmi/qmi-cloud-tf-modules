variable "region" {
    default = "us-east-1"
}

variable "provision_id" { 
}

variable "user_id" {
}

variable "vpc_id" {
  default = "vpc-c079f5bd"
  description = "VPC For Transfer Server"
}

variable "sftp_users" {
  type    = map(string)
  default = { sftpusr = "sftpusr" }
}

variable "subnet_ids" {
  default = ["subnet-4d26552b"]
  description = "Subnets For Transfer Server"
}

variable "force_destroy" {
  type        = bool
  default     = true
  description = "Whether to delete all the users associated with server so that server can be deleted successfully."
}

variable "security_policy_name" {
  type        = string
  default     = "TransferSecurityPolicy-2020-06"
  description = "Specifies the name of the [security policy](https://docs.aws.amazon.com/transfer/latest/userguide/security-policies.html) to associate with the server"
}

variable "forced_destroy" {
  default = null
}


