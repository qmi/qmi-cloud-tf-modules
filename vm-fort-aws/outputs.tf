output "fort_url" {
    value = "https://${aws_eip.lb.public_ip}"
}

output "fort_admin_console" {
    value = "http://${aws_eip.lb.public_ip}:8000/admin/"
}

output "ssh" {
    value = "ssh -i qmiforts.cer qlikfort@${aws_eip.lb.public_ip}"
}

output "private_ip" {
    value = aws_eip.lb.private_ip
}

output "public_ip" {
    value = aws_eip.lb.public_ip
}

output "public_dns" {
    value = aws_eip.lb.public_dns
}