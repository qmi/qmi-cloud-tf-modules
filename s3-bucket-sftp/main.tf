terraform {

  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.74.1"
    }
  }
}

resource "tls_private_key" "sftp-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "generated_key" {    
  key_name   = "key-${var.provision_id}"
  public_key = tls_private_key.sftp-key.public_key_openssh
}

## Public_secret

resource "aws_secretsmanager_secret" "public_key" {
  name = "pub-${var.provision_id}"
}

resource "aws_secretsmanager_secret_version" "public_key" {
  secret_id = aws_secretsmanager_secret.public_key.id
  secret_string = aws_key_pair.generated_key.public_key
}

## Private Secret

resource "aws_secretsmanager_secret" "private_key" {
  name = "priv-${var.provision_id}"
}

resource "aws_secretsmanager_secret_version" "private_key" {
  secret_id = aws_secretsmanager_secret.private_key.id
  secret_string = nonsensitive(tls_private_key.sftp-key.private_key_pem)
}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}

locals {

  port                 = "22"
  tags = {
    Deployment = "QMI"
    "Cost Center" = "3100"
    #QMI_user = var.user_id
    ProvID   = var.provision_id
    Name     = "sftp-${var.provision_id}"
    forced_destroy = var.forced_destroy
  }
}


module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  version = "~> 2.1.0"

  bucket = "sftp-${var.provision_id}"
  acl    = "private"

  versioning = {
    enabled = false
  }

  force_destroy = true

  tags = local.tags

}

###

resource "aws_s3_bucket_public_access_block" "sftp-block" {
  bucket = module.s3_bucket.s3_bucket_id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

###

resource "aws_iam_role" "logging" {
  name               = "${var.provision_id}-transfer-logging"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "transfer.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "logging" {
  name   = "${var.provision_id}-transfer-logging"
  role   = aws_iam_role.logging.id
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:CreateLogGroup",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
POLICY
}

resource "aws_transfer_server" "sftp" {
  endpoint_type          = "VPC"
  protocols              = ["SFTP"]
  identity_provider_type = "SERVICE_MANAGED"
  logging_role           = aws_iam_role.logging.arn
  force_destroy          = var.force_destroy
  security_policy_name   = var.security_policy_name
  endpoint_details {
    vpc_id             = var.vpc_id
    subnet_ids         = var.subnet_ids
    security_group_ids = [
      module.security_group.security_group_id,
      module.security_group_2.security_group_id
    ]
  }
  tags = local.tags
}

module "security_group" {
  
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG"
  description = "${var.provision_id}-SG-SFTP"
  vpc_id      = var.vpc_id


  # ingress

  ingress_cidr_blocks =  module.fw-ips.cidr_blocks

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Allow SFTP Inbound"

    },
  ]

  # egress

  egress_cidr_blocks =  module.fw-ips.cidr_blocks

  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Allow SFTP outbound"

    },
  ]

  tags = local.tags
  
}

module "security_group_2" {
  
  # SGs created here as Ports differ per Engine. Only Azure Firewall IPs added for now.
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4.3"

  name        = "${var.provision_id}-SG2"
  description = "${var.provision_id}-SG2-SFTP"
  vpc_id      = var.vpc_id


  # ingress

  ingress_cidr_blocks = module.fw-ips.cidr_blocks_others
  

  ingress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Allow SFTP Inbound"

    },
  ]

  # egress

  egress_cidr_blocks = module.fw-ips.cidr_blocks_others

  egress_with_cidr_blocks = [
    {
      from_port   = local.port
      to_port     = local.port
      protocol    = "tcp"
      description = "Allow SFTP Inbound"

    },
  ]

  tags = local.tags
}


resource "aws_iam_role" "user" {
  for_each           = var.sftp_users
  name               = "${var.provision_id}-sftp-user-${each.key}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "transfer.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "user" {
  for_each = var.sftp_users
  name     = "${var.provision_id}-sftp-user-${each.key}"
  role     = aws_iam_role.user[each.key].id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowListingOfUserFolder",
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketLocation"
      ],
      "Effect": "Allow",
      "Resource": [
       "${join("", ["arn:aws:s3:::", module.s3_bucket.s3_bucket_id])}"
      ]
    },
    {
      "Sid": "HomeDirObjectAccess",
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObjectVersion",
        "s3:DeleteObject",
        "s3:GetObjectVersion"
      ],
      "Resource": "${join("", ["arn:aws:s3:::", module.s3_bucket.s3_bucket_id, "/", each.value, "/*"])}"
    }
  ]
}
POLICY
}

resource "aws_transfer_user" "this" {
  for_each       = var.sftp_users
  server_id      = aws_transfer_server.sftp.id
  user_name      = each.key
  home_directory = "/${module.s3_bucket.s3_bucket_id}/${each.value}"
  role           = aws_iam_role.user[each.key].arn
}

resource "aws_transfer_ssh_key" "this" {
  for_each   = { "sftpusr" = aws_secretsmanager_secret_version.public_key.secret_string }
  server_id  = aws_transfer_server.sftp.id
  user_name  = each.key
  body       = each.value
  depends_on = [aws_transfer_user.this]
}

/*resource "null_resource" "get-endpoint-dns" {
  provisioner "local-exec" {
    command = "aws ec2 describe-vpc-endpoints --vpc-endpoint-ids ${aws_transfer_server.sftp.endpoint_details[0].vpc_endpoint_id} --query 'VpcEndpoints[*].DnsEntries[0].DnsName'> dns.txt"
  }
}

data "local_file" "endpoint-dns" {
  filename   = "dns.txt"
  depends_on = [null_resource.get-endpoint-dns]
}*/
