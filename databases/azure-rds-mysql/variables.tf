variable "resource_group_name" {

}

variable "provision_id" {
  type = string
  description = "(optional) describe your variable"
}

variable "location" {
  type = string
  description = "(optional) describe your variable"
  default = "EAST US"
}

variable "subnet_id" {
  default = null
}

variable "user_id" {
}

variable "admin_login" {
  type = string
  description = "Login to authenticate to MySQL Server"
  default = "qmi"
}

variable "mysql-version" {
  type = string
  description = "MySQL Server version to deploy"
  default = "8.0"
}
variable "mysql-sku-name" {
  type = string
  description = "MySQL SKU Name"
  default = "GP_Gen5_8"
}
variable "mysql-storage" {
  type = string
  description = "MySQL Storage in MB"
  default = "5120"
  }

variable "dummydata" {
  default = null
}

variable "envbranch" {
  default = "master"
}
