
Param(
    [string] $repo="qmi-cloud-tf-modules",
    [string] $branch="master",
    [string] $scenario="vm-fromsnapshot-win",
    [string] $path="scripts"    
)

$pathIs="$scenario/$path"

$DownloadUrl="https://gitlab.com/qmi/$repo/-/archive/$branch/$repo-$branch.zip?path=$pathIs"

Write-Host "--- Boostrap# Downloading repository files ($DownloadUrl) from branch ($branch)..."

New-Item -ItemType Directory -Force -Path C:\Temp | Out-Null

$ProgressPreference = 'SilentlyContinue'

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
(New-Object System.Net.WebClient).DownloadFile($DownloadUrl, "C:\Temp\$scenario.zip") 

Expand-Archive "C:\Temp\$scenario.zip" -DestinationPath "C:\Temp" -Force

New-Item -ItemType Directory -Force -Path C:\tmp | Out-Null
New-Item -ItemType Directory -Force -Path C:\tmp\provision | Out-Null

Copy-Item -Path "C:\Temp\$repo-$branch-$scenario-scripts\$scenario\$path\*" -Destination "C:\tmp\provision" -Recurse -Force

Remove-Item "C:\Temp\$repo-$branch-$scenario-scripts" -Recurse
Remove-Item "C:\Temp\$scenario.zip" -Recurse
