#!/bin/bash

echo "--- Executing: $0 $@"

BASEDIR=$(dirname "$0")

KEY=$1
echo "KEY=$KEY"

cVer=`rpm -E %{rhel}`

FILE="NessusAgent-10.8.2-el$cVer.x86_64.rpm"

echo "--- Installing Tenable Nessus Agent --> $FILE"

wget --quiet https://d7ipctdjxxii4.cloudfront.net/others/$FILE -O $BASEDIR/$FILE

sudo /bin/yum -y --quiet install $BASEDIR/$FILE -y

echo "--- Linking Tenable Nessus Agent..."
sudo /bin/systemctl start nessusagent.service
sudo /opt/nessus_agent/sbin/nessuscli agent link --key=$KEY --groups="Qlik IT Cloud Agents" --host=cloud.tenable.com --port=443

sudo /opt/nessus_agent/sbin/nessuscli agent status