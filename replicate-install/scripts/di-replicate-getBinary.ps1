<#
Module:             di-replicate-getBinary
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - Started from qs-getBinary
last updated:       20/04/2020
Intent: Download the Attunity binary selected by the end user of QMI.
Download collaterals binary (.NET and DB)
#>

Param(
    [string] $url
)

$ProgressPreference = 'SilentlyContinue'

$bin = "$PSScriptRoot\binaries"
New-Item -ItemType Directory -Force -Path $bin | Out-Null

$path = join-path $bin Attunity

$fileName = $url.Substring($url.LastIndexOf("/") + 1)

if ( -Not (Test-Path $path\$fileName))
    {
        Write-Host "Binary not found for $($fileName), downloading..."
        if ( -Not (Test-Path $path))
        {
            New-Item -ItemType directory -Path $path -ea Stop | Out-Null 
        }
        
        $dlLoc = join-path $path $fileName

        Write-Host "Downloading '$fileName' from $url into $dlLoc"
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
        (New-Object System.Net.WebClient).DownloadFile($url, $dlLoc)

    }
else {
    Write-Host "Binary found for $($fileName)"
}
