resource "null_resource" "dummy-data" {
    
  provisioner "local-exec" {
      command = "chmod +x ${path.module}/scripts/*.sh"
      interpreter = ["/bin/bash", "-c"]
  }

  provisioner "local-exec" {
      command = "${path.module}/scripts/dummy-${var.type}.sh \"$username\" \"$password\" \"$host\" \"$db\""
      interpreter = ["/bin/bash", "-c"]

      environment = {
          
          username  = var.username
          password  = var.password
          host      = var.host 
          db        = var.database
      }
  }

}