Param(
    [string] $KEY
)


New-Item -ItemType Directory -Force -Path C:\Temp | Out-Null
$ProgressPreference = 'SilentlyContinue'
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$version="NessusAgent-10.8.2-x64.msi"

Write-Host "--- Installing Tenable Nessus Agent... --> $version"

$cbBinaryUrl = "https://d7ipctdjxxii4.cloudfront.net/others/$version"
Invoke-WebRequest -Uri $cbBinaryUrl -OutFile "C:\Temp\$version"

$command = "/I C:\Temp\$version NESSUS_GROUPS=""Qlik IT Cloud Agents"" NESSUS_SERVER=""cloud.tenable.com:443"" NESSUS_KEY=$KEY /qn"

Start-Process msiexec.exe -Wait -ArgumentList $command 

