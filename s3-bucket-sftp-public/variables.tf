variable "region" {
    default = "us-east-1"
}

variable "provision_id" { 
}

variable "user_id" {
}

variable "sftp_type" {
  type        = string
  default     = "PUBLIC"
  description = "Type of SFTP server. **Valid values:** `PUBLIC`, `VPC` or `VPC_ENDPOINT`"
}

variable "protocols" {
  type        = list(string)
  default     = ["SFTP"]
  description = "List of file transfer protocol(s) over which your FTP client can connect to your server endpoint. **Possible Values:** FTP, FTPS and SFTP"
}

variable "identity_provider_type" {
  type        = string
  default     = "SERVICE_MANAGED"
  description = "Mode of authentication to use for accessing the service. **Valid Values:** `SERVICE_MANAGED`, `API_GATEWAY`, `AWS_DIRECTORY_SERVICE` or `AWS_LAMBDA`"
}

variable "api_gw_url" {
  type        = string
  default     = null
  description = "URL of the service endpoint to authenticate users when `identity_provider_type` is of type `API_GATEWAY`"
}

variable "invocation_role" {
  type        = string
  default     = null
  description = "ARN of the IAM role to authenticate the user when `identity_provider_type` is set to `API_GATEWAY`"
}

variable "force_destroy" {
  type        = bool
  default     = true
  description = "Whether to delete all the users associated with server so that server can be deleted successfully. **Note:** Supported only if `identity_provider_type` is set to `SERVICE_MANAGED`"
}

variable "security_policy_name" {
  type        = string
  default     = "TransferSecurityPolicy-2020-06"
  description = "Specifies the name of the [security policy](https://docs.aws.amazon.com/transfer/latest/userguide/security-policies.html) to associate with the server"
}

variable "sftp_users" {
  type    = map(string)
  default = { sftpusr = "sftpusr" }
}

variable "forced_destroy" {
  default = null
}
