output "StorageAccount-Scope" {
  value = azurerm_storage_account.saccount.id
}

output "StorageAccount-AccessKey" {
  value = nonsensitive(azurerm_storage_account.saccount.primary_access_key)
}

output "StorageAccount-ConnectionString" {
  value = nonsensitive(azurerm_storage_account.saccount.primary_connection_string)
}

output "StorageAccount-Name" {
  value = azurerm_storage_account.saccount.name
}

output "StorageAccount-ContainerName" {
  value = azurerm_storage_container.scontainer.name
}

output "StorageAccount-ContainerId" {
  value = azurerm_storage_container.scontainer.id
}

output "Azure_Active_Directory_Tenant_ID" {
  value = "c21eeb5f-f5a6-44e8-a997-124f2f7a497c"
}

output "Azure_Application_Registration_Client_ID" {
  value = var.dbricks_app_registration_application_id
}

output "Azure_Application_Registration_Secret" {
  value = "i3F8Q~FxhoyOP1-4r9sstaohnjxXaf~ulhVJFav_"
}