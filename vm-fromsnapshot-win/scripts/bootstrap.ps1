

if (!(Test-Path "C:\provision\QMIProvision.log"))
{
   New-Item -path "C:\provision" -name "QMIProvision.log" -ItemType "file" -Force
}

New-Item -ItemType directory -Path C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiCLI -force | Out-Null
Copy-Item $PSScriptRoot\qmiCLI.psm1 C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiCLI\qmiCLI.psm1 | Out-Null
Import-Module qmiCLI.psm1 | Out-Null

#Importing Utils module
New-Item -ItemType directory -Path C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiUtils -force | Out-Null
Copy-Item $PSScriptRoot\qmiUtils.psm1 C:\Windows\System32\WindowsPowerShell\v1.0\Modules\qmiUtils\qmiUtils.psm1 | Out-Null
Import-Module qmiUtils.psm1 | Out-Null

Write-Log "Scripts locations is: $PSScriptRoot"

#Install Chocolatey
#Write-Log "Install Chocolatey"
#Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

### Disable Password policy
secedit /export /cfg c:\secpol.cfg  | Out-Null
(Get-Content C:\secpol.cfg).replace("PasswordComplexity = 1", "PasswordComplexity = 0") | Out-File C:\secpol.cfg  | Out-Null
secedit /configure /db c:\windows\security\local.sdb /cfg c:\secpol.cfg /areas SECURITYPOLICY | Out-Null
rm -force c:\secpol.cfg -confirm:$false  | Out-Null

#Write-Log "Resize Partition C to max size"
#$size = Get-PartitionSupportedSize -DriveLetter C
#Resize-Partition -DriveLetter C -Size $size.SizeMax | Out-Null





