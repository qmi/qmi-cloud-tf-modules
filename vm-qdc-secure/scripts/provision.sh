#!/bin/bash

BASEDIR=$(dirname "$0")
QDC_LIC=$1
FALCON_CID=$2
QDC_IP=$3

LOG="/home/qmi/provision.log"
touch $LOG

echo '"'$0'" "'$1'" "'$2'" "'$3'" '>> $LOG 

$BASEDIR/bootstrap-module.sh "vm-qdc" >> $LOG

echo "--- Executing: falcon.sh: $FALCON_CID" >> $LOG
/home/qmi/scripts/falcon.sh $FALCON_CID >> $LOG

echo "--- Executing: resizedisk.sh" >> $LOG
/home/qmi/scripts/resizedisk.sh >> $LOG

echo "--- Executing: qdc-nextgen-xml.sh '$QDC_IP' '4.13.0'" >> $LOG
/home/qmi/scripts/feb2022/qdc-nextgen-xml.sh "$QDC_IP" "4.13.0" >> $LOG

echo "--- Executing: core_env_setup.sh" >> $LOG
/home/qmi/scripts/feb2022/core_env_setup.sh >> $LOGs

echo "--- Executing: set-license.sh '$QDC_LIC'" >> $LOG
/home/qmi/scripts/feb2022/set-license.sh "$QDC_LIC" >> $LOG

