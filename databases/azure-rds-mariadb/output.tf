output "db_server_fqdn" {
  value = azurerm_mariadb_server.mariadb-server.fqdn
}

output "root_username" {
  value = "${var.admin_login}@qmi-mariadb-${var.provision_id}"
}

output "root_username_password" {
  value = nonsensitive(random_password.password.result)
}