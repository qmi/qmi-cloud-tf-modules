
Param(
    [string] $IsExternal,
    [string] $Url
)

Write-Log -Message "Restarting hMailServer"
Restart-Service hMailServer -Force

Write-Log -Message "Modifying Qlik Alerting configuration for this hostname"

Expand-Archive -LiteralPath "$PSScriptRoot\apinode.zip" -DestinationPath $PSScriptRoot\

# Set Node from QS
$ENV:PATH += ";C:\Program Files\Qlik\Sense\ServiceDispatcher\Node"

Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\client*.pem" $PSScriptRoot\apinode\certs\.
if ( $IsExternal -eq "ok" ) {
    node $PSScriptRoot\apinode\index.js $Url
} else {
    node $PSScriptRoot\apinode\index.js
}

Write-Log "Recovering Qlik Alerting users for new hostname: $env:computername"
Start-Process powershell.exe -Wait -ArgumentList "Start-Process cmd.exe -Verb runAs -ArgumentList '/c $PSScriptRoot\qa-reset-users.bat'"

Start-Sleep -s 10