#!/bin/bash

echo "Exec --- $0 $1"

BASEDIR=$(dirname "$0")


#echo "Restarting Licensing containers"
#sudo -u qdc docker container restart licenses

#sudo -u qdc docker container ps
#ifconfig


echo "Use Feb 2021 SP2 onwards licence key"
licenseBody='{"licKeyString": "'"$1"'"}'

echo "Key: "$1

echo "Wait 120 seconds to make sure QDC is running"

sleep 120

URL="http://localhost:8080/qdc/"
echo "--> Login to QDC: $URL"
## Login
curl $URL \
  -H "Connection: keep-alive" \
  -H "Cache-Control: max-age=0" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Origin: http://localhost:8080" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" \
  -H "Referer: http://localhost:8080/qdc/login" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  --compressed \
  --insecure \
  -c $BASEDIR/qdcinit.cookie.txt

csrf=`awk 'NR == 5 {print $7}' $BASEDIR/qdcinit.cookie.txt`

echo "CSRF = $csrf"

URL="http://localhost:8080/qdc/j_spring_security_check"
echo "--> Login to QDC: $URL"
 
## Login
curl $URL \
  -H "Connection: keep-alive" \
  -H "Cache-Control: max-age=0" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Origin: http://localhost:8080" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" \
  -H "Referer: http://localhost:8080/qdc/login" \
  -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
  --data "j_password=nvs2014%21&j_username=podium&_csrf=$csrf" \
  --compressed \
  --insecure \
  -b $BASEDIR/qdcinit.cookie.txt \
  -c $BASEDIR/qdc.cookie.txt

echo "Cookie: "
cat $BASEDIR/qdc.cookie.txt

csrf=`awk 'NR == 5 {print $7}' $BASEDIR/qdc.cookie.txt`
echo "CSRF = $csrf"

curl "http://localhost:8080/qdc/license/register/" \
    -X 'PUT' \
    -H "Connection: keep-alive" \
    -H "Cache-Control: no-cache" \
    -H "Content-Type: application/json;charset=UTF-8" \
    -H "Origin: http://localhost:8080" \
    -H "Referer: http://localhost:8080/qdc/" \
    -H "X-XSRF-TOKEN: $csrf" \
    -H "Host: localhost:8080" \
    -H "Accept: application/json, text/plain, */*" \
    -H "Accept-Encoding: gzip, deflate" \
    -H "Accept-Language: en-US,en;q=0.9,it;q=0.8,it-IT;q=0.7" \
    -d "$licenseBody"  \
    --compressed \
    --insecure \
    -b $BASEDIR/qdc.cookie.txt


