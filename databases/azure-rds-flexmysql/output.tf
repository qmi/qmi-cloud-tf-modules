output "db_server_fqdn" {
  value = azurerm_mysql_flexible_server.mysql-server.fqdn
}

output "root_username" {
  value = var.admin_login
}

output "root_username_password" {
  value = nonsensitive(random_password.password.result)
}

output "dummy_data_databases_available" {
  value = var.dummydata != null? module.dummy-data[0].dbs : null
}