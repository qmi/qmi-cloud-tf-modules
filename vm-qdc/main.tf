resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
  min_numeric = 2
}

data "azurerm_key_vault_secret" "license-sept20" {
  name         = "qdc-license-full-sept20"
  key_vault_id = var.key_vault_id
}

locals {
  virtual_machine_name = "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = nonsensitive(random_password.password.result)
  qdc_license  = nonsensitive(data.azurerm_key_vault_secret.license-sept20.value)
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id

  isExternal = var.isExternal
}

resource "azurerm_linux_virtual_machine" "vm" {
  name                = local.virtual_machine_name
  resource_group_name = var.resource_group_name
  location            = var.location
  size                = var.vm_type
  admin_username      = local.admin_username
  admin_password      = local.admin_password
  disable_password_authentication = false

  network_interface_ids = [ module.qmi-nic.id ]


  os_disk {
    name                 = "${local.virtual_machine_name}-osdisk"
    caching              = "ReadWrite"
    storage_account_type = var.managed_disk_type
    disk_size_gb         = var.disk_size_gb
  }

  source_image_id = var.image_reference

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

  
  provisioner "file" {
    connection {
        type     = "ssh"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = local.admin_password
        timeout  = "10m"
        #private_key = "${file("~/.ssh/id_rsa")}"   
    }
    source      = "${path.module}/scripts"
    destination = "/home/${local.admin_username}"
  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      timeout  = "10m"
      #private_key = "${file("~/.ssh/id_rsa")}"
    }

    inline = [
        "echo ${local.admin_password} | sudo -S chmod a+x /home/${local.admin_username}/scripts/feb2022/*.sh",
        "sudo /home/${local.admin_username}/scripts/feb2022/0-postgres_password.sh ${local.admin_password}",
        #"sudo /home/${local.admin_username}/scripts/feb2022/1-qdc-nextgen-xml.sh '${module.qmi-nic.private_ip_address}' ${var.image_reference}",
        "sudo /home/${local.admin_username}/scripts/feb2022/2-core_env_setup.sh",
        "sudo /home/${local.admin_username}/scripts/feb2022/3-set-license.sh '${local.qdc_license}'"
    ]

  }
  
}

module "linux-common" {

  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//linux-common"

  depends_on = [
    azurerm_linux_virtual_machine.vm
  ]
  
  os_type             = "centos"
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = local.admin_password  
  update              = false
  
}