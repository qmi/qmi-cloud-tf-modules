output "default_primary_connection_string" {
  value = nonsensitive(azurerm_eventhub_namespace.ehbnamespace.default_primary_connection_string)
}

output "default_primary_key" {
  value = nonsensitive(azurerm_eventhub_namespace.ehbnamespace.default_primary_key)
}

output "namespace_name" {
  value = "qlik${local.scnamelower}ns"
}

