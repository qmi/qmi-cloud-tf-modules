<#
Module:             di-c4dw-install
Author:             Vincenzo Esposito
Modified by:        -
Modification History:
 - First release 04/05/2020
last updated:       21/04/2020
Intent: Install the selected version of Attunity Replicate
#>

Param(
    [string] $url
)

Import-Module $PSScriptRoot\q-helper.psm1 | Out-Null

$fileName = $url.Substring($url.LastIndexOf("/") + 1)

Write-Host "--- Starting di-compose-install.ps1"

$bin = "$PSScriptRoot\binaries\Attunity"
Write-Host "Binary Path $($bin)"


$issFile = "Compose_install.iss"

if ( $url -Match "2021.2" ) {
	$issFile="Compose_install_2021.2.0.iss"
}
if ( $url -Match "Attunity_Compose_for_Data_Warehouses" ) {
	$issFile="Compose_install_c4dw.iss"
}

Write-Host "Using ISS file: $issFile"

Write-Host $bin\$fileName

if (Test-Path "$($bin)\Compose_silent_x64_install.log") {
	Remove-Item -Path "$($bin)\Compose_silent_x64_install.log" -Force
}

If ((Test-Path $bin\$fileName))
{
	Write-Host "Installing Attunity Compose from $($bin)\$($fileName)"
    
    $fileNoExtension = [IO.Path]::GetFileNameWithoutExtension("$bin\$fileName")
	Expand-Archive $bin\$fileName -DestinationPath $bin\$fileNoExtension -Force

    Write-Host "Binary decompressed in folder $($bin)\$($fileNoExtension)"	
	$exec = Get-ChildItem $bin\$fileNoExtension\*.exe | Select-Object -ExpandProperty Name
    #Write-Host "Exec: $($exec)"

    $silentInstall = "$($bin)\$($fileNoExtension)\$($exec) /s /f1$($PSScriptRoot)\$($issFile) /f2$($bin)\Compose_silent_x64_install.log"

    Start-Sleep 5 
    
	Write-Host "Run Compose silent installation : $($silentInstall)"
    Invoke-Expression $silentInstall
    while (!(Test-Path "$($bin)\Compose_silent_x64_install.log")) { 
        Write-Host "[Waiting Compose to be installed] ..."
        Start-Sleep 4
    }
	$resultLogs = Get-IniFile "$($bin)\Compose_silent_x64_install.log"
	$testResult = $resultLogs.ResponseResult.ResultCode
	Write-Host "Installation return code : $($testResult)"

}
else
{
    Write-Host "No Binary found: $($bin)\$($fileName)"
}
		

