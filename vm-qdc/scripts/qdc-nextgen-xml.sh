#!/bin/bash

echo "--- Start qdc-nextgen-xml.sh $1"
TOMCAT_HOME="/usr/local/qdc/apache-tomcat-7.0.94"
hostname=`hostname | cut -d"." -f1`
BASEDIR=$(dirname "$0")

FILE=/usr/local/qdc/qlikcore/docker-compose-qlikContainers.yml
if [ -f "$FILE" ]; then  

    sudo systemctl daemon-reload
    sudo service containerd restart
    sudo service docker stop
    sudo service docker start

    sleep 20

    echo "Stopping qlikContainers service..."
    sudo systemctl stop qlikContainers.service
    echo "Done."

    echo "Setting machine IP ($1) in docker-compose-qlikContainers.yml"
    sed -i "s/host= port=5432/host=$1 port=5432/" $FILE
    echo "Done."

    sleep 5 

    echo "Starting qlikContainers service..."
    sudo systemctl start qlikContainers.service
    echo "Done."

    sudo docker ps
    
fi

FILE=$TOMCAT_HOME/conf/core_env.properties
if [ -f "$FILE" ]; then
    echo "Setting machine IP ($1) in core_env.properties"
    sed -i "s/your-host-name:8080/$1:8080/" $FILE
    echo "Done."
fi

FILE=/usr/local/qdc/dcaasIntegration/docker-compose.dcaas-connectors.yml
if [ -f "$FILE" ]; then

    echo "Stopping Tomcat"
    sudo systemctl stop tomcat.service
    mv $TOMCAT_HOME/conf/server.xml $TOMCAT_HOME/conf/server.xml_backup
    cp $BASEDIR/nextgen-xml_server.xml $TOMCAT_HOME/conf/server.xml
    echo "Starting Tomcat"
    sudo systemctl start tomcat.service
    echo "Done."
    
    echo "Setting machine IP ($1) in docker-compose.dcaas-connectors.yml"
    sed -i "s/\${DCAAS_HOST_NAME}/$1/" $FILE
    echo "Done."

    echo "Restarting nextgen-xml.service"
    sudo systemctl restart nextgen-xml.service
    echo "Done."

    sudo docker ps
    
fi

echo "--- Done qdc-nextgen-xml.sh"



