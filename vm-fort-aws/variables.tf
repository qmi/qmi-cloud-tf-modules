variable "region" {
    default = "eu-west-1"
}

variable "provision_id" { 
}

variable "user_id" {
    default = null
}

variable "key_name" {
    default = "qmiforts"
}

variable "instance_type" {
    default = "t2.2xlarge"
}

variable "volume_type" {
    default = null
}

variable "ami_eu" {
    default = "ami-0b29542f4855536ee"
}

variable "ami_ap" {
    default = "ami-0bd8ac0283a352071"
}

variable "ami_us" {
    default = "ami-0b6b4ec19052ff87e"
}

variable "vpc_security_group_ids_eu" {
  default = ["sg-0162d0120153aea3b"]
}

variable "vpc_security_group_ids_ap" {
  default = ["sg-0db65e064ace2f88f"]
}

variable "vpc_security_group_ids_us" {
  default = ["sg-0bf4042a0477d2da4"]
}

variable "subnet_ids_eu" {
  default = ["subnet-4d441b17", "subnet-95c22fde", "subnet-70938116"]
}

variable "subnet_ids_ap" {
  default = ["subnet-658aec3c", "subnet-e030eba8", "subnet-94309bf2"]
}

variable "subnet_ids_us" {
  default = ["subnet-4d26552b", "subnet-0414685b", "subnet-c97f7c84", "subnet-7f695c71", "subnet-96acd2b7", "subnet-88ab2cb9"]
}