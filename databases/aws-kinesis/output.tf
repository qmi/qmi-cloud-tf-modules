output "arn_metadata" {
  value = aws_kinesis_stream.metadata.arn
} 

output "arn_employees" {
  value = aws_kinesis_stream.semployees.arn
}

output "arn_jobs" {
  value = aws_kinesis_stream.sjobs.arn
}

output "arn_attrep_apply_exceptions" {
  value = aws_kinesis_stream.attrep_apply_exceptions.arn
}

output "iam_name" {
  value = module.iam_user.this_iam_user_name
}

output "iam_access_key" {
  value = module.iam_user.this_iam_access_key_id
}

output "iam_access_secret" {
  value = nonsensitive(module.iam_user.this_iam_access_key_secret)
}