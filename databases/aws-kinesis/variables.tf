variable "region" {
  default = "us-east-1"
}

variable "provision_id" {
  
}

variable "user_id" {
}

variable "forced_destroy" {
  default = null
}
