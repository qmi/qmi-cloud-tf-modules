output "default_subnet_id" {
    value = azurerm_subnet.default.id
}

output "frontend_subnet_id" {
    value = azurerm_subnet.frontend.id
}

output "bastion_subnet_id" {
    value = azurerm_subnet.bastion-subnet.id
}

output "log_analytics_workspace_id" {
    value = azurerm_log_analytics_workspace.logs.id
}