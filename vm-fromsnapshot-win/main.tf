resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!@"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = (var.virtual_machine_name != null)? var.virtual_machine_name :  "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = nonsensitive(random_password.password.result)
  restartAfter = (var.restartAfterRename == true)? "-RestartAfter" : ""
  notrename = (var.notrename != null)? "-NotApply" : ""
  snaploc = (var.location == "westeurope")? "weu" : (var.location == "eastus")? "" : "sea"
  storage_account_id = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Storage/storageAccounts/machinesnapshots${local.snaploc}"
}

resource "azurerm_managed_disk" "md-import" {
  
  count = var.snapshot_uri != null? 1 : 0 

  name                  = "Disk-${var.prefix}-${random_id.randomMachineId.hex}"
  location              = var.location
  resource_group_name   = var.resource_group_name
  storage_account_type  = "Premium_LRS"
  create_option         = "Import"
  storage_account_id    = local.storage_account_id
  source_uri            = var.snapshot_uri
  // Special V2 for Compose (QDI)
  hyper_v_generation    = (length(regexall(".*COMP.*", local.virtual_machine_name)) > 0)? "V2" : "V1"
  
  disk_size_gb          = var.disk_size_gb

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
  }
}

resource "azurerm_managed_disk" "md-copy" {

  count = var.snapshot_id != null? 1 : 0
  
  name = "Disk-${var.prefix}-${random_id.randomMachineId.hex}"
  location = var.location
  resource_group_name = var.resource_group_name
  storage_account_type = "Premium_LRS"
  create_option = "Copy"
  source_resource_id = var.snapshot_id
  disk_size_gb = var.disk_size_gb

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
  }
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_virtual_machine" "vm" {
  name                  = local.virtual_machine_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [ module.qmi-nic.id ]
  vm_size               = var.vm_type


  storage_os_disk {
    name              = var.snapshot_uri != null? azurerm_managed_disk.md-import[0].name : azurerm_managed_disk.md-copy[0].name
    os_type           = "Windows"
    managed_disk_id   = var.snapshot_uri != null? azurerm_managed_disk.md-import[0].id : azurerm_managed_disk.md-copy[0].id
    managed_disk_type = "Premium_LRS"
    create_option     = "Attach"
  }

  os_profile_windows_config {
    
    provision_vm_agent        = true
    enable_automatic_upgrades = false

    winrm {
      protocol = "HTTP"
    }
  }

  identity {
    type = "SystemAssigned"
  }

  /*os_profile {
    computer_name = local.virtual_machine_name
    admin_username = local.admin_username
  }*/

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

}

module "win-common" {
  count = var.wincommon? 1 : 0
  
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//win-common"

  depends_on = [
    azurerm_virtual_machine.vm
  ]
  
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = var.initial_password  
  carbonblack         = var.carbonblack
  tenable             = var.tenable
  
}

resource "null_resource" "post-vm-fromsnapshot-win" {
  
  count = var.initial_password != null? 1 : 0

  depends_on = [
    azurerm_virtual_machine.vm,
    module.win-common
  ]

  provisioner "file" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = var.initial_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }
    source      = "${path.module}/main"
    destination = "C:/tmp/provision"
  }


  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = var.initial_password
      port     = 5985
      https    = false
      timeout  = "30m"
    }

    inline = [
      "powershell.exe -File C:/tmp/provision/prep-files.ps1 -NoProfile",
      "powershell.exe -File C:/tmp/provision/bootstrap.ps1 -NoProfile",
      "powershell.exe -File C:/tmp/provision/password.ps1 -NoProfile -Username ${local.admin_username} -Pass ${local.admin_password}"
    ]
  }

  # Rename Computer
  provisioner "remote-exec" {
    connection {
      type     = "winrm"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = local.admin_password
      port     = 5985
      https    = false
      timeout  = "20m"
    }

    inline = [
      "powershell.exe -File C:/tmp/provision/RenameComputer.ps1 -NoProfile -NewName ${local.virtual_machine_name} ${local.notrename} ${local.restartAfter}" 
    ]

    on_failure = continue
  }

  provisioner "local-exec" {
    command = "echo 'Waiting ${var.waitAfterRestartSecs} seconds after restart'; sleep ${var.waitAfterRestartSecs};"
  }

}
