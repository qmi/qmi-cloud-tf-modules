
$ProgressPreference = 'SilentlyContinue'
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Write-Host "--- Downloading QLC demo content..."

New-Item -ItemType Directory -Force -Path "C:\Temp" | Out-Null
New-Item -ItemType Directory -Force -Path "C:\QLC_Setup" | Out-Null
New-Item -ItemType Directory -Force -Path "C:\QLC_Setup\QlikSense Demo" | Out-Null
New-Item -ItemType Directory -Force -Path "C:\Qlik" | Out-Null


$url = "https://d7ipctdjxxii4.cloudfront.net/QLCDemo/Classicmodels3.zip"
Invoke-WebRequest -Uri $url -OutFile "C:\Temp\Classicmodels.zip"

$url = "https://d7ipctdjxxii4.cloudfront.net/QLCDemo/ExampleStructure.zip"
Invoke-WebRequest -Uri $url -OutFile "C:\Temp\ExampleStructure.zip"

#$url = "https://d7ipctdjxxii4.cloudfront.net/QLCDemo/QlikLineageConnectorSetup.msi"
#$url = "https://github.com/qlik-download/lineage-connector/releases/download/v1.0.0/QlikLineageConnectorSetup.msi"
$url = "https://github.com/qlik-download/lineage-connector/releases/download/v1.1.0/QlikLineageConnectorSetup.msi"
Invoke-WebRequest -Uri $url -OutFile "C:\QLC_Setup\QlikLineageConnectorSetup.msi"

$url = "https://d7ipctdjxxii4.cloudfront.net/QLCDemo/QLC_Installation and Settings.docx"
Invoke-WebRequest -Uri $url -OutFile "C:\QLC_Setup\QLC_Installation and Settings.docx"

Expand-Archive -LiteralPath C:\Temp\Classicmodels.zip -DestinationPath "C:\QLC_Setup\QlikSense Demo" -Force  | Out-Null
Expand-Archive -LiteralPath C:\Temp\ExampleStructure.zip -DestinationPath "C:\Qlik" -Force  | Out-Null

$wshshell = New-Object -ComObject WScript.Shell
$lnk = $wshshell.CreateShortcut("C:\Users\Public\Desktop\QLC_Setup.lnk") 
$lnk.TargetPath ="c:\QLC_Setup"
$lnk.Save() 

$wshshell2 = New-Object -ComObject WScript.Shell
$urllnk = $wshshell2.CreateShortcut("C:\Users\Public\Desktop\QLC_Installation and Settings.lnk")
$urllnk.TargetPath = $wshshell2.ExpandEnvironmentStrings("C:\Program Files\Google\Chrome\Application\chrome.exe")
$urllnk.Arguments = "https://qliktechnologies365-my.sharepoint.com/:w:/g/personal/ves_qlik_com/EW4zTx_x-PtNgd_rBUX4oz0BBPRHzTyhwHQiVIVBrpRg6g?e=fpNsvE"
$urllnk.Save()


$wshshell3 = New-Object -ComObject WScript.Shell
$urllnk2 = $wshshell3.CreateShortcut("C:\QLC_Setup\QLC_Installation and Settings.lnk")
$urllnk2.TargetPath = $wshshell3.ExpandEnvironmentStrings("C:\Program Files\Google\Chrome\Application\chrome.exe")
$urllnk2.Arguments = "https://qliktechnologies365-my.sharepoint.com/:w:/g/personal/ves_qlik_com/EW4zTx_x-PtNgd_rBUX4oz0BBPRHzTyhwHQiVIVBrpRg6g?e=fpNsvE"
$urllnk2.Save()


Write-Host "--- Importing Qlik Sense apps..."
Function restartQse {
    Write-Host "Checking Engine Service has started..."
    $qse = Get-Service QlikSenseEngineService
    Write-Host "The engine is currently $($qse.Status)"
    if ($qse.Status -eq "Stopped") {
        Write-Host "Starting Qlik Sense Engine and waiting 60 seconds"; 
        Start-Service QlikSenseEngineService; 
        Restart-Service QlikSenseServiceDispatcher; 
        start-sleep -s 60
    }
    Write-Host "The engine is currently $($qse.Status)"
}

Import-Module Qlik-Cli


### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {
    Write-Host "--- Connecting to Qlik Sense Repository"; start-sleep 5
} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )


### Import scenario extensions
<#Write-Host "Importing extensions from C:\Temp\qs-import\Extensions"
if ( Test-Path "C:\Temp\qs-import\Extensions" ) {
    gci C:\\Temp\\qs-import\\Extensions\\*.zip | foreach {
        try {
            Write-Host "Importing $_";
            Import-QlikExtension -ExtensionPath $_.FullName | Out-Null
        } catch {
            Write-Host $_.Exception.Message -ForegroundColor Red
        }
    }
}#>



### Import scenario applications
Write-Host "Connecting as user Qlik to QRS..."
try {
    $cert = "CN=$env:COMPUTERNAME-ca"
    gci cert:\CurrentUser\My | where {$_.issuer -eq $cert} | Connect-Qlik -username "$env:COMPUTERNAME\qlik" | Out-Null
} catch {
    Write-Host $_.Exception.Message -ForegroundColor Red
}

### Creating data connection
try {
    Write-Host "Creating data connection..."
    New-QlikDataConnection -connectionstring "C:\QLC_Setup\QlikSense Demo\Classicmodels" -name "Classicmodels" -type "folder" | Out-Null
} catch {
    Write-Host $_.Exception.Message -ForegroundColor Red
}

Write-Host "Importing applications from C:\QLC_Setup\QlikSense Demo ..."

If (Test-Path "C:\QLC_Setup\QlikSense Demo\") {

    $files = gci "C:\QLC_Setup\QlikSense Demo\*.qvf" -File
    
    $EveryoneStreamId = $(Get-QlikStream -filter "name eq 'Everyone'").id

    foreach ($file in $files) {
        try {
            Write-Host "Importing $($file.FullName)";
            Import-QlikApp -name $file.BaseName -file $file.FullName -upload | Out-Null;
            $appId = $(Get-QlikApp -filter "name eq '$($file.BaseName)'").id

            Write-Host "Reloading App: $($file.BaseName) ..."
            New-QlikTask -appId $appId -name "$($file.BaseName)" | Start-QlikTask

            Start-Sleep -s 5

            Write-Host "Publishing App: $($file.BaseName) ..."
            Publish-QlikApp -id $appId -stream $EveryoneStreamId -name $file.BaseName | Out-Null

            

        } catch {
            Write-Host $_.Exception.Message -ForegroundColor Red
        }
    }
}


 

