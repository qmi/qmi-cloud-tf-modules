output "cluster_id" {
  value = (var.cluster_name != null)? databricks_cluster.dbrickscluster[0].id : null
}

output "databricks_token" {
    value = nonsensitive(databricks_token.pat.token_value)
}

output "cluster_name" {
    value = "cluster-${var.cluster_name}"
}

output "sql_endpoint_jdbc_url" {
    value = var.sku == "premium"? databricks_sql_endpoint.sqlep[0].jdbc_url : null
}

output "sql_endpoint_data_source_id" {
    value = var.sku == "premium"? databricks_sql_endpoint.sqlep[0].data_source_id : null
}

output "sql_endpoint_odbc_params" {
    value = var.sku == "premium"? databricks_sql_endpoint.sqlep[0].odbc_params : null
}

 