resource "random_id" "randomMachineId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 2
}

resource "random_password" "password" {
  length = 16
  special = true
  override_special = "_!"
  upper = true
  lower = true
  min_lower = 2
  min_upper = 2
  min_special = 2
}

locals {
  virtual_machine_name = (var.virtual_machine_name != null)? var.virtual_machine_name :  "${var.prefix}-${random_id.randomMachineId.hex}"
  admin_username = var.admin_username
  admin_password = var.new_password != null? var.new_password : nonsensitive(random_password.password.result)

  snaploc = (var.location == "westeurope")? "weu" : (var.location == "eastus")? "" : "sea"
  storage_account_id = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-Machines/providers/Microsoft.Storage/storageAccounts/machinesnapshots${local.snaploc}"
  restartAfter = (var.restartAfterRename == true)? "YES" : "NO"
}

resource "azurerm_managed_disk" "md-import" {
  
  count = var.snapshot_uri != null? 1 : 0 

  name                  = "Disk-${var.prefix}-${random_id.randomMachineId.hex}"
  location              = var.location
  resource_group_name   = var.resource_group_name
  storage_account_type  = "Premium_LRS"
  create_option         = "Import"
  storage_account_id    = local.storage_account_id
  source_uri            = var.snapshot_uri
  disk_size_gb          = var.disk_size_gb

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
  }
}

resource "azurerm_managed_disk" "md-copy" {

  count = var.snapshot_id != null? 1 : 0
  
  name = "Disk-${var.prefix}-${random_id.randomMachineId.hex}"
  location = var.location
  resource_group_name = var.resource_group_name
  storage_account_type = "Premium_LRS"
  create_option = "Copy"
  source_resource_id = var.snapshot_id
  disk_size_gb = var.disk_size_gb

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
  }
}

module "qmi-nic" {
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//qmi-nic"
  
  prefix = local.virtual_machine_name
  location = var.location
  subnet_id = var.subnet_id
  
  resource_group_name = var.resource_group_name
  user_id = var.user_id
}

resource "azurerm_virtual_machine" "vm" {
  name                  = local.virtual_machine_name
  location              = var.location
  resource_group_name   = var.resource_group_name
  network_interface_ids = [ module.qmi-nic.id ]
  vm_size               = var.vm_type


  storage_os_disk {
    name              = var.snapshot_uri != null? azurerm_managed_disk.md-import[0].name : azurerm_managed_disk.md-copy[0].name
    os_type           = "Linux"
    managed_disk_id   = var.snapshot_uri != null? azurerm_managed_disk.md-import[0].id : azurerm_managed_disk.md-copy[0].id
    managed_disk_type = "Premium_LRS"
    create_option     = "Attach"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  identity {
    type = "SystemAssigned"
  }

  tags = {
    "Deployment" = "QMI PoC"
    "Cost Center" = "3100"
    "ProvId"  = var.provId != null? var.provId : null
    "QMI_user" = var.user_id != null? var.user_id : null
    "Owner" = var.user_id != null? var.user_id : null
    "24x7" = var.is_24x7 == true? "" : null
    "ShutdownTime": var.is_24x7 == false? var.shutdownTime : null
    "StartupTime": var.is_24x7 == false? var.startupTime : null
  }

}

module "linux-common" {

  count = var.initial_password != null? 1 : 0
  
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//linux-common"

  depends_on = [
   azurerm_virtual_machine.vm
  ]
  
  os_type             = "centos"
  private_ip_address  = module.qmi-nic.private_ip_address
  admin_username      = local.admin_username
  admin_password      = var.initial_password
  resize              = false
  update              = false
}

resource "null_resource" "post-vm-fromsnapshot-linux" {
  
  count = var.initial_password != null? 1 : 0

  depends_on = [
    module.linux-common
  ]

  provisioner "file" {
    connection {
        type     = "ssh"
        host     = module.qmi-nic.private_ip_address
        user     = local.admin_username
        password = var.initial_password
        timeout  = "3m"
    }

    source      = "${path.module}/provision-scripts"
    destination = "/home/${local.admin_username}"
  }

  provisioner "local-exec" {

    command = "echo 'User/Pass: ${local.admin_username} / ${var.initial_password}'"

  }

  provisioner "remote-exec" {
    connection {
      type     = "ssh"
      host     = module.qmi-nic.private_ip_address
      user     = local.admin_username
      password = var.initial_password
      timeout  = "3m"
    }

    inline = [
        "echo ${var.initial_password} | sudo -S chmod a+x /home/${local.admin_username}/provision-scripts/*.sh",
        "sudo /home/${local.admin_username}/provision-scripts/setnewpassword.sh ${local.admin_username} ${local.admin_password}",
        "sudo /home/${local.admin_username}/provision-scripts/rename-machine.sh ${local.virtual_machine_name} ${local.restartAfter}",
    ]
  }
    
}




