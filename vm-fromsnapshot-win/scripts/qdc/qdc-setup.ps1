Param(
    [string] $QDC_HOST = "QMI-QDC-SN",
    [string] $QDC_PUBLIC_HOST
)

function New-Credential($u,$p) {
    $secpasswd = ConvertTo-SecureString $p -AsPlainText -Force
    return New-Object System.Management.Automation.PSCredential ($u, $secpasswd)
}

Write-Log -Message "Setting up QDC pre-requisites in Qlik Sense"

Import-Module Qlik-Cli

### Connect to the Qlik Sense Repository Service with Qlik-Cli
do {write-log -Message "Connecting to Qlik Sense Repository..."; start-sleep 15} 
While( (Connect-Qlik $($env:COMPUTERNAME) -TrustAllCerts -UseDefaultCredentials -ErrorAction SilentlyContinue).length -eq 0 )

#-----------

# Load variables
. $PSScriptRoot\qdc-scenario-config.ps1

Write-Log -Message "Creating QDC virtual proxy"
$jwtcert = Get-Content -raw 'C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\server.pem'
New-QlikVirtualProxy -description "Qlik Data Catalyst" -sessionCookieHeaderName "X-Qlik-QDC-Session" -authenticationMethod JWT `
  -prefix qdc -loadBalancingServerNodes $(Get-QlikNode -filter "name eq 'Central'").id `
  -jwtAttributeUserDirectory "[QLIK-EXTERNAL-SERVICE]" -jwtAttributeUserId "name" -jwtPublicKeyCertificate $jwtcert -websocketCrossOriginWhiteList $QDC_PUBLIC_HOST | Out-Null

Add-QlikProxy -ProxyId $(Get-QlikProxy -filter "serverNodeConfiguration.hostName eq '$($env:COMPUTERNAME)'").id -VirtualProxyId $(Get-QlikVirtualProxy -filter "description eq 'Qlik Data Catalyst'").id | Out-Null

Start-Sleep -s 10

Write-Log -Message "Downloading qdc_proxy_artifacts - This will create 'qlik-data-catalyst' qliksense user"
Write-Log -Message "PROXY_ARTIFACTS = $PROXY_ARTIFACTS"

$ENV:PATH += ";C:\Program Files\Qlik\Sense\ServiceDispatcher\Node"
cd $env:TEMP
(New-Object System.Net.WebClient).DownloadFile($PROXY_ARTIFACTS, "$env:TEMP\qdc_proxy_artifacts.zip")
#Invoke-WebRequest -UseBasicParsing -OutFile qdc_proxy_artifacts.zip -Uri $PROXY_ARTIFACTS
Expand-Archive .\qdc_proxy_artifacts.zip .
cd qs-virtual-proxy
(Get-Content .\check-proxy.js).replace("ducks-sense1.ad.podiumdata.net", "$($env:COMPUTERNAME)") | Set-Content .\check-proxy.js
Copy-Item "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates\qdc.jwt" .

ls

Get-QlikUser

node .\check-proxy.js

Get-QlikUser -filter "name eq 'qlik-data-catalyst'"

Write-Log -Message "Waiting 20 secs"
Start-Sleep -s 20

Write-Log -Message "Set 'qlik-data-catalyst' as AuditAdmin"
Update-QlikUser -id $(Get-QlikUser -filter "name eq 'qlik-data-catalyst'").id -roles AuditAdmin | Out-Null

Write-Log -Message "Creating security rule for access by QLIK-EXTERNAL-SERVICE"
New-QlikRule -Name "Security rule for access by QLIK-EXTERNAL-SERVICE" -resourceFilter "DataConnection_*,App_*" `
-actions 2 -rule '((user.userDirectory="QLIK-EXTERNAL-SERVICE"))' -rulecontext both -category Security | Out-Null


Write-Log -Message "Adding qvd-mock to service dispatcher"
$dir = pwd
cd  "C:\Program Files\Qlik\Sense"
Expand-Archive $dir\qvd-mock.zip .
Copy-Item $dir\qvd-mock.json  "C:\Program Files\Qlik\Sense\BrokerService\service-configs\"

$mock=@"

[qvd-mock]
Identity=Qlik.qvd-mock
DisplayName=QVD Mock
ExePath=Node\node.exe
Script=..\qvd-mock\server.js        
"@

Add-Content "C:\Program Files\Qlik\Sense\ServiceDispatcher\services.conf" $mock

Restart-Service QlikSenseServiceDispatcher

Write-Log -Message "Creating 'QVD Catalog' tag"
New-QlikTag -name "QVD Catalog" | Out-Null

Write-Log -Message "Creating 'c:\QVDs' folder and set SMB"
New-Item "C:\QVDs" -type directory | Out-Null
New-SMBShare -Name "qvds" -Path "C:\QVDs" | Out-Null 
Grant-SmbShareAccess -Name qvds -AccountName Everyone -AccessRight Change -Force | Out-Null

Write-Log -Message "Creating Qlik Sense data connection for C:\QVDs"
# ---- Workaround qlik-cli QS April 2020
# Qlik CLI New-QlikDataConnection fails with only 1 tag, that's why I create and use FakeTag too.
New-QlikTag -name "FakeTag" | Out-Null
$qvdsDC = New-QlikDataConnection -connectionstring "\\$($env:COMPUTERNAME)\qvds" -name "QVDs" -tags "QVD Catalog","FakeTag" -type "folder"
# ----

Write-Log -Message "Grant access to all user to this connection"
New-QlikRule -name "QVDs allow to all" -comment "Allow QVDs Connection to all users" -category "Security" -resourceFilter  "DataConnection_$($qvdsDC.id)" -actions 23 -rule '((user.name like "*"))' | Out-Null


Write-Log -Message "Setting firewall rules for QDC"
New-NetFirewallRule -DisplayName "QDC QVD Metadata" -Action allow -LocalPort 7007 -Protocol TCP | Out-Null
New-NetFirewallRule -DisplayName "pub2qlik" -Action allow -LocalPort 4243,4747 -Protocol TCP | Out-Null

# certs needed for qdc
Write-Log -Message "SMB Qlik Sense certificates folder" 
New-SMBShare -Name "certs" -Path "C:\ProgramData\Qlik\Sense\Repository\Exported Certificates\.Local Certificates" | Out-Null
Grant-SmbShareAccess -Name certs -AccountName Everyone -AccessRight Read -Force | Out-Null


# create podium user
Import-Module "Carbon"
Write-Log -Message "Adding 'podium' user to Windows system"
$cred = New-Credential "podium" "Qlik1234"
Install-CUser -Credential $cred | Out-Null

#gci cert:\CurrentUser\My | where {$_.issuer -eq $cert}  | Connect-Qlik -Username $env:COMPUTERNAME\podium -Computername $env:COMPUTERNAME
#gci cert:\CurrentUser\My | where {$_.issuer -eq $cert} | Connect-Qlik -Computername $env:COMPUTERNAME
#Update-QlikUser -id $(Get-QlikUser -filter "name eq 'podium'").id -roles RootAdmin

#gci cert:\CurrentUser\My | where {$_.issuer -eq $cert}  | Connect-Qlik -Username $env:COMPUTERNAME\podium -Computername $env:COMPUTERNAME

Write-Log -Message "Creating 'podium_dist' postgres connection"
$cred = New-Credential "postgres" "postgres"
$podiumDistDC = New-QlikDataConnection -connectionstring "'CUSTOM CONNECT TO `"provider=QvOdbcConnectorPackage.exe;driver=postgres;host=$QDC_HOST;port=5432;db=podium_dist;SSLMode=prefer;UseSystemTrustStore=false;ByteaAsLongVarBinary=0;TextAsLongVarchar=0;UseUnicode=1;FetchTSWTZasTimestamp=1;MaxVarcharSize=262144;UseDeclareFetch=1;Fetch=200;EnableTableTypes=1;MoneyAsDecimal=1;QueryTimeout=30;`"" `
 -name podium_dist  -type 'QvOdbcConnectorPackage.exe' -Credential $cred 

Write-Log -Message "Grant access to all user to this connection"
New-QlikRule -name "Podium_Dist allow to all" -comment "Allow Podium_Dist Connection to all users" -category "Security" -resourceFilter  "DataConnection_$($podiumDistDC.id)" -actions 23 -rule '((user.name like "*"))' | Out-Null



 Write-Log "Copy Sample QVDs into C:/QVDs folder"
Copy-Item $PSScriptRoot\*.qvd C:\QVDs

$x=1
$files = Get-ChildItem C:\QVDs\*.qvd|sort LastWriteTime
foreach ($file in $files) #{ echo $file }
{

    if ($x -lt 3) {
        Set-ItemProperty -Path $file -Name LastWriteTime -Value (get-date) 
        sleep 2
    }
    $x= $x +1
}

Write-Log "QDC Config Completed."