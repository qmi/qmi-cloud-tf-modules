Write-Host "Create Target DB on MSSQL" 
$cmd = 'sqlcmd -U sa -P Qlik1234 -Q "CREATE DATABASE ReplicateTarget"'
Invoke-Expression $cmd

Write-Host "Open Firewall ports 80 and 443"
New-NetFirewallRule -DisplayName "Attunity Replicate and Compose" -Direction Inbound -Action Allow -EdgeTraversalPolicy Allow -Protocol TCP -LocalPort 80,443 | Out-Null
