resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = var.resource_group_name
    }
    
    byte_length = 8
}

resource "azurerm_storage_account" "qmi-storage-account" {
    name                = "diag${random_id.randomId.hex}"
    resource_group_name = var.resource_group_name
    location            = var.location
    
    account_kind              = "StorageV2"
    account_replication_type  = "LRS"
    account_tier              = "Standard"
    access_tier               = "Hot"

    tags = {
        Deployment = "QMI PoC"
        "Cost Center" = "3100"
        QMI_user = var.user_id
        Owner    = var.user_id
    }
}