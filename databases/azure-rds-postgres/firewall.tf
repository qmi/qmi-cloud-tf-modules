resource "azurerm_postgresql_virtual_network_rule" "vnetrule" {

  count = var.subnet_id != null? 1 : 0

  name                = "vnet-rule-${var.provision_id}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.postgresql-server.name
  subnet_id           = var.subnet_id
}

resource "azurerm_postgresql_virtual_network_rule" "vnetrule_qmiinfra" {

  count = var.envbranch == "master" && var.location == "eastus" ? 1 : 0

  name                = "vnet-qmiinfra-${var.provision_id}"
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.postgresql-server.name
  subnet_id           = "/subscriptions/62ebff8f-c40b-41be-9239-252d6c0c8ad9/resourceGroups/QMI-infra-vnet/providers/Microsoft.Network/virtualNetworks/QMI-Automation-Vnet/subnets/QMI-Infrastructure"

}

module "fw-ips" {
    source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/firewall_ips"
}


resource "azurerm_postgresql_firewall_rule" "fw_rule" {
  for_each = module.fw-ips.ips

  name                = each.key
  resource_group_name = var.resource_group_name
  server_name         = azurerm_postgresql_server.postgresql-server.name
  start_ip_address    = each.value.0
  end_ip_address      = each.value.1

}

module "dummy-data" {

  count = var.dummydata != null? 1 : 0

  depends_on = [ 
    azurerm_postgresql_firewall_rule.fw_rule, 
    azurerm_postgresql_virtual_network_rule.vnetrule,
    azurerm_postgresql_virtual_network_rule.vnetrule_qmiinfra 
  ]
  
  source = "git::https://gitlab.com/qmi/qmi-cloud-tf-modules.git//databases/dummy"

  type      = "postgres"
  host      = azurerm_postgresql_server.postgresql-server.fqdn
  username  = "${var.admin_login}@qmi-postgresql-${var.provision_id}"
  password  = nonsensitive(random_password.password.result)
  database  = "postgres"
}