
terraform {

  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.69.0"
    }
  }
}

locals {
  myRegex = "/[^[:alpha:]]/"
  splitLower = split(" ", lower(var.user_id))
  np0 = replace(element(local.splitLower,0), local.myRegex, "")
  np1 = replace(element(local.splitLower,1), local.myRegex, "")
  container_n1 = substr(local.np0, 0, 3)
  container_n2 = substr(local.np1, 0, 1)
  container_n3 = substr(strrev(local.np1), 0, 1)
  
  scnamelower = "${local.container_n1}${local.container_n2}${local.container_n3}"

  tags = {
    Deployment = "QMI PoC"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    Owner    = var.user_id
    ProvID   = var.provision_id
    Name     = "qmi-${var.provision_id}"
    force_destroy = var.forced_destroy
  }

}

resource "aws_kinesis_stream" "attrep_apply_exceptions" {

  name             = "${local.scnamelower}.attrep_apply_exceptions"
  shard_count      = 1

  /*stream_mode_details {
    stream_mode = "PROVISIONED"
  }*/

  tags = local.tags
}

resource "aws_kinesis_stream" "semployees" {

  name             = "${local.scnamelower}.EMPLOYEES"
  shard_count      = 3

  /*stream_mode_details {
    stream_mode = "PROVISIONED"
  }*/

  tags = local.tags
}

resource "aws_kinesis_stream" "sjobs" {

  name             = "${local.scnamelower}.JOBS"
  shard_count      = 3

  /*stream_mode_details {
    stream_mode = "PROVISIONED"
  }*/

  tags = local.tags
}

resource "aws_kinesis_stream" "metadata" {

  name             = "${local.scnamelower}.metadata"
  shard_count      = 1

  /*stream_mode_details {
    stream_mode = "PROVISIONED"
  }*/

  tags = local.tags
}

module "iam_user" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"


  version = "~> 3.0"

  name          = "qmi-user-${var.provision_id}"
  force_destroy = true

  create_iam_user_login_profile = false
  #pgp_key = "keybase:test"

  password_reset_required = false

  tags = local.tags

}

resource "aws_iam_user_policy" "kinesis_pol" {
    
  name = "kinesis_policy_${module.iam_user.this_iam_user_name}"
  user = module.iam_user.this_iam_user_name

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "kinesis:*",
            "Resource": "*"
        }
    ]
  })
}