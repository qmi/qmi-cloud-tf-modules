#!/bin/bash

IN="$3"
arrIN=(${IN//:/ })
host=`echo ${arrIN[0]}` 

echo "---- Load Dummy Data -----"
echo "Username: $1"
echo "Password: $2"
echo "Host: $host"
echo "DB: $4" 
echo "--------------------------"    


cd /tmp
git clone https://github.com/datacharmer/test_db.git
cd test_db
echo "-------- Loading Mysql Employees sample database -----------"  
mysql -h $host -u$1 -p$2 --ssl=TRUE < employees.sql
cd ..
wget -q https://downloads.mysql.com/docs/sakila-db.tar.gz
tar xvfz sakila-db.tar.gz
cd sakila-db

echo "-------- Loading Mysql Sakila sample database -----------"  

mysql -h $host -u$1 -p$2 --ssl=TRUE < sakila-schema.sql 
mysql -h $host -u$1 -p$2 --ssl=TRUE < sakila-data.sql 

cd /tmp

curl https://www.mysqltutorial.org/wp-content/uploads/2018/03/mysqlsampledatabase.zip -o mysqlsampledatabase.zip
unzip mysqlsampledatabase.zip
echo "-------- Loading Mysql ClassicModels sample database -----------"  

mysql -h $host -u$1 -p$2 --ssl=TRUE < mysqlsampledatabase.sql


rm -fr /tmp/*

