Write-Host "--- Downloading Qlik Data Transfer (QDT)..."
$downloadURl="https://da3hntz84uekx.cloudfront.net/QlikDataTransfer/2.4/0/_MSI/Qlik%20DataTransfer.exe"

$ProgressPreference = 'SilentlyContinue'

Write-Host "Create c:\QVDs for Folder Watcher"
New-Item -ItemType Directory -Force -Path C:\QVDs | Out-Null

Write-Host "Downloading QDT"
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
Invoke-WebRequest -Uri $downloadURl -OutFile "C:\Users\Public\Desktop\Install Qlik DataTransfer.exe"


New-Item -ItemType Directory -Force -Path C:\Users\Public\Desktop\QDT-Demo | Out-Null

Copy-Item "$PSScriptRoot\QDT-Prep.bat" -Destination "C:\Users\Public\Desktop\QDT-Demo" -Force | Out-Null
Copy-Item -Path "$PSScriptRoot\apps\*" -Destination "C:\Users\Public\Desktop\QDT-Demo" -Recurse -Force | Out-Null

Write-Host "--- QDT Completed"