
variable "prefix" {
  default = "QMI"
}

variable "subnet_id" {
}

variable "location" {
}

variable "snapshot_id" {
  default = null
}

variable "snapshot_uri" {
  default = null
}

variable "resource_group_name" {
}

variable "vm_type" {
  default = "Standard_D4s_v3"
}

variable "managed_disk_type" {
  default = "Premium_LRS"
}

variable "disk_size_gb" {
  default = "256"
}

variable "admin_username" {
  default = "scdemoadmin"
}

variable "virtual_machine_name" {
  default = null
}

variable "initial_password" {
  default = null
}

variable "new_password" {
  default = null
}

variable "user_id" {
  default = null
}

variable "provId" {
  default = null
}

variable "is_24x7"{
  type = bool
  default = null
}

variable "shutdownTime"{
  default = null
}

variable "startupTime"{
  default = null
}

variable "restartAfterRename" {
  default = false
}


