#!/bin/bash


IN="$3"
arrIN=(${IN//:/ })
host=`echo ${arrIN[0]}`

echo "---- Load Dummy Data -----"
echo "Username: $1"
echo "Password: $2"
echo "Host: $host"
echo "DB: $4"
echo "--------------------------"



echo "---- Loading BikeStore sample database -----"
cd /tmp
curl https://gitlab.com/qmi/qmi-cloud-tf-modules/-/archive/master/qmi-cloud-tf-modules-master.zip?path=databases/dummy/scripts -o qmi-cloud-tf-modules-master-databases-dummy-scripts.zip

unzip qmi-cloud-tf-modules-master-databases-dummy-scripts.zip
cd qmi-cloud-tf-modules-master-databases-dummy-scripts/databases/dummy/scripts

sqlcmd -C -S "$host" -U "$1" -P "$2" -Q "CREATE DATABASE BikeStores"
sqlcmd -C -S "$host" -d "BikeStores" -U "$1" -P "$2" -i ./mssql/createbike.sql
sqlcmd -C -S "$host" -d "BikeStores" -U "$1" -P "$2" -i ./mssql/Bikeloaddata.sql > /tmp/bikestore_load.logs

echo "---- Loading Northwind sample database -----"
cd /tmp
curl https://raw.githubusercontent.com/microsoft/sql-server-samples/master/samples/databases/northwind-pubs/instnwnd.sql -o instnwnd.sql
sqlcmd -C -S "$host" -U "$1" -P "$2" -Q "CREATE DATABASE Northwind"
sqlcmd -C -S "$host" -d "Northwind" -U "$1" -P "$2" -i instnwnd.sql > /tmp/northwind_load.logs

rm -fr /tmp/qmi-cloud-tf-modules-*
