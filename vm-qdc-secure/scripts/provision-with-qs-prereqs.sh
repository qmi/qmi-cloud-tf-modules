#!/bin/bash

BASEDIR=$(dirname "$0")
QDC_LIC=$1
FALCON_CID=$2
QS_HOST=$3
QS_IP=$4
QS_USERNAME=$5
QS_PASSWORD=$6
APPGW_HOSTNAME=$7
QLIKUSER_PASSWORD=$8
QDC_IP=$9

LOG=/home/qmi/provision.log
touch $LOG

echo '"'$0'" "'$1'" "'$2'" "'$3'" "'$4'" "'$5'" "'$6'" "'$7'" "'$8'" "'$9'"'>> $LOG 

$BASEDIR/bootstrap-module.sh "vm-qdc" >> $LOG


echo "--- Executing: falcon.sh: $FALCON_CID" >> $LOG
/home/qmi/scripts/falcon.sh $FALCON_CID >> $LOG

echo "--- Executing: resizedisk.sh" >> $LOG
/home/qmi/scripts/resizedisk.sh >> $LOG

echo "--- Executing: qdc-nextgen-xml.sh '$QDC_IP' '4.13.0'" >> $LOG
/home/qmi/scripts/feb2022/qdc-nextgen-xml.sh "$QDC_IP" "4.13.0" >> $LOG

echo "--- Executing: core_env_setup.sh" >> $LOG
/home/qmi/scripts/feb2022/core_env_setup.sh >> $LOGs

echo "--- Executing: set-license.sh '$QDC_LIC'" >> $LOG
/home/qmi/scripts/feb2022/set-license.sh "$QDC_LIC" >> $LOG



echo "---- Waiting 5 minutes for QS to finish setup in its end" >> $LOG
sleep 5m

echo "--- QDC SETUP" >> $LOG
echo '--- Executing: qdc-post-cfg.sh "'$QS_HOST'" "'$QS_IP'" "'$QS_USERNAME'" "'$QS_PASSWORD'" "'$APPGW_HOSTNAME'" "ok" ' >> $LOG
/home/qmi/scripts/feb2022/qdc-post-cfg.sh $QS_HOST $QS_IP $QS_USERNAME $QS_PASSWORD $APPGW_HOSTNAME "ok"  >> $LOG

echo "--- Qlik Sense connection setup in QDC for QVD Import" >> $LOG
echo '--- Executing: setup-QS-Connection-QVDImport.sh "'$QS_HOST'" "'$QDC_IP'" ' >> $LOG
/home/qmi/scripts/feb2022/setup-QS-Connection-QVDImport.sh $QS_HOST $QDC_IP >> $LOG

echo "--- QVD Catalog setup in Qlik Sense DataManager" >> $LOG
echo '--- Executing: setup-QVDCatalog-DataManager.sh "'$QS_HOST'" "'$QLIKUSER_PASSWORD'" "'$QDC_IP':8080/qdc" ' >> $LOG
/home/qmi/scripts/feb2022/setup-QVDCatalog-DataManager.sh $QS_HOST $QLIKUSER_PASSWORD "$QDC_IP:8080/qdc" >> $LOG
