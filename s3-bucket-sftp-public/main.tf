
terraform {

  required_version = ">= 0.13"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "= 3.74.1"
    }
  }
}

locals {
  tags = {
    Deployment = "QMI"
    "Cost Center" = "3100"
    QMI_user = var.user_id
    ProvID   = var.provision_id
    Name     = "sftp-${var.provision_id}"
    forced_destroy = var.forced_destroy
  }
}

resource "tls_private_key" "sftp-key" {
  algorithm = "RSA"
  rsa_bits  = 4096

}

resource "aws_key_pair" "generated_key" {    
  key_name   = "key-${var.provision_id}"
  public_key = tls_private_key.sftp-key.public_key_openssh

  tags = local.tags
}

## Public_secret

resource "aws_secretsmanager_secret" "public_key" {
  name = "pub-${var.provision_id}"

  tags = local.tags
}

resource "aws_secretsmanager_secret_version" "public_key" {
  secret_id = aws_secretsmanager_secret.public_key.id
  secret_string = aws_key_pair.generated_key.public_key

}

## Private Secret

resource "aws_secretsmanager_secret" "private_key" {
  name = "priv-${var.provision_id}"

  tags = local.tags
}

resource "aws_secretsmanager_secret_version" "private_key" {
  secret_id = aws_secretsmanager_secret.private_key.id
  secret_string = nonsensitive(tls_private_key.sftp-key.private_key_pem)

}


module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"

  version = "~> 2.1.0"

  bucket = "sftp-${var.provision_id}"
  acl    = "private"

  versioning = {
    enabled = false
  }

  force_destroy = true

  tags = local.tags

}

###

resource "aws_s3_bucket_public_access_block" "sftp-block" {
  bucket = module.s3_bucket.s3_bucket_id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true

}

###

resource "aws_iam_role" "logging" {
  name  = "${var.provision_id}-transfer-logging"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "transfer.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "logging" {
  name  = "${var.provision_id}-transfer-logging"
  role   = aws_iam_role.logging.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:DescribeLogStreams",
        "logs:CreateLogGroup",
        "logs:PutLogEvents"
      ],
      "Resource": "*"
    }
  ]
}
POLICY
}

resource "aws_transfer_server" "public" {
  endpoint_type          = var.sftp_type 
  protocols              = var.protocols 
  identity_provider_type = var.identity_provider_type 
  url                    = var.api_gw_url
  invocation_role        = var.invocation_role
  logging_role           = aws_iam_role.logging.arn
  force_destroy          = var.force_destroy 
  security_policy_name   = var.security_policy_name

  tags = local.tags
}


resource "aws_iam_role" "user" {
  for_each           = var.sftp_users
  name               = "${var.provision_id}-sftp-user-${each.key}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "transfer.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy" "user" {
  for_each = var.sftp_users
  name     = "${var.provision_id}-sftp-user-${each.key}"
  role     = aws_iam_role.user[each.key].id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "AllowListingOfUserFolder",
      "Action": [
        "s3:ListBucket",
        "s3:GetBucketLocation"
      ],
      "Effect": "Allow",
      "Resource": [
       "${join("", ["arn:aws:s3:::", module.s3_bucket.s3_bucket_id])}"
      ]
    },
    {
      "Sid": "VisualEditor1",
      "Effect": "Allow",
      "Action": [
          "s3:ListStorageLensConfigurations",
          "s3:ListAccessPointsForObjectLambda",
          "s3:ListAllMyBuckets",
          "s3:ListAccessPoints",
          "s3:ListJobs",
          "s3:ListMultiRegionAccessPoints"
      ],
      "Resource": "*"
    },
    {
      "Sid": "HomeDirObjectAccess",
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObjectVersion",
        "s3:DeleteObject",
        "s3:GetObjectVersion"
      ],
      "Resource": "${join("", ["arn:aws:s3:::", module.s3_bucket.s3_bucket_id, "/*"])}"
    }
  ]
}
POLICY
}

resource "aws_transfer_user" "this" {
  for_each       = var.sftp_users
  server_id      = aws_transfer_server.public.id
  user_name      = each.key
  home_directory = "/${module.s3_bucket.s3_bucket_id}/${each.value}"
  role           = aws_iam_role.user[each.key].arn

  tags = local.tags
}


resource "aws_transfer_ssh_key" "this" {
  for_each   = { "sftpusr" = aws_secretsmanager_secret_version.public_key.secret_string }
  server_id  = aws_transfer_server.public.id
  user_name  = each.key
  body       = each.value
  depends_on = [aws_transfer_user.this]

}
